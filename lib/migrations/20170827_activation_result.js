exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('activations', function(table) {
            table.string('a_result');
        }),

    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('activations', function(table){
            table.dropColumn("a_result");
        }),
    ])

};
