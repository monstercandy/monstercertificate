// NOTE: each entity here is related to the current server, since MonsterCertificate will always run locally

exports.up = function(knex, Promise) {

    return Promise.all([


         /*
          * an entry in this table is added right after it was purchased at mc
          * it will be possible to activate it at a selected server in case of local use (webhosting)
          * or at a dedicated server (external use)
          * the remote MonsterCertificate instances will report back to the global registry
          *
          */
         knex.schema.createTable('purchases', function(table) {
            table.string('pc_id').primary().notNullable();

            table.string('pc_user_id').notNullable();

            /*
            * pending: it has been purchased already, waiting for CSR
            * processing: csr has been uploaded and submitted towards the reseller
            * issued: certificate is issued on pc_server
            */
            table.enu('pc_status', ["pending","processing","issued","revoked","renewed","removed"]).notNullable();

            table.string('pc_activation')
                 .references('a_id')
                 .inTable('activations');
            table.boolean('pc_local');

            table.string('pc_certificate')
                 .references('c_id')
                 .inTable('certificates');

            table.string('pc_reseller').notNullable();
            table.string('pc_vendor').notNullable();
            table.string('pc_product').notNullable();
            table.string('pc_reseller_certificate_id');
            table.string('pc_product_parameters').notNullable();

            table.string('pc_subject_common_name');

            table.timestamp('pc_begin');
            table.timestamp('pc_end');

            table.string('pc_comment').notNullable().defaultTo("");
            table.string('pc_misc');

            table.index(["pc_user_id", "pc_status"]);

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
         }),

         knex.schema.createTable('activations', function(table) {
            table.string('a_id').primary().notNullable();

            table.string('a_user_id').notNullable();
            table.string('a_purchase').notNullable()
                 .references('pc_id')
                 .inTable('purchases');

            table.enu('a_status', ["processing","failed","successful"]).notNullable().defaultTo("processing");

            table.string('a_payload').notNullable();
            table.string('a_extra').notNullable();

            table.index(["a_purchase"]);

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
         }),

         knex.schema.createTable('certificates', function(table) {
            table.string('c_id').primary().notNullable();

            table.string('c_user_id').notNullable();
            table.integer('c_webhosting').notNullable();

            table.string('c_issuer_organization').notNullable();
            table.string('c_issuer_common_name').notNullable();

            table.string('c_subject_common_name').notNullable();
            table.string('c_subject_organization_unit').notNullable();

            table.timestamp('c_begin').notNullable();
            table.timestamp('c_end').notNullable();

            table.boolean('c_settled').notNullable().defaultTo(false);

            table.boolean('c_private_key').notNullable().defaultTo(false);
            table.boolean('c_certificate').notNullable().defaultTo(false);
            table.boolean('c_intermediate_chain').notNullable().defaultTo(false);
            table.boolean('c_certificate_signing_request').notNullable().defaultTo(false);

            // private properties
            table.string('c_alias').notNullable().defaultTo("");

            table.string('c_purchase').notNullable();
            table.string('c_reseller').notNullable();
            table.string('c_vendor').notNullable();
            table.string('c_product').notNullable();
            table.string('c_reseller_certificate_id').notNullable();

            table.string('c_comment').notNullable().defaultTo("");

            table.boolean('c_deleted').notNullable().defaultTo(false);

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());

            table.index(["c_user_id", "c_webhosting", "c_deleted"]);

         }),


         knex.schema.createTable('contacts', function(table) {
            table.increments('co_id').primary().notNullable();

            table.string('co_user_id').notNullable();

            table.boolean('co_deleted').notNullable().defaultTo(false);

            table.string('co_EmailAddress').notNullable();
            table.string('co_JobTitle').notNullable();
            table.string('co_FirstName').notNullable();
            table.string('co_LastName').notNullable();
            table.string('co_OrganizationName').notNullable();
            table.string('co_Address1').notNullable();
            table.string('co_Address2').notNullable();
            table.string('co_City').notNullable();
            table.string('co_StateProvince').notNullable();
            table.string('co_PostalCode').notNullable();
            table.string('co_Country').notNullable();
            table.string('co_Phone').notNullable();

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
         }),


    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('purchases'),
        knex.schema.dropTable('certificates'),
        knex.schema.dropTable('activations'),
        knex.schema.dropTable('contacts'),
    ])

};
