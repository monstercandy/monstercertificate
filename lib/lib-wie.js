var lib = module.exports = function(app, knex) {

   const MError = app.MError;

   const certsLib = require("lib-certs.js")
   var certs = certsLib(app, knex);

   const purchasesLib = require("lib-purchases.js")
   var purchases = purchasesLib(app, knex);
   const Token = require("Token")

   var wie = {};

   const dotq = require("MonsterDotq");
   const moment = require("MonsterMoment");

   wie.BackupWh = function(wh, req) {

      var allUserGroups = [];
      var allUsers;

      var re = {};
      return certs.ListByWebhosting(wh.wh_id, typeof afterEach == "undefined" ? true : false ) // mocha detection (we work differently for unit tests)
        .then(certs=>{
           re.certs = [];

           return dotq.linearMap({array: certs, action: function(cert){

              return cert.Read()
                .then(()=>{
                    Array("closeToExpire", "isExpired", "c_user_id", "c_webhosting").forEach(x=>{
                       delete cert[x];
                    })

                    re.certs.push(cert);
                })

           }})


        })
        .then(()=>{
            // and now lets query letsencrypt activations that would need to be restored
            var this_server = app.config.get("mc_server_name");
            re.purchases = [];
            return purchases.FindLocalLetsEncryptPurchases(wh)
              .then(purchs=>{
                  return dotq.linearMap({array: purchs, action: purch=>{

                      return Token.GetPseudoTokenAsync(purch.pc_id, purch.a_id,  null, 8, "hex")
                      .then(id=>{
                         purch.pc_id = id;
                         return Token.GetPseudoTokenAsync(purch.a_id, purch.pc_id, null, 8, "hex")
                      })
                      .then(id=>{
                         purch.a_id = id;

                          var a = {activation: {}, purchase: {}};
                          Array("a_purchase", "a_user_id", "actions", "canBeRenewed", "isExpired", "pc_activation", "pc_certificate", "pc_user_id").forEach(x=>{
                              delete purch[x];
                          })

                          purch.a_payload = replaceAll(purch.a_payload, '"'+this_server+'"', '"[[server]]"')
                          purch.a_payload = replaceAll(purch.a_payload, '"'+wh.wh_id+'"', '"[[wh_id]]"')
                          purch.a_payload = replaceAll(purch.a_payload, ':'+wh.wh_id+',', ':[[wh_id]],')

                          Object.keys(purch).forEach(n=>{
                             if(n.startsWith('a_'))
                                a.activation[n] = purch[n];
                             if(n.startsWith('pc_'))
                                a.purchase[n] = purch[n];
                          })
                          a.purchase.created_at = purch.created_at;

                          re.purchases.push(a);
                      })
                  }})
              })

        })
        .then(()=>{
            return re;
        })


   }

   // wrapping the restore all operation in a huge transaction
   wie.RestoreAllWhs = function(whs, in_data, req){
      return knex.transaction(function(trx){
          var wieTrx = require("Wie").transformWie(app, lib(app, trx));
          return wieTrx.GenericRestoreAllWhs(whs, in_data, req);
      })
   }


   wie.GetAllWebhostingIds = function(){
      return certs.GetWebhostingIds()
        .then(rows=>{
            return rows.map(x => x.c_webhosting)
        })
   }

   wie.RestoreWh = function(wh, in_data, req) {

      // lets put the site first
      return dotq.linearMap({array: in_data.certs, action: cert=>{

         var now = moment.now();

         cert.c_webhosting = wh.wh_id;
         cert.c_user_id = wh.wh_user_id;

         cert.updated_at = now;

         extend(cert, cert.data);
         delete cert.data;
         if(cert.intermediates) {
            cert.intermediate = cert.intermediates.join("\n");
            delete cert.intermediates;
         }


         return certs.Insert(cert, req)
           .catch(ex=>{
              if((ex.message == "CERT_ALREADY_EXISTS") && (ex.underlyingException) && (ex.underlyingException.cert) && (ex.underlyingException.cert.c_webhosting == wh.wh_id))
                 return; // this is fine, it is already present.

              throw ex;
           })


      }})
      .then(()=>{
         // restoring the purchases as well
         var this_server = app.config.get("mc_server_name");
         return knex.transaction(function(trx){
             var purchases = purchasesLib(app, trx);
             return dotq.linearMap({array: in_data.purchases, action: purch=>{

                 purch.activation.a_payload = replaceAll(purch.activation.a_payload, "[[server]]", this_server);
                 purch.activation.a_payload = replaceAll(purch.activation.a_payload, "[[wh_id]]", wh.wh_id);

                 Array("pc_misc", "pc_product_parameters").forEach(x=>{
                    purch.purchase[x] = JSON.stringify(purch.purchase[x]);
                 })

                 purch.purchase.pc_activation = purch.activation.a_id;
                 purch.activation.a_purchase = purch.purchase.pc_id;

                 purch.purchase.pc_user_id = wh.wh_user_id;
                 purch.activation.a_user_id = wh.wh_user_id;

                 return purchases.RestoreBackup(purch)
                   .catch(ex=>{
                      if(ex.code == 'SQLITE_CONSTRAINT') // this is fine, (secondary restoration)
                         return;
                      throw ex;
                   })

             }});
         })

      })
      .then(()=>{
          app.InsertEvent(req, {e_event_type: "restore-wh-certs", e_other: wh.wh_id})

      })



   }



   return wie;

    function replaceAll(target, search, replacement) {
        return target.split(search).join(replacement);
    };

}
