var lib = module.exports = function(app, knex){
	var re = {}

	const vendorCode = "vendor2"

	const OpensslLib = require("lib-openssl.js");

	const fs = require("MonsterDotq").fs()
	const path = require("path")

    var ValidatorsLib = require("MonsterValidators")
    var vali = ValidatorsLib.ValidateJs()

    const MError = app.MError

    const domainValidatedProducts = [
					"PositiveSSL",
					"PositiveSSL Wildcard",
					"PositiveSSL multi domain",
					"EssentialSSL",
					"EssentialSSL Wildcard",
    ]

      vali.validators.isValidContact = function(value, options, key, attributes) {
      	  if(!value) return
          return options.app.getContacts().GetContactById(value)
            .then(contact=>{
            	attributes[key] = contact
            	return Promise.resolve()
            })
            .catch(ex=>{
            	return Promise.resolve(ex.message)
            })
      }

      vali.validators.isValidCsr = function(value, options, key, attributes) {
      	  // console.log("is this the shitty one?");
          var openssl = OpensslLib(options.app);
          return openssl.ParseCertificateSigningRequest(value)
            .then(csr=>{
            	attributes[key] = csr
            	return Promise.resolve()
            })
            .catch(ex=>{
            	return Promise.resolve(ex.message)
            })
      }

    re.GetNumberOfDaysToRenewTurn = function(){
    	return app.config.get("namecheap_purchase_renew_period")
    }

	re.ShouldRenewBeBought = function(purchase, now, pcend){
          return now.add(re.GetNumberOfDaysToRenewTurn(), 'days').isAfter(pcend)
	}

	re.GetVendors = function() {
		return ["Comodo"]
	}
	re.GetProductsOfVendor = function(vendorName) {
		return [
					"PositiveSSL",
					"PositiveSSL Wildcard",
					"PositiveSSL multi domain",
					"EssentialSSL",
					"EssentialSSL Wildcard",
					"PremiumSSL WildCard",
					"PremiumSSL",
					"InstantSSL Pro",
					"InstantSSL",
					"Multi domain SSL",
					"Unified Communications",
					"EV SSL",
					"EV SGC SSL",
					"EV Multi domain SSL",
			    ]
	}

	re.isValidCertificateId = function(certId){
		var aC = ""+certId
		return aC.match(/^\d+$/)
	}


	function buyValidate(in_data){
		return vali.async(in_data.pc_product_parameters, {
			Years:{presence:true,isInteger: {greaterThanOrEqualTo:1,smallerThanOrEqualTo:3}}
		})
	}


    re.Renew = function(purchase, candidate, in_data) {
    	console.log("Renew was called:", in_data)
		return buyValidate(in_data)
		.then((d)=>{
			extend(candidate.pc_product_parameters, d)
      	   return sendNamecheapRequest("ssl.renew", {Years:d.Years, SSLType: purchase.pc_product, CertificateID: purchase.pc_reseller_certificate_id})
		})
		.then(r=>{
			 // console.log("crap", r)
			 var cert_id
			 if(r.SSLRenewResult)
			 	cert_id = r.SSLRenewResult[0]['$'].CertificateID
			 else
			 if(r['$'])
			 	cert_id = r['$'].CertificateID

			 console.log("crap", r, cert_id)

			if(!cert_id)
				throw new MError("RENEW_INTERNAL_ERROR")
            candidate.pc_reseller_certificate_id = cert_id
		})
    }

	re.Buy = function(in_data, thisIsAnActivation){
		return buyValidate(in_data)
		.then((d)=>{
			if((app.config.get("namecheap_buy_at_activation_only"))&&(!thisIsAnActivation))
				return Promise.resolve()

			// is it bought already?
			if(in_data.pc_reseller_certificate_id)
				return Promise.resolve()

     		return sendNamecheapRequest("ssl.create", {Years:d.Years, Type: in_data.pc_product})
     		  .then(r=>{

     		  	 if(r['$'].IsSuccess != "true")
     		  	 	throw new MError("CREATE_CERTIFICATE_FAILED")

     		  	 return Promise.resolve({purchase:{pc_reseller_certificate_id:r.SSLCertificate[0]['$'].CertificateID}})
     		  })

		})

	}

    function activateDomainValidatedCommon(command, purchase, mainParams, complete, payLoadToSave, extraParams, insertActivationCb) {
		if(!re.isDomainValidated(purchase))
			throw new MError("CERT_IS_NOT_DOMAIN_VALIDATED")

        var activation

        // lets purchase it first it might be still just an entry in the local database
        return re.Buy(purchase, true)
          .then((params)=>{
          	 var e = {}
          	 if(extraParams.mainDomain)
          	 	e.pc_subject_common_name = extraParams.mainDomain

          	 if((params)&&(params.purchase)){
          	 	// it was just purchased, we should save it immediately.
          	 	extend(e, params.purchase)
          	 }

          	 if(Object.keys(e).length)
          	 	return purchase.UpdateParams(e)


          	 return Promise.resolve()
          })
          .then(()=>{
          	if(!purchase.pc_reseller_certificate_id) {
          		console.error("Purchase has no upstream certificate ID, aborting.")
          		throw new MError("INTERNAL_ERROR")
          	}

            return insertActivationCb({activationParameters: payLoadToSave, extraParameters: extraParams})
          })
		  .then(aActivation=>{

		  	activation = aActivation

		  	var p = {
		  		CertificateID: purchase.pc_reseller_certificate_id,
		  		csr: complete.csr,
		  		WebServerType: "apacheopenssl",
		  	}


		  	if(complete.approvalType == "email")
		  		p.ApproverEmail= complete.ApproverEmail
		  	if(complete.approvalType == "http")
		  		p.HTTPDCValidation= 'TRUE'
		  	if(complete.approvalType == "dns")
		  		p.DNSDCValidation= 'TRUE'

            const accents = require("lib-accents.js")
		  	Array("Admin").forEach(mainCat=>{
			  	Object.keys(complete[mainCat]).forEach(x=>{
			  		if(Array("co_id","co_user_id", "created_at").indexOf(x) > -1) return
		  			var v = complete[mainCat][x]
		  		    if(typeof v != "string") return

			  	    var n = x.substr(3)
			  		p[mainCat+n] = accents(v)
			  	})

		  	})

		  	return sendNamecheapRequest("ssl."+(command.toLowerCase()), p, true)
		  })
		  .then(resp=>{

		  	console.log("namecheap responded", resp)
		  	if(resp['$'].IsSuccess != "true")
		  		throw new MError("UPSTREAM_ACTIVATION_FAILED")

            return activation.SetStatus("successful", resp)
          })
          .then(()=>{
          	return purchase.UpdateParams({pc_status:"processing", pc_activation: activation.a_id})

		  })
		  .then(()=>{
		  	 return Promise.resolve(activation)
		  })
		  .catch(ex=>{
    	  	 console.log("error during namecheap activation", complete, ex)

		  	 if(activation) {
		  	 	// mark activation as unsuccessful
		  	 	activation.SetStatus("failed")
		  	 }

		  	 throw ex
		  })

    }




	re.doActivateLocal = function(command, purchase, d, in_data, insertActivationCb){
		/* local activations receive nothing more in d than:
         {
           local:true,
           domains: ...,
           attach:{server: $scope.model.server, webhosting_id:$scope.model.webhosting_id,docroot_domain: $scope.model.docroot_domain}
         }
        */

        var stuffs
        const mainDir = app.config.get("common_params").domain_validation_comodo_webroot_directory
        if(!mainDir) return Promise.reject(new MError("NOT_CONFIGURED"))

        app.fixDomainNamesWww(d);

        return purchase.activateLocal(d, in_data)
          .then(astuffs=>{
          	  stuffs = astuffs

              stuffs.complete.approvalType = "http"

              return activateDomainValidatedCommon(
               	  command,
               	  purchase,
               	  d,
               	  stuffs.complete,
               	  stuffs.save,
               	  stuffs.extra,
               	  insertActivationCb
               )

          })
          .then((activation)=>{
          	 var raw = activation.a_result.HttpDCValidation[0].DNS[0]
          	 if(!raw)
          	 	 throw new MError("HTTP_VALIDATION_NOT_AVAILABLE")
          	 var in_data = {FileName: raw.FileName[0], FileContent: raw.FileContent[0]}

          	 return vali.async(in_data, {
          	 	FileName: {presence: true, isString: true, length:{maximum:100}, isSimpleFilename: true},
          	 	FileContent: {presence: true, isString: true, length:{maximum:1024}},
          	 })
          })
          .then((dv)=>{

          		var fullPath = path.join(mainDir, dv.FileName)
          		return fs.writeFileAsync(fullPath, dv.FileContent)
          })

	}

	re.doActivateGeneric = function(command, purchase, mainParams, in_data, insertActivationCb){

		var activation

		var d

		return vali.async(in_data, {
			csr: {presence: true, isString: true, isValidCsr: {app: app}},
			approvalType: {inclusion:["email","http","dns"], default:"email"},
			Admin_contact_id:{presence: true, isValidContact: {co_user_id: purchase.pc_user_id, app: app}},
		 })
		  .then(ad=>{
		  	 d = ad

             if(d.approvalType == "email")
             	 return vali.async(in_data, {ApproverEmail:{presence: true, mcEmail: true}})

             return Promise.resolve()
          })
          .then(ad=>{
          	 extend(d, ad)

		  	 var extra = {mainDomain: d.csr.Subject.CN}
		  	 d.csr = d.csr['CERTIFICATE REQUEST']

		  	 var save = simpleCloneObject(d)
		  	 save.Admin_contact_id = save.Admin_contact_id.co_id

		  	 d.Admin = d.Admin_contact_id
		  	 delete d.Admin_contact_id

		  	 return activateDomainValidatedCommon(command, purchase, mainParams, d, save, extra, insertActivationCb)
		  })
	}

	re.Revoke = function(purchase, in_data){

		  return sendNamecheapRequest("ssl.revokecertificate", {CertificateType: purchase.pc_product, CertificateID: purchase.pc_reseller_certificate_id})

	}

	re.dispatch_parse_csr = function(purchase, in_data){
		 return vali.async(in_data, {csr: {presence: true, isString: true}})
		   .then((d)=>{
        		return sendNamecheapRequest("ssl.parseCSR", extend({}, d, {HTTPDCValidation: 'TRUE', CertificateType: purchase.pc_product}))
		   })
		   .then(x=>{
		   	  // const util = require("util");console.log(util.inspect(x, false, null))
		   	  var re = {}
		   	  re.csr = x.CSRDetails[0]
		   	  if((x.HttpDCValidation)&&(x.HttpDCValidation[0]))
			   	  re.http_validation = {
			   	  	 FileName: x.HttpDCValidation[0].FileName[0],
			   	  	 FileContent: x.HttpDCValidation[0].FileContent[0],
			   	  }

              return Promise.resolve(re)
		   })

	}

	re.dispatch_query_validation = function(purchase, in_data){
		 var x = {}

         return re.dispatch_get_approver_emails(purchase, in_data)
		   .then(d=>{
		   	  x.email_validation = d.emails

		   	  return Promise.resolve(x)
		   })

	}


	re.dispatch_resend_approver_email = function(purchase, in_data){
		return vali.async(in_data, {ApproverEmail:{mcEmail: true}})
		  .then(d=>{
		  	    if(purchase.pc_status != "processing") throw new MError("INVALID_STATUS")

        		return sendNamecheapRequest("ssl.resendApproverEmail", extend({}, d, {CertificateID: purchase.pc_reseller_certificate_id}))
		  })
		  .then(x=>{
		  	 return Promise.resolve("ok")
		  })
	}

	re.dispatch_get_approver_emails = function(purchase, in_data){
		return vali.async(in_data, {DomainName:{presence:true, isHostWildcard: true}})
		  .then(d=>{

        		return sendNamecheapRequest("ssl.getApproverEmailList", {DomainName: d.DomainName, CertificateType: purchase.pc_product})
		  })
		  .then(x=>{
		  	 var re = {emails:[]}
		  	 Array("Domainemails","Genericemails").forEach(c=>{
		  	 	if((!x[c])||(!x[c][0])||(!x[c][0].email)) return
		  	 	x[c][0].email.forEach(e=>{
		  	 		if(e == "none") return
		  	 		re.emails.push(e)
		  	 	})
		  	 })
		  	 return Promise.resolve(re)
		  })
	}

	re.isDomainValidated = function(purchase) {
		return (domainValidatedProducts.indexOf(purchase.pc_product) > -1)
	}

	re.GetActions_pending = function(purchase){
		if(re.isDomainValidated(purchase))
		   return Array(vendorCode+"-activate-general", vendorCode+"-activate-local")

		return Array(vendorCode+"-manual")
	}
	Array("revoked", "issued").forEach(x=>{
		re["GetActions_"+x] = function(purchase){
			var r = Array()
			if(x=="issued")
			  r.push(vendorCode+"-revoke")
			if(!re.isDomainValidated(purchase)) return
	          r.push(vendorCode+"-reissue-general")

	        if(purchase.pc_local)
	        	 r.push(vendorCode+"-reissue-local")

			return r
		}

	})
	re.GetActions_processing = function(purchase){
		var x = Array(vendorCode+"-resend-approver")
		/*
		if(re.isDomainValidated(purchase)) {
		   x.push(vendorCode+"-activate-general")
		   x.push(vendorCode+"-activate-local")
		}
		*/

		return x

	}

	re.GetBalanceFull = function(){
		return sendNamecheapRequest("users.getBalances")
		  .then(d=>{
		  	  return Promise.resolve({account:re.GetCredentials(),balance:d['$']})
		  })
	}

	re.GetBalance = function(silent){
		return Promise.resolve()
		  .then(()=>{
			if(silent) {
				var p = re.GetCredentials();
				if(!p.ApiKey) return;
			}

			return re.GetBalanceFull()		  	
			  .then(x=>{
			  	 return {AvailableBalance: parseFloat(x.balance.AvailableBalance), Currency: x.balance.Currency, _full: x};
			  })
		  })
	}

	re.GetCredentials = function(dontHideSecret){
		var rawNc = app.config.get("volume").namecheap
		var re = rawNc ? simpleCloneObject(rawNc) : {}
		if(!dontHideSecret)
			delete re.ApiKey
		return re
	}
	function setCredentials(new_creds){
    	var vol = app.config.get("volume")
    	if(!new_creds)
    		delete vol.namecheap
    	else
    	    vol.namecheap = new_creds
    	app.config.saveVolume(vol)
	}
	re.ClearCredentials = function(){
		return setCredentials()
	}
	re.SetCredentials = function(in_data){
		return vali.async(in_data, {
			UserName:{presence:true,isString:true},
			ApiKey:{presence:true,isString:true},
			ApiUser:{presence:true,isString:true},
	    })
	    .then(d=>{
	    	setCredentials(d)
	    })
	}

	re.Poll = function(purchase) {
		var issuedCertificate = {}
		return sendNamecheapRequest("ssl.getInfo", {CertificateID: purchase.pc_reseller_certificate_id, returncertificate: 'true', returntype: 'individual'})
		  .then(d=>{

		  	  // const util = require("util"); console.log("XXXXXXXXXXXXXXX3", util.inspect(d, false, null), d['$'].Status); process.reallyExit();

              if((!d.CertificateDetails)||(!d.CertificateDetails[0])||(!d.CertificateDetails[0].Certificates)||(!d.CertificateDetails[0].Certificates[0])||(d.CertificateDetails[0].Certificates[0]['$'].CertificateReturned != 'true')) {
		  	 	console.log("Certificate not yet enrolled", purchase.pc_id, purchase.pc_reseller_certificate_id)
		  	 	throw new MError("NOT_YET_READY")
		  	  }

		  	  // it is ready!



	  	    var certs = d.CertificateDetails[0].Certificates[0]
	  	    issuedCertificate.certificate = certs.Certificate.join("")

	  	    if((certs.CaCertificates)&&(certs.CaCertificates[0])&&(certs.CaCertificates[0].Certificate)) {
		  	 	issuedCertificate.intermediate = ""
		  	 	certs.CaCertificates[0].Certificate.forEach(ca=>{
		  	 		issuedCertificate.intermediate += ca.Certificate.join("")+"\n"
		  	 	})
		  	}


		  	 return purchase.GetActivation()
			  .then(activation=>{
			  	 if((activation.a_extra)&&(activation.a_extra.privateKey))
			  	 	 issuedCertificate.privateKey = activation.a_extra.privateKey

			  	 return activation.UploadResult(issuedCertificate)
			  })

		  })
	}


    dealWithClientIp()

	return re

   function buildRequestParams(command, params) {
   	  var p = extend({Command: "namecheap."+command}, re.GetCredentials(true), params)
   	  if((app.clientIp)&&(app.clientIp != "-"))
   	  	 p.ClientIP = app.clientIp

   	  if(!p.ApiKey) throw new MError("SERVICE_NOT_CONFIGURED")
   	  return p
   }
   function buildCurlCommand(url, p){
   	  var pl = ""
   	  Object.keys(p).forEach(x=>{
   	  	  var v = x == "ApiKey" ? "***" : encodeURIComponent(p[x])
   	  	  if(pl) pl += "&"
   	  	  pl += x+"="+v
   	  })

   	  return "curl -v -d '"+pl+"' "+url
   }
   function grabTextPartsFromError(errArray) {
   	  var re = []
   	  errArray.forEach(er=>{
   	  	try{
   	  		re.push(er.Error[0]["_"])
   	  	}catch(ex){
   	  		console.error("Unable to retrieve error code", er)
   	  	}

   	  })
   	  return re
   }
   function sendNamecheapRequest(command, params, relayErrors) {
   	  var p = buildRequestParams(command, params)
   	  //request.post('http://service.com/upload', {form:{key:'value'}})
   	  var url = app.config.get("namecheap_api_url")
   	  console.log("Executing Namecheap call", buildCurlCommand(url, p))
   	  return app.PostForm(url, p)
   	    .then(re=>{
   	    	console.log("Namecheap returned", re)
   	        return app.parseXml(re)
   	    })
   	    .then(d=>{
   	    	// const util=require("util"); console.log("We got a namecheap response", command, params, util.inspect(d, {showHidden: false, depth: null}))

   	    	if(!d.ApiResponse)
   	    		throw new MError("INVALID_UPSTREAM_RESPONSE")

   	    	var ApiResponse = d.ApiResponse
   	    	if((ApiResponse.Errors)&&(ApiResponse.Errors[0] != '')) {
   	    		console.error("There were some recoverable(?) errors while talking to the upstream provider")
   	    	}
   	    	if((ApiResponse.Warnings)&&(ApiResponse.Warnings[0] != '')) {
   	    		console.warn("There were some recoverable(?) warnings while talking to the upstream provider")
   	    	}

   	    	if((ApiResponse['$'])&&(ApiResponse['$'].Status != "OK")) {
   	    		throw new MError("UPSTREAM_SERVICE_PROCESSING_ERROR", relayErrors ? grabTextPartsFromError(ApiResponse.Errors) : undefined)
   	    	}
   	    	if((!Array.isArray(ApiResponse.CommandResponse))||(ApiResponse.CommandResponse.length != 1)) {
   	    		console.error("Invalid response structure with CommandResponse")
   	    		throw new MError("INVALID_UPSTREAM_RESPONSE")
   	    	}
   	    	var cr = ApiResponse.CommandResponse[0]
   	    	var k = Object.keys(cr)
   	    	if((k.length == 2)&&(k[0]=='$')) {
   	    		cr = cr[k[1]]
   	    		if((Array.isArray(cr))&&(cr.length == 1))
   	    			cr = cr[0]
   	    	}
   	    	return Promise.resolve(cr)
   	    })
   }



  function dealWithClientIp(){
    if(app.clientIp) return
    app.clientIp = app.config.get("client_ip_for_namecheap")
    if(app.clientIp) return
	app.clientIp = "-"
	return app.PostForm('https://api.ipify.org')
	  .catch(ex=>{
	  	 console.error("Unable to contact ipify", ex);
	  	 return app.PostForm("https://4.ipcfg.me", null, {headers:{"User-Agent": "curl"}});
	  })
	  .then(x=>{
	  	  app.clientIp = x
	  	  console.log("Client IP aquired as", app.clientIp)
	  })
	  .catch(ex=>{
	  	  console.error("Couldnt resolve client IP", ex)
	  })
   }

}
