var lib = module.exports = function(app, knex){

    var router = app.ExpressPromiseRouter({mergeParams: true})

    const purchasesLib = require("lib-purchases.js")
    const certsLib = require("lib-certs.js")

    const dotq = require("MonsterDotq")
    var ValidatorsLib = require("MonsterValidators")

    router.route("/:webhosting_id/move")
      .post(function(req,res,next){
            var success = 0;

            return vali.async(req.body.json, {
                dest_wh_id: {presence: true, isInteger: {lazy:true}},
                dest_u_id: {presence: true, isValidUserId: { info: app.MonsterInfoAccount }},
                domain: {presence: true, isString: true},
            })
            .then(d=>{
                d = ad;
                return knex.transaction(trx=>{
                   var certs = certsLib(app, trx);
                   var purchases = purchasesLib(app, trx);

                   return certs.ListByWebhosting(req.params.webhosting_id)
                    .then(certs=>{
                        return dotq.linearMap({array: certs, action: cert=>{
                           var purchase;
                           if(cert.c_subject_common_name != d.domain) {
                              console.log("skipping relocation of certificate with subject", cert.c_subject_common_name);
                              return;
                           }
                           console.log("relocating certificate with subject", cert.c_subject_common_name);
                           return purchases.GetPurchaseById(cert.c_purchase)
                             .then(apurchase=>{
                                 purchase = apurchase;
                                 return purchase.UpdateParams({pc_user_id: d.dest_u_id})
                             })
                             .then(()=>{
                                return trx.raw("UPDATE activations SET a_user_id=?, a_payload=REPLACE(a_payload,?,?) WHERE a_purchase=?", [dest.u_id, '"'+req.params.webhosting_id+'"', '"'+d.dest_wh_id+'"', purchase.pc_id]);
                             })
                             .then(()=>{
                                // this is to trigger execution of the update above
                                success++;
                             })

                        }})
                    })

                })        

            })
            .then(()=>{
               return req.sendResponse({success: success});
            })
      })

    router.route("/:webhosting_id")
      .delete(function(req,res,next){

        return req.sendOk(
             app.MonsterInfoWebhosting.GetInfo(req.params.webhosting_id)
              .then(wh=>{

                  return knex.transaction(trx=>{
                     var purchases = purchasesLib(app, trx);

                     return purchases.DeleteLocalPurchases(wh)
                       .then(()=>{
                          var certs = certsLib(app, trx);
                          return certs.DetachLocalCertificates(wh.wh_id);
                       })
                  })

              })
        );

      })



    return router


}
