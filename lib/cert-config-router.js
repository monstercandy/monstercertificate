var lib = module.exports = function(app, knex){

	const contactsLib = require("lib-contacts.js")
	var contacts = contactsLib(app, knex)

    var router = app.ExpressPromiseRouter({mergeParams: true})
    var nc = app.resellers.namecheap

    const MError = app.MError


    router.get("/countries", function(req,res,next){

        const misc = require("lib-misc.js")

        return req.sendResponse(misc.GetCountries())

      })

    router.route("/contacts/default")
       .get(function(req,res,next){
          return req.sendResponse(contacts.GetDefault())
        })
       .post(function(req,res,next){
          return req.sendOk(contacts.SetDefault(req.body.json))
        })
       .delete(function(req,res,next){
          return req.sendOk(contacts.ClearDefault())
        })

    router.get("/resellers", function(req,res,next){

        return req.sendResponse({resellers:Object.keys(app.resellers)})

      })

    router.get("/resellers/:reseller/vendors", function(req,res,next){

        return req.sendResponse({vendors:app.resellers[req.params.reseller].GetVendors()})

      })

    router.post("/resellers/:reseller/vendors/products", function(req,res,next){

        return req.sendResponse({products:app.resellers[req.params.reseller].GetProductsOfVendor(req.body.json.vendor)})

      })
    router.get("/resellers/:reseller/vendors/:vendor/products", function(req,res,next){

        return req.sendResponse({products:app.resellers[req.params.reseller].GetProductsOfVendor(req.params.vendor)})

      })


    router.get("/balances", function(req,res,next){
        return req.sendPromResultAsIs(getResellerBalances())

      })


    router.get("/namecheap/balance", function(req,res,next){

        return req.sendPromResultAsIs(nc.GetBalanceFull())

      })

    router.route("/namecheap/credentials")
       .get(function(req,res,next){
       	  return req.sendResponse(nc.GetCredentials())
       	  // return UserName and ApiUser
        })
       .post(function(req,res,next){
       	  // save UserName, ApiKey and ApiUser to the volume config
       	  return req.sendOk(nc.SetCredentials(req.body.json))
        })
       .delete(function(req,res,next){
       	  // save UserName, ApiKey and ApiUser to the volume config
       	  return req.sendOk(nc.ClearCredentials())
        })


    install_cron()


    return router




  function install_cron(){
    if(app.cron_balance_poll) return
    app.cron_balance_poll = true

    var csp = app.config.get("cron_balance_poll")
    if(!csp) return

    const cron = require('Croner');

    cron.schedule(csp, function(){
        return getResellerBalances()
          .then(balances=>{

              var violations = {}

              var balance_warnings = app.config.get("balance_warnings")

              Object.keys(balances).forEach(resellerName=>{
                 var v = balances[resellerName]
                 var c = v.Currency
                 if(!c) return

                 var b = v.AvailableBalance

                 var k = resellerName+"-"+c


                 var min = balance_warnings[k] || balance_warnings[c]
                 if(!min) return
                 if(b <= min) {
                   violations[resellerName] = b + " " + c
                 }
              })

              if(Object.keys(violations).length) {
                 console.warn("Reseller balances too low:", violations)
                 app.InsertException(null, {message: "BALANCE_TOO_LOW", violations: violations});
              }


          })
    });

  }



   function getResellerBalances(){
        var re = {}
        var ps = []
        Object.keys(app.resellers).forEach(resellerName=>{
           var reseller = app.resellers[resellerName]
           if(!reseller.GetBalance) return
           ps.push(
             reseller.GetBalance(true)
               .then(x=>{
                 if(!x) return;
                 re[resellerName] = x
               })
               .catch(ex=>{
                 console.error("Couldnt fetch balance:", resellerName, ex)
               })
            )
        })
        return Promise.all(ps)
          .then(()=>{
             return Promise.resolve(re)
          })
   }

}
