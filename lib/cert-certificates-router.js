var lib = module.exports = function(app, knex){

	const certificatesLib = require("lib-certs.js")
	var certificates = certificatesLib(app, knex)

    var router = app.ExpressPromiseRouter({mergeParams: true})

    router.use(function(req,res,next){
    	req.filters = {}
    	if(req.params.webhosting_id)
    	   req.filters.c_webhosting = req.params.webhosting_id
    	if(req.params.user_id)
    	   req.filters.c_user_id = req.params.user_id

    	next()
    })

    function findCertById(req,res,next){
        return certificates.GetItemBy(extend({c_id:req.params.cert_id}, req.filters))
          .then(cert=>{
             req.cert = cert
             next()
          })
        }

    router.post("/item/:cert_id/enable", findCertById, function(req,res,next){
       return req.sendOk(req.cert.Enable())
    })
    router.post("/item/:cert_id/disable", findCertById, function(req,res,next){
       return req.sendOk(req.cert.Disable())
    })
    router.post("/item/:cert_id/attach", findCertById, function(req,res,next){
       return req.sendOk(req.cert.AttachToWebhosting(req.body.json.c_webhosting))
    })
    router.post("/item/:cert_id/detach", findCertById, function(req,res,next){
       return req.sendOk(req.cert.DetachFromWebhosting())
    })
    router.post("/item/:cert_id/alias", findCertById, function(req,res,next){
       return req.sendOk(req.cert.SetAlias(req.body.json.alias))
    })
    router.post("/item/:cert_id/raw", findCertById, function(req,res,next){
       return req.sendOk(req.cert.UpdateParams(req.body.json))
    })

    router.put("/item/:cert_id/private-key", findCertById, function(req,res,next){
       return req.sendOk(req.cert.UploadPrivateKey(req.body.json.privateKey))
    })
    router.put("/item/:cert_id/intermediates", findCertById, function(req,res,next){
       return req.sendOk(req.cert.UploadIntermediates(req.body.json.intermediates))
    })

    router.post("/item/:cert_id/settled", findCertById, function(req,res,next){
       return req.sendOk(req.cert.SetSettled(req.body.json.settled))
    })

    function readCert(req,res,next){
      return req.cert.Read()
        .then(function(){
           return next()
        })
    }

    router.get("/item/:cert_id/full", findCertById, readCert, function(req,res,next){
        return req.sendResponse(req.cert)
      })

    router.route("/item/:cert_id")
      .all(findCertById)
      .delete(function(req,res,next){
         return req.sendOk(req.cert.Wipe())
      })
      .get(readCert, function(req,res,next){
        return req.sendResponse(req.cert.Cleanup())
      })

    router.post("/search", canBeAdded, function(req,res,next){

        return req.sendWithCanBeAdded(
               // note req.filters at the end, meaning route parameters will override json ones
             certificates.ListBy(extend({}, req.body.json, req.filters))
        )

    })

    router.route("/")
      .put(queryWebhosting, function(req,res,next){

        return req.sendPromResultAsIs(
             certificates.Insert(extend({}, req.body.json, req.filters), req)
        )
      })
      .get(canBeAdded, function(req,res,next){

        return req.sendWithCanBeAdded(
             certificates.ListBy(req.filters)
               .then(certs=>{
                  certs.Cleanup()
                  return Promise.resolve(certs)
               })
        )
      })
      .delete(function(req,res,next){

        return certificates.ListBy(req.filters)
          .then(certs=>{
             var ps = []
             certs.forEach(cert=>{
                ps.push(cert.DetachFromWebhosting())
             })
             return Promise.all(ps)
          })
          .then(()=>{
            return req.sendOk()
          })
      })

    return router

    function canBeAdded(req,res,next){
        var re = {canBeAdded: true}
        req.sendWithCanBeAdded = function(prom){
            return req.sendPromResultAsIs(prom
             .then(d=>{
                re.certificates = d.Cleanup()
                // console.log("in hre", re)
                return Promise.resolve(re)
            }))
        }
        return certificates.canBeAdded(req.filters)
          .catch(x=>{
             re.canBeAdded = false
          })
          .then(()=>{
             return next()
          })
    }


    function queryWebhosting(req,res,next) {
        if(!req.params.webhosting_id) return next()

        return app.MonsterInfoWebhosting.GetInfo(req.params.webhosting_id)
          .then(webhostingInfo=>{
              req.webhosting = webhostingInfo
              req.body.json.c_user_id = req.webhosting.wh_user_id

              next()
          })
    }


}
