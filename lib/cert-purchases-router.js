var lib = module.exports = function(app, knex){

	const purchasesLib = require("lib-purchases.js")
	var purchases = purchasesLib(app, knex)

    var router = app.ExpressPromiseRouter({mergeParams: true})

    router.use(function(req,res,next){
    	req.filters = {}
    	if(req.params.user_id)
    	   req.filters.pc_user_id = req.params.user_id

    	next()
    })

    router.route("/item/:purchase_id/activation")
       .all(findPurchaseById)
       .get(function(req,res,next){

          return req.purchase.GetActivation()
            .then(activation=>{
               req.sendResponse(activation.Cleanup())
            })
       })

    router.route("/item/:purchase_id/activations/raw")
       .put(findPurchaseById, function(req,res,next){
          // this one will be used by the migration script
          return req.sendPromResultAsIs(req.purchase.InsertActivation(req.body.json, req.query.ts))
       })


    router.route("/item/:purchase_id/activate/task")
       .post(findPurchaseById, function(req,res,next){
          return req.purchase.Activate(req.body.json, req.query.ts, req, true)
       })

    router.route("/item/:purchase_id/activate")
       .post(findPurchaseById, function(req,res,next){
          return req.sendPromResultAsIs(req.purchase.Activate(req.body.json, req.query.ts, req))
       })

    router.route("/item/:purchase_id/activations")
       .all(findPurchaseById)
       .get(function(req,res,next){

          return req.purchase.ListActivations()
            .then(activations=>{
               req.sendResponse({activations:activations.Cleanup()})
            })
       })
       .put(function(req,res,next){
          return req.sendPromResultAsIs(req.purchase.Activate(req.body.json, req.query.ts, req))
       })

    router.route("/item/:purchase_id/poll")
      .post(findPurchaseById, function(req,res,next){
          return req.sendOk(req.purchase.Poll(req.body.json, req.query.ts))
      })

    router.route("/item/:purchase_id/revoke")
      .post(findPurchaseById, function(req,res,next){
          return req.sendOk(
             req.purchase.Revoke(req.body.json, req.query.ts)
             .then(()=>{
                app.InsertEvent(req, {e_event_type:"revoke", e_other:true});
             })
          )
      })

    router.route("/item/:purchase_id/reissue/task")
      .post(findPurchaseById, function(req,res,next){
          return req.purchase.Reissue(req.body.json, req.query.ts, req, true)
      })

    router.route("/item/:purchase_id/reissue")
      .post(findPurchaseById, function(req,res,next){
          return req.sendPromResultAsIs(req.purchase.Reissue(req.body.json, req.query.ts, req))
      })

    router.route("/item/:purchase_id/raw")
      .post(findPurchaseById, function(req,res,next){
          return req.sendOk(
             req.purchase.UpdateParams(req.body.json)
             .then(()=>{
                app.InsertEvent(req, {e_event_type:"certificate-change", e_other:true});
             })
          )
      })
    router.route("/item/:purchase_id/dispatch")
      .post(findPurchaseById, function(req,res,next){
          return req.purchase.Dispatch(req.body.json.method, req.body.json)
            .then(r=>{
                app.InsertEvent(req, {e_event_type:"dispatch", e_other:true});
                return req.sendResponse(r);
            });
      })
    router.route("/item/:purchase_id/dispatch/:method")
      .post(findPurchaseById, function(req,res,next){
          return req.purchase.Dispatch(req.params.method, req.body.json)
            .then(r=>{
                app.InsertEvent(req, {e_event_type:"dispatch", e_other:true});
                return req.sendResponse(r);
            });
      })
    router.route("/item/:purchase_id/renew")
      .post(findPurchaseById, function(req,res,next){
          return req.purchase.Renew(req.body.json)
            .then(r=>{
                app.InsertEvent(req, {e_event_type:"renew", e_other:true});
                req.sendResponse(r);
            })
      })

    router.route("/item/:purchase_id")
      .get(findPurchaseById, function(req,res,next){
          return req.sendResponse(req.purchase.Cleanup())
      })
      .delete(findPurchaseById, function(req,res,next){
          return req.sendOk(
              req.purchase.Delete()
              .then(()=>{
                  app.InsertEvent(req, {e_event_type:"purchase-removed", e_other:true});
              })
          )
      })

    router.post("/letsencrypt/renew", function(req,res,next){
          var emitter = app.commander.EventEmitter()
          return emitter.spawn()
            .then(h=>{
               req.sendTask(Promise.resolve(h))
               return purchases.LetsencryptRenewWhereExpirationIsClose(emitter)
            })
            .then(()=>{
               return emitter.close()
            })
            .catch(ex=>{
               console.error("Error while renewing certs", ex)
               emitter.send_stdout_ln("Error while renewing certs")
               return emitter.close(1)
            })
      })

    router.route("/letsencrypt")
      .put(function(req,res,next){
          return purchases.LetsencryptInsertAndActivate(extend({},req.body.json, req.filters), req.query.ts, req)
      })

    router.route("/poll")
      .post(function(req,res,next){
          purchases.PollAll(req)
      })

    router.post("/remove/expired", function(req,res,next){
       return req.sendPromResultAsIs(purchases.RemoveExpiredPurchases())
    })

    router.post("/notify/expired", function(req,res,next){
       return req.sendOk(purchases.NotifyAboutExpiredPurchases())
    })

    router.post("/turndatecoming",function(req,res,next){
         return purchases.ListByTurndate(req)
            .then(purchases=>{
               purchases.Cleanup()

               return req.sendResponse({purchases: purchases})
            })
      })


    router.route("/")
      .put(function(req,res,next){

        return req.sendPromResultAsIs(
             purchases.Insert(extend({}, req.body.json, req.filters), req.query.ts)
        )
      })
      .get(function(req,res,next){

        return req.sendPromResultAsIs(purchases.ListBy(req.filters)
            .then(purchases=>{
               purchases.Cleanup()

               return Promise.resolve({purchases: purchases})
            })
        )

      })



    return router

    function findPurchaseById(req,res,next){
        return purchases.GetPurchaseBy({pc_id: req.params.purchase_id})
          .then(purchase=>{
              req.purchase = purchase
              return next()
          })
    }



}
