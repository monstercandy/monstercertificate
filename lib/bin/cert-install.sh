#!/bin/bash

set -e

. /opt/MonsterCommon/lib/common.inc

mkdir -p "/var/lib/monster/$INSTANCE_NAME/store/archive"
mkdir -p "/var/lib/monster/$INSTANCE_NAME/store/active"
mkdir -p "/var/lib/monster/$INSTANCE_NAME/letsencrypt/etc"
mkdir -p "/var/lib/monster/$INSTANCE_NAME/letsencrypt/var"

mkdir -p "/var/log/monster/$INSTANCE_NAME"
mkdir -p "/var/log/monster/$INSTANCE_NAME/letsencrypt"
