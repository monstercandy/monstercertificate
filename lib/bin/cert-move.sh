#!/bin/bash

OLD_WH=$1
DOMAIN=$2
DEST_U_ID=$3
DEST_WH_ID=$4

if [ -z "$DEST_WH_ID" ]; then
   echo "Usage: $0 old_wh_id domain dest_u_id dest_wh_id"
   exit 1
fi

/opt/client-libs-for-other-languages/internal/perl/generic-light.pl cert POST "/cert/cleanup/$OLD_WH/move" \
   "{\"domain\":\"$DOMAIN\",\"dest_wh_id\":$DEST_WH_ID,\"dest_u_id\": \"$DEST_U_ID\"}"
