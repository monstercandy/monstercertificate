var lib = module.exports = function(app, knex){

    const pemMimeType = "application/x-pem-file"

    const moment = require("MonsterMoment")
    const path = require("path")
    const dotq = require("MonsterDotq")
    const fs = dotq.fs()

    const Token = require("Token")
    const KnexLib = require("MonsterKnex")

    var ValidatorsLib = require("MonsterValidators")
    var Common = ValidatorsLib.array()
    var vali = ValidatorsLib.ValidateJs()

    const MError = app.MError

    var resellers = app.resellers

    var resellerNames = Object.keys(resellers)


    var valiHelper = {}
    valiHelper.isValidVendor = function(reseller, value, attributes) {
      var vendors = reseller.GetVendors()
      if(vendors.indexOf(value) < 0) return "invalid vendor"
    }
    valiHelper.isValidProduct = function(reseller, value, attributes) {
      var products = reseller.GetProductsOfVendor(attributes.pc_vendor)
      if(products.indexOf(value) < 0) return "invalid product"
    }

    Array("isValidCertificateId", "isValidProduct", "isValidVendor").forEach(category=>{
      vali.validators[category] = function(value, options, key, attributes, glob) {
         if(!value) return
         var reseller = getReseller(attributes)
         if(!reseller) return "no reseller"

         if(valiHelper[category])
           return valiHelper[category](reseller, value, attributes)
         else
         if(!reseller[category](value, attributes))
            return "invalid certificate id"
      }

    })


    var re = {}

    re.ListByTurndate = function(){
       var f = {sqlFilterStr: "0=1", sqlFilterValues: []}
       var daysHash = {}
       Object.keys(resellers).forEach(resellerName=>{
          var reseller = resellers[resellerName]
          if(!reseller.GetNumberOfDaysToRenewTurn) return
          var n = reseller.GetNumberOfDaysToRenewTurn()
          if(!daysHash[n]) daysHash[n] = []
          daysHash[n].push(resellerName)
       })

       if(Object.keys(daysHash).length > 0) {
          var now = moment()
          f.sqlFilterStr = "pc_status='issued' AND "
          var p = []
          Object.keys(daysHash).forEach(days=>{
             var rs = []
             daysHash[days].forEach(r=>{
                rs.push("pc_reseller=?")
                f.sqlFilterValues.push(r)
             })
             var rsStr = "("+rs.join(" OR ")+")"
             var expiration = now.add(days, 'days').format("YYYY-MM-DD")
             f.sqlFilterValues.push(expiration)
             p.push("("+rsStr+" AND DATE(pc_end) = ?)")
          })

          f.sqlFilterStr += "("+p.join(" OR ")+")"

       }

       return re.ListByRaw(f)
    }

    re.LetsencryptRenewWhereExpirationIsClose = function(emitter){
       return resellers["letsencrypt"].RenewWhereExpirationIsClose(emitter)
    }

    re.LetsencryptInsertAndActivate = function(in_data, ts, req) {
       return resellers["letsencrypt"].InsertAndActivate(in_data, ts, req)
    }

    re.RemoveExpiredPurchases = function(in_data, req){
       var days = app.config.get("remove_purchase_after_expiration_days")
       if(!days) return Promise.reject(new MError("NOT_CONFIGURED"))

       var now = moment()
       var expiration = now.add(-1*days, 'days').format("YYYY-MM-DD")
       var f = {sqlFilterStr: "pc_status='issued' AND DATE(pc_end)=?", sqlFilterValues: [expiration]}

       return knex("purchases").update({pc_status:"removed"}).whereRaw(f.sqlFilterStr, f.sqlFilterValues).then(affected=>{
           var r = {affected: affected};
           if(affected)
              app.InsertEvent(req, {e_event_type:"purchase-remove-expired", e_other:r});
           return r;
       })
    }

    re.NotifyAboutExpiredPurchases = function(in_data){
       var days = app.config.get("notify_about_expired_purchase_after_days")
       if(!days) return Promise.reject(new MError("NOT_CONFIGURED"))

       var now = moment()
       var expiration = now.add(-1*days, 'days').format("YYYY-MM-DD")
       var f = {sqlFilterStr: "pc_status='issued' AND DATE(pc_end)=?", sqlFilterValues: [expiration]}

       return re.ListByRaw(f)
         .then((purchases)=>{
            var mailer = app.GetMailer()

            return Promise.map(purchases, purchase=>{
                 var x = {}
                 x.toAccount = {user_id: purchase.pc_user_id, emailCategory: "TECH"}
                 x.template = "cert/expired-purchase"
                 x.context = {purchase: purchase}

                 return mailer.SendMailAsync(x)
            })

         })
    }

    re.Insert = function(in_data, ts) {

        var d
        return vali.async(in_data, {
                pc_user_id: {presence: true, isString: true, isValidUserId: {info: app.MonsterInfoAccount}},
                pc_status:{inclusion:["pending","processing","finalizing","issued"], default: "pending"},
                pc_reseller: {presence: true, inclusion: resellerNames},
                pc_vendor: {presence: true, isValidVendor: true},
                pc_product: {presence: true, isValidProduct: true},
                pc_reseller_certificate_id: {isString: {lazy:true}, isValidCertificateId: true},
                pc_product_parameters: {isSimpleObject: true, default: {value:{}}},
                pc_misc: {isSimpleObject: true, default: {value:{}}},

                pc_comment: {isString: true, default: {value:""}},

                pc_local: {isBooleanLazy: true},

                pc_certificate: {isString: {strictName: true}},
                pc_subject_common_name: {isString: true},
                pc_begin: {isString: true},
                pc_end: {isString: true},

        })
        .then(ad=>{
            d = ad

            var cn = d.pc_user_id + " " + d.pc_status + " " + d.pc_reseller + " " + d.pc_vendor + " " + d.pc_product + " " + d.pc_reseller_certificate_id
            return Token.GetTokenAsync(8, "hex") // cn, cn, ts,
        })
        .then(id=>{
            d.pc_id = id

            if(process.env.SKIP_REAL_PURCHASE) return Promise.resolve()

            var reseller = getReseller(d)
            return reseller.Buy(d)

        })
        .then((additionalParams)=>{

            if(additionalParams)
               extend(d, additionalParams.purchase)

            Array("pc_product_parameters","pc_misc").forEach(x=>{
               d[x] = JSON.stringify(d[x])
            })

            return knex.insert(d).into("purchases")
        })
        .then(()=>{

            if(!in_data.skip_email) {

                 var mailer = app.GetMailer()
                 var x = {}
                 x.toAccount = {user_id: d.pc_user_id, emailCategory: "TECH"}
                 x.template = "cert/purchase-to-activate"
                 x.context = {purchase: d}

                 // note we fire this promise unhandled
                 mailer.SendMailAsync(x)

            }

            return Promise.resolve({pc_id: d.pc_id})
        })


    }

    re.GetPurchaseById = function(pc_id){

        return re.GetPurchaseBy({pc_id:pc_id})
    }

    re.GetPurchaseBy = function(filterHash){

        filterHash.pc_status = "-" // we want to retrieve removed ones as well

        return re.ListBy(filterHash)
          .then(purchases=>{
             return Common.EnsureSingleRowReturned(purchases, "PURCHASE_NOT_FOUND")
          })

    }

    re.ListByRaw=function(f, joinActivation){
        var k = knex("purchases").select();
        if(joinActivation)
           k = k.leftJoin("activations", "a_id", "pc_activation")

        return k.whereRaw(f.sqlFilterStr, f.sqlFilterValues)
          .then(rows=>{

                  rows.map(row => purchaseRowToObject(row))
                  Common.AddCleanupSupportForArray(rows)

                  return Promise.resolve(rows)
          })

    }

    re.ListBy=function(filterHash){

        if(typeof filterHash.pc_status == "undefined")
            filterHash["!pc_status"] = "removed"
        else if(filterHash.pc_status == "-")
            delete filterHash.pc_status

        var f = KnexLib.filterHash(filterHash)
        return re.ListByRaw(f)
    }


    re.PollAll = function(req){
         var emitter = app.commander.EventEmitter()
         return emitter.spawn()
           .then(h=>{
              // the cronned invocation has no request...
              if(req) req.sendTask(Promise.resolve(h))

              return re.ListBy(extend({}, req ? req.filters : {}, {pc_status: "processing"}))
           })
           .then(purchases=>{
              doNext()

              function doNext(){
                 var purchase = purchases.shift()
                 if(!purchase) return emitter.close()

                 emitter.send_stdout_ln("Working on purchase "+purchase.pc_subject_common_name)

                 return Promise.resolve()
                   .then(()=>{
                      return purchase.Poll()
                   })
                   .catch(ex=>{
                      // we swallow this one here as noone cares
                      if(ex.message != "NOT_YET_READY")
                         console.warn("Unable to poll purchase", purchase, ex)
                   })
                   .then(()=>{
                      return doNext()
                   })
              }
           })

    }

    re.RestoreBackup = function(purch){
        // console.log("restorebackup", purch)

        return knex.transaction(trx=>{

          return trx.insert(purch.purchase).into("purchases")
            .then(()=>{

                return trx.insert(purch.activation).into("activations")
            })
        })

    }

    re.DeleteLocalPurchases = function(wh, vendor){
       return re.FindLocalPurchases(wh, vendor)
         .then(pcs=>{
            return dotq.linearMap({array: pcs, action: function(pc){
                return pc.Delete();
            }})
         })
    }

    re.FindLocalPurchases = function(wh, vendor){
       var this_server = app.config.get("mc_server_name");
       var f = {
          sqlFilterStr: "pc_status='issued' AND pc_user_id=? AND a_payload LIKE ? AND (a_payload LIKE ? OR a_payload LIKE ?)", //
          sqlFilterValues: [wh.wh_user_id, '%"server":"'+this_server+'"%', '%"webhosting_id":'+wh.wh_id+',%', '%"webhosting_id":"'+wh.wh_id+'"%'],//
       };

       if(vendor) {
          f.sqlFilterStr += " AND pc_vendor=? ";
          f.sqlFilterValues.push(vendor);
       }

       //console.log(f);
       return re.ListByRaw(f, true)
    }

    re.FindLocalLetsEncryptPurchases = function(wh){
       return re.FindLocalPurchases(wh, "Let's Encrypt");
    }


    install_cron1()
    install_cron2()

    return re



  function install_cron1(){
    if(app.cron_purchase_poll) return
    app.cron_purchase_poll = true

    var csp = app.config.get("cron_purchase_poll")
    if(!csp) return

    const cron = require('Croner');

    cron.schedule(csp, function(){
        return re.PollAll()
    });

  }

  function install_cron2(){
    if(app.cron_remove_expired_purchases) return
    app.cron_remove_expired_purchases = true

    var csp = app.config.get("cron_remove_expired_purchases")
    if(!csp) return

    const cron = require('Croner');

    cron.schedule(csp, function(){
        return re.RemoveExpiredPurchases()
    });

  }


    function purchaseRowToObject(purchase){

        var reseller = getReseller(purchase)

        purchase.pc_product_parameters = JSON.parse(purchase.pc_product_parameters)
        purchase.pc_misc = JSON.parse(purchase.pc_misc)

        if(purchase.pc_end) {
          var now = moment()
          var pcend = moment(purchase.pc_end)
          purchase.isExpired = ((purchase.pc_status == "issued") && (now.isAfter(pcend)))

          if(purchase.pc_status == "issued")
            purchase.canBeRenewed = reseller.ShouldRenewBeBought(purchase, now, pcend)
        }

        purchase.actions = []
        if(reseller["GetActions_"+purchase.pc_status])
           purchase.actions = reseller["GetActions_"+purchase.pc_status](purchase) || []

        purchase.Cleanup = function(){

            // pc_reseller, pc_reseller_certificate_id should be hidden through the business logic layer when needed
            return purchase
        }

        purchase.Delete = function(){
            return knex("purchases").update({pc_status:"removed"}).whereRaw("pc_id=?", [purchase.pc_id])
        }

        purchase.ListActivations = function(extraFilters){
            if(!extraFilters) extraFilters = {}
            extraFilters.a_purchase = purchase.pc_id

            var f = KnexLib.filterHash(extraFilters)

            return knex("activations").select().whereRaw(f.sqlFilterStr, f.sqlFilterValues)
              .then(rows=>{
                  rows.map(row => activationRowToObject(row, purchase))
                  Common.AddCleanupSupportForArray(rows)

                  return Promise.resolve(rows)
              })
        }
        purchase.GetActivationById = function(a_id){
            return purchase.ListActivations({a_id: a_id})
              .then(rows=>{
                 return Common.EnsureSingleRowReturned(rows, "ACTIVATION_NOT_FOUND")
              })
        }
        purchase.GetActivation = function(){
          if(!purchase.pc_activation) return Promise.reject(new MError("NOT_ACTIVATED"))
          return purchase.GetActivationById(purchase.pc_activation)
        }
        purchase.SetStatus= function(paramsToChange, activation){
            return purchase.UpdateParams(paramsToChange)
              .then(()=>{
                 if(paramsToChange.pc_status != "issued") return Promise.resolve()

                 if(activation) return Promise.resolve(activation)

                 return purchase.GetActivation()
              })
              .then((activation)=>{
                    if((!activation)||(!activation.a_payload)||(!activation.a_payload.attach)) return Promise.resolve()

                    return app.GetRelayerMapiPool().postAsync(
                       "/s/"+activation.a_payload.attach.server+
                       "/docrootapi/docroots/"+activation.a_payload.attach.webhosting_id+
                       "/"+activation.a_payload.attach.docroot_domain+"/certificate",
                       {certificate_id: paramsToChange.pc_certificate}
                    )

              })
        }
        purchase.UpdateParams = function(params) {

            console.log("Updating certificate parameters", purchase, "params", params);

            return knex("purchases").update(params).whereRaw("pc_id=?",[purchase.pc_id]).then(()=>{
                Object.keys(params).forEach(k=>{
                    purchase[k] = params[k]
                })
                return Promise.resolve()
            })

        }
        purchase.Poll = function(){
           if(purchase.pc_status != "processing") throw new MError("INVALID_STATUS")
           return getReseller(purchase).Poll(purchase)
        }

        purchase.Revoke = function(in_data){
           if(purchase.pc_status != "issued") throw new MError("INVALID_STATUS")
           return getReseller(purchase).Revoke(purchase, in_data)
              .then(x=>{
                  return purchase.UpdateParams({pc_status: "revoked"})
              })

        }
        purchase.Renew = function(in_data){
           if(purchase.pc_status != "issued") throw new MError("INVALID_STATUS")
            var candidate = {}
            Array("pc_user_id","pc_reseller","pc_product","pc_vendor","pc_subject_common_name","pc_misc").forEach(x=>{
              candidate[x] = purchase[x]
            })
            candidate.pc_status = "pending"
            candidate.pc_comment = "Renewed for "+purchase.pc_id
            candidate.pc_product_parameters = {}

           return getReseller(purchase).Renew(purchase, candidate, in_data)
              .then(x=>{
                  return re.Insert(candidate)
              })
              .then((res)=>{
                  return purchase.UpdateParams({pc_status: "renewed"})
                    .then(()=>{
                       return Promise.resolve(res)
                    })
              })
        }

        Array("Activate","Reissue").forEach(command=>{

            purchase[command] = function(in_data, ts, req, asTask) {

                 if((command == "Activate")&&(["pending"].indexOf(purchase.pc_status) < 0))//,"processing"
                    throw new MError("INVALID_STATUS")

                 if((command == "Reissue")&&(purchase.pc_status != "issued"))
                    throw new MError("INVALID_STATUS")

                 var emitter;

                 var a_id
                 var mainActivationParams
                 var localMode
                 var p
                 if((Object.keys(in_data || {}).length == 0)&&(command == "Reissue")) {
                    p = purchase.GetActivation()
                      .then(activation=>{
                         in_data = activation.a_payload
                      })
                 } else {
                    p = Promise.resolve()
                 }

                return p
                .then(()=>{
                    var reseller = getReseller(purchase)

                    localMode = in_data.local ? true : false

                    return vali.async(in_data, {
                      local: {isBoolean:true},
                      attach:{isSimpleObject:true},
                      domains:{presence:localMode, isArray:{minLength:1},isHostArray: {returnCanonical:true}}
                    })

                })
                .then(ad=>{
                   mainActivationParams = ad

                    if(mainActivationParams.attach)
                      return vali.async(mainActivationParams.attach, {
                        server: {presence: true, isString: {strictName: true}},
                        webhosting_id: {presence: true, isInteger: true},
                        docroot_domain: {presence: true, isHost: {returnCanonical:true}},
                      })

                    return Promise.resolve()
                })
                .then(ad=>{
                     if(ad)
                        mainActivationParams.attach = ad

                     var fn = reseller["doActivate"+(localMode ? "Local" : "Generic")]


                     var p;
                     if(asTask){
                        emitter = app.commander.EventEmitter()
                        p = emitter.spawn().then(h=>{
                           req.sendTask(Promise.resolve(h))
                        })
                     } else
                       p = Promise.resolve();


                     return p.then(()=>{
                         if(emitter) emitter.send_stdout_ln(command+" in progress");
                         return fn(command, purchase, mainActivationParams, in_data, function(aResellerParams){
                            return Promise.resolve()
                              .then(()=>{

                                 return purchase.UpdateParams({pc_local: localMode}, aResellerParams.purchase)

                              })
                              .then(()=>{

                                 var payload = extend({}, mainActivationParams, aResellerParams.activationParameters)
                                 var extra = aResellerParams.extraParameters || {}

                                 var d = extend({
                                  a_payload:JSON.stringify(payload),
                                  a_extra: JSON.stringify(extra)
                                 }, aResellerParams.activation)


                                 return purchase.InsertActivation(d)
                              })
                              .then(d=>{
                                 a_id = d.a_id
                                 return activationRowToObject(d, purchase)
                              })

                         }, emitter)

                     })
                  })
                  .then(()=>{
                     if(emitter) emitter.send_stdout_ln(command+" succeeded.");
                  })
                  .catch(ex=>{
                     console.error("Error during activation", ex)
                     if(emitter) {
                       emitter.send_stdout_ln("Error during activation");
                       emitter.close(1);
                     }
                     throw ex;
                  })
                  .then(()=>{
                    var re = {a_id: a_id, pc_id: purchase.pc_id}
                    if(purchase.pc_certificate) re.c_id= purchase.pc_certificate

                    app.InsertEvent(req, {e_event_type: command.toLowerCase(), e_other:true});
                    if(emitter)
                       emitter.close();
                    return re
                  })
            }


        })

        purchase.GetCertificate = function(){
            const certLib = require("lib-certs.js")(app, knex)
            return certLib.GetCertificateById(purchase.pc_certificate);
        }


        purchase.InsertActivation = function(in_data){
            return Token.GetTokenAsync(8, "hex")
                .then(id=>{
                   in_data.a_id = id
                   in_data.a_user_id = purchase.pc_user_id
                   in_data.a_purchase = purchase.pc_id

                   return knex.insert(in_data).into("activations").return(in_data)
                })

        }

          purchase.Dispatch=function(method, in_data) {
             var reseller = getReseller(purchase)
             return vali.async({method: method}, {method: {presence:true, isString: {strictName: true}}})
               .then(d=>{
                  var k = "dispatch_"+d.method
                  if(!reseller[k])
                    throw new MError("INVALID_METHOD")
                  return reseller[k](purchase, in_data)
               })
          }


        purchase.activateLocal=function(d, in_data){

            const openssl = require("lib-openssl.js")(app)

            var complete = {}

            var mainDomain = d.domains[0]

            var extraParams = {}

            var contacts = app.getContacts()
            return contacts.GetDefaultContactObject()
              .then(aDefContact=>{
                   complete.Admin = aDefContact

                   return openssl.GenerateRawRSAPrivateKey()
              })
              .then(rsa=>{
                 extraParams.privateKey = rsa
                 return openssl.GenerateCsrForDomains(rsa, complete.Admin, d.domains)
              })
              .then(csr=>{
                 extraParams.csr = complete.csr = csr
                 extraParams.mainDomain = mainDomain

                 return {complete: complete, save:{},  extra: extraParams};
              })


        }


        return purchase
    }


    function activationRowToObject(activation, purchase){

        Array("a_payload", "a_extra", "a_result").forEach(x=>{
            activation[x] = JSON.parse(activation[x]||"{}")
        })

        activation.Cleanup = function(){

            if((activation.a_extra)&&(activation.a_extra.privateKey))
               activation.a_extra.privateKey = "***"

            return activation
        }

        activation.SetStatus = function(newStatus, result){
            return knex("activations").update({
              a_status: newStatus,
              a_result: result ? JSON.stringify(result) : ""
            }).whereRaw("a_id=?",[activation.a_id]).then(()=>{
               activation.a_status = newStatus
               activation.a_result = result
               return Promise.resolve(activation)
            })
        }

        activation.UploadResult = function(issuedRawCertificate){

             var data = extend({}, issuedRawCertificate, {
                c_user_id: purchase.pc_user_id,

                c_purchase: activation.a_purchase,
                c_settled: issuedRawCertificate.privateKey ? true : false,
                c_reseller: purchase.pc_reseller,
                c_product: purchase.pc_product,
                c_vendor: purchase.pc_vendor,
             })

             var p
             if(!issuedRawCertificate.certificate) {
                p = Promise.resolve({})
             }
             else
             if(activation.a_payload.attach) {

                p = app.GetRelayerMapiPool().putAsync(
                   "/s/"+activation.a_payload.attach.server+
                   "/cert/certificates/by-webhosting/"+activation.a_payload.attach.webhosting_id,
                   data
                )
                .then(res=>{
                  // console.log("put has finished !!!", res)
                  return res.result;
                })

             } else {
                const certLib = require("lib-certs.js")(app, knex)
                p = certLib.Insert(data)
             }

             var uploadedCert
             return p
               .then((res)=>{
                  uploadedCert = res
                  console.log("UpdateResult", uploadedCert);
                  return activation.SetStatus("successful")
               })
               .then(()=>{
                  var statusData = {
                      pc_activation: activation.a_id,
                      pc_status: "issued"
                    }
                  var mapping = {"c_id":"pc_certificate", "c_begin":"pc_begin", "c_end":"pc_end", "c_subject_common_name":"pc_subject_common_name"}
                  Object.keys(mapping).forEach(x=>{
                    if(uploadedCert[x])
                      statusData[mapping[x]] = uploadedCert[x]
                  })

                  if(issuedRawCertificate.c_reseller_certificate_id)
                      statusData.pc_reseller_certificate_id = issuedRawCertificate.c_reseller_certificate_id

                  return purchase.SetStatus(statusData, activation)
               })
               .then(response=>{

                  if(issuedRawCertificate.certificate) {

                    var mailer = app.GetMailer()
                    var x = {}
                    x.toAccount = {user_id: activation.a_user_id, emailCategory: "TECH"}
                    x.template = "cert/certificate-enrolled"
                    x.context = {purchase: purchase}
                    x.attachments = []
                    var filename = purchase.pc_subject_common_name.replace("*.", "")
                    if(issuedRawCertificate.certificate)
                       x.attachments.push({
                          name: filename + ".crt",
                          type: pemMimeType,
                          inline: false,
                          data: issuedRawCertificate.certificate
                       })

                    if(issuedRawCertificate.intermediate)
                       x.attachments.push({
                          name: filename + ".intermediate.crt",
                          type: pemMimeType,
                          inline: false,
                          data: issuedRawCertificate.intermediate
                       })

                    // note we fire this promise unhandled
                    mailer.SendMailAsync(x)

                  }

                  return Promise.resolve(response)
               })


        }

        return activation
    }

   function getReseller(purchase) {
      return resellers[purchase.pc_reseller]
   }

}
