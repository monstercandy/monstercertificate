var lib = module.exports = function(app, knex){

	const contactsLib = require("lib-contacts.js")
	var contacts = contactsLib(app, knex)

    var router = app.ExpressPromiseRouter({mergeParams: true})

    router.use(function(req,res,next){
    	req.filters = {}
    	if(req.params.user_id)
    	   req.filters.co_user_id = req.params.user_id

    	next()
    })

    router.route("/item/:contact_id")
      .get(findContactById, function(req,res,next){
          return req.sendResponse(req.contact.Cleanup())
      })
      .delete(findContactById, function(req,res,next){
          return req.sendOk(req.contact.Delete())
      })



    router.route("/")
      .put(function(req,res,next){

        return req.sendPromResultAsIs(
             contacts.Insert(extend({}, req.body.json, req.filters), req.query.ts)
        )
      })
      .get(function(req,res,next){

        return req.sendPromResultAsIs(contacts.ListDefault(req.filters)
            .then(contacts=>{
               contacts.Cleanup()

               return Promise.resolve({contacts: contacts})
            })
        )

      })



    return router

    function findContactById(req,res,next){
        return contacts.GetContactBy({co_id: req.params.contact_id})
          .then(contact=>{
              req.contact = contact
              return next()
          })
    }



}
