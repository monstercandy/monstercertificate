const moment = require("MonsterMoment")
const MError = require("MonsterError")
const cnfCounter = 1
var lib = module.exports = function(app){
    var ValidatorsLib = require("MonsterValidators")
    var vali = ValidatorsLib.ValidateJs()

    function getSpawnParameters(){
    	return [app.config.get("cmd_pathes"), app.config.get("common_params"), ]
    }

	return {
		GenerateCsrForDomains: function(rsaPrivateKey, contact, domains) {

			const tmpromise = require("tmpromise")

			return tmpromise({
				data: rsaPrivateKey,
				pathOptions: {suffix: ".pem"},
				writeOptions:{mode:0600},
				callback: function(pathToRsaPrivateKey) {
   		 	  	    var config = lib.generateOpensslConfForDomains(contact, domains)

					return tmpromise({
						data: config,
						pathOptions: {suffix: ".conf"},
						callback: function(pathToConfigFile) {
					        var chain = simpleCloneObject(app.config.get("cmd_openssl_req_csr"))
					        var params = getSpawnParameters()
					        params.push({path_rsa_private_key: pathToRsaPrivateKey, path_config_file: pathToConfigFile})
							return app.commander.spawn({omitControlMessages:true,chain:chain,removeImmediately:true}, params)
							  .then(h=>{
							  	 return h.executionPromise
							  })
							  .then(x=>{
							  	 return x.output.toString()
							  })
						}
				    })

				}
		    })


		},
		GenerateRawRSAPrivateKey: function(bitLength){
			bitLength = bitLength || app.config.get("rsa_key_length_bit")
	        var chain = simpleCloneObject(app.config.get("cmd_openssl_gen_rsa"))
	        var params = getSpawnParameters()
	        params.push({keylength: bitLength})
            return app.commander.spawn({omitControlMessages:true,omitAggregatedStderr: true,removeImmediately:true, chain: chain}, params)
	          .then(h=>{
	          	 return h.executionPromise
		          .catch(ex=>{
		          	 console.error("Unable to generate RSA private key:", ex)
		          	 throw new MError("GENERATE_FAILED")
		          })
	          })
	          .then(x=>{
	          	 return Promise.resolve(x.output.toString())
	          })

		},
		ParseCertificateSigningRequest: function(csr){

	    	return vali.async({csr: csr}, {csr: {presence: true, isString: true}})
	    	  .then(d=>{
			        var chain = simpleCloneObject(app.config.get("cmd_openssl_csr_text"))
			       	chain.stdin = d.csr

		            return app.commander.spawn({removeImmediately: true, chain: chain}, getSpawnParameters())
	    	  })
	          .then(h=>{
	          	 return h.executionPromise
			          .catch(ex=>{
			          	 console.error("Unable to parse certificate signing request:", ex)
			          	 throw new MError("PARSING_FAILED")
			          })
	          })
	          .then(x=>{
	          	 var r = lib.ParseCsrTextOutput(x.output.toString())
	          	 if(!r["CERTIFICATE REQUEST"]) throw new MError("INVALID_REQUEST")

	          	 return Promise.resolve(r)
	          })
		},

		ParseRSAPrivateKey: function(privateKeyText) {
			// https://blog.hboeck.de/archives/888-How-I-tricked-Symantec-with-a-Fake-Private-Key.html
			// //openssl rsa -in [privatekey] -check

	    	return vali.async({privateKeyText: privateKeyText}, {privateKeyText: {presence: true, isString: true}})
	    	  .then(d=>{
			        var chain = simpleCloneObject(app.config.get("cmd_openssl_rsa_private_text"))
			       	chain.stdin = d.privateKeyText

		            return app.commander.spawn({removeImmediately:true, dontLog_stdout: true, chain: chain}, getSpawnParameters())
	    	  })
	          .then(h=>{
	          	 return h.executionPromise
			          .catch(ex=>{
			          	 console.error("Unable to parse private key:", ex)
			          	 throw new MError("PARSING_FAILED")
			          })
	          })
	          .then(x=>{
	          	 var r = lib.ParseRsaTextOutput(x.output.toString())
	          	 if(!r["RSA PRIVATE KEY"]) throw new MError("INVALID_PRIVATE_KEY")

	          	 return Promise.resolve(r)
	          })

		},

		ParseX509Certificates: function(certificates) {

	    	var cs = lib.SplitX509Certificates(certificates)
	    	if(cs.length <= 0) return Promise.reject(new MError("CHAIN_EMPTY"))
	    	if(cs.length > 5) return Promise.reject(new MError("CHAIN_TOO_LONG"))

	    	var re = []

	        var x509_chain = simpleCloneObject(app.config.get("cmd_openssl_x509_text"))

	        return new Promise(function(resolve,reject){

			    	return processNext()

			        function processNext(){
			        	var acs = cs.shift()
			        	if(!acs)
			        		return resolve(re)

			        	x509_chain.stdin = acs

			            return app.commander.spawn({removeImmediately: true, chain: x509_chain}, getSpawnParameters())
			              .then(h=>{
			              	 return h.executionPromise
			              })
			              .then(x=>{
			              	 var r = lib.ParseX509TextOutput(x.output.toString())
			              	 // console.log("shitt", certificates, x, r)
			              	 if(!r["CERTIFICATE"]) return reject(new MError("INVALID_CERTIFICATE"))
			              	 re.push(r)

			              	 return processNext()
			              })
			              .catch(ex=>{
			              	 console.error("Unable to parse certificates:", acs, ex)
			              	 return reject(new MError("PARSING_FAILED"))
			              })
			        }


	        })


		}
	}

}
lib.ParseCsrTextOutput = function(openSslText){
	var re = {}

    var n = "Subject"
	var v = lib.getDNLine(n, lib.getField(n, openSslText))
	if(v) re[n] = v

    n = "CERTIFICATE REQUEST"
    v = lib.getWrappedText(n, openSslText)
    if(v) re[n] = v

    v = lib.getModulus(openSslText)
    if(v) re["Modulus"] = v

    if(!re.Subject)
     	throw new MError("MISSING_SUBJECT")

    var countryMap = require("lib-misc.js").GetCountries();
    if((typeof re.Subject.C != "string")||(!countryMap[re.Subject.C]))
     	throw new MError("SUBJECT_COUNTRY_MUST_BE_ISO_3166_1")

	return re
}

lib.ParseRsaTextOutput = function(openSslText){

    if(openSslText.indexOf("error") > -1)
    	throw new MError("FAKE_PRIVATE_KEY")

	var re = {}

	var n = "Private-Key"
	var v = lib.getField(n, openSslText)
	if(v)
		re[n] = v

    n = "RSA PRIVATE KEY"
    v = lib.getWrappedText(n, openSslText)
    if(v) re[n] = v

    v = lib.getModulus(openSslText)
    if(v) re["Modulus"] = v

    if(!Object.keys(re).length)
    	throw new MError("INVALID_PRIVATE_KEY")

	return re
}

lib.SplitX509Certificates = function(certificates){
	const splitter = "-----END CERTIFICATE-----"
    var x = certificates.split(splitter)
	var re = []
	x.forEach(c=>{
		c = c.trim()
		if(!c) return

		re.push(c+"\n"+splitter+"\n")
	})
	return re

}

lib.ParseX509TextOutput = function(openSslText){
	var re = {}
	Array("Subject","Issuer").forEach(c=>{
		assignDNLine(c, getField(c))
	})
	Array("Not Before", "Not After").forEach(c=>{
		assignDate(c)
	})
	Array("X509v3 Subject Alternative Name").forEach(c=>{
		assignDNLine(c, getNextLine(c))
	})

    const n = "CERTIFICATE"
    assignFieldValue(n, lib.getWrappedText(n, openSslText))

    assignFieldValue("Modulus", lib.getModulus(openSslText))

    return re


    function assignDNLine(name, text) {
    	var r = lib.getDNLine(name, text)
    	if(!r) return
	    re[name] = r
	    return r
    }

    function assignFieldValue(name, value){
    	if(!value) return
    	re[name] = value
    	return value
    }

    function assignNextLine(name){
    	assignFieldValue(name, getNextLine(name))
    }
    function assignField(name){
    	assignFieldValue(name, getField(name))
    }
    function assignDate(name){
    	var v = getField(name)
    	if(!v) return
    	var d = moment(new Date(v)).toISOString() // toUtcDBISOString()
        re[name] = {"text": v, "iso": d}
    }
    function getField(name){
    	return lib.getField(name, openSslText)
    }
    function getNextLine(name){
    	return lib.getNextLine(name, openSslText)
    }
}

lib.getField = function(name, openSslText){
	var r = new RegExp(name+"\\s*: (.+)")
	var m = r.exec(openSslText)
	if(!m) return
	return m[1]
}
lib.getNextLine= function(name, openSslText){
	var r = new RegExp(name+"\\s*:[^]\\s*(.+)")
	var m = r.exec(openSslText)
	if(!m) return
	return m[1]
}
lib.getModulus= function(openSslText){
	var r = /Modulus\s*:([\s\S]+?)(public)?Exponent\s*:/i
	var m = r.exec(openSslText)
	if(!m) return

	return m[1].replace(/\s+/g, "")
}
lib.getWrappedText = function(name, openSslText) {
	var r = new RegExp("(-----BEGIN "+name+"-----[\\s\\S]*?-----END "+name+"-----)")
	var m = r.exec(openSslText)
	if(!m) return
	return m[1]+"\n"
}

/**
* return error message if they dont belong together (validation logic)
*/
lib.arePrivateKeyAndCertMatching = function(parsedPrivateKey, parsedCertificate) {
  if(typeof parsedPrivateKey != "object") return "private key is not an object"
  if(!parsedPrivateKey['RSA PRIVATE KEY']) return "private key is invalid"
  if(!parsedPrivateKey['Modulus']) return "private key has no modulus"

  if(typeof parsedCertificate != "object") return "certificate is not an object"
  if(!parsedCertificate['CERTIFICATE']) return "certificate is invalid"
  if(!parsedCertificate['Modulus']) return "certificate has no modulus"

  if(parsedPrivateKey['Modulus'] != parsedCertificate['Modulus']) return "mismatching private key and certificate"

  return ""
}

lib.asString = function(f) {
  if(Array.isArray(f)) return f.join(" / ")
  return f || ""
}
lib.getDNLine = function(name, line) {

    if(!line) return
	var are = {text: line}
    var fields = line.split(/, /)
    fields.forEach(f =>{
    	var e = f.split("/emailAddress=")
    	if(e.length == 2)
    		e[1] = "emailAddress=" + e[1]
    	e.forEach(fe=>{
	    	var kv = fe.split(/=|:/)
	    	//console.log(f, kv)
	    	processKv(kv)

    	})
    })

    return are

    function processKv(kv){
    	if(kv.length != 2)
    		return
    	var k = kv[0].trim();
    	var v = kv[1].trim();
    	if(!are[k])
    	    are[k] = v;
    	else {
    	  if(!Array.isArray(are[k]))
    		are[k] = [are[k]]
    	  are[k].push(v)
    	}

    }
}

lib.generateOpensslConfForDomains = function(contact, domains) {

	const accents = require("lib-accents.js")

	var acontact = accents.hash(contact)

	var maindomain = domains[0]
	if(!maindomain) throw new Error("No domain specified")


	var re =`[req]
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C=${acontact.co_Country}
ST=${acontact.co_StateProvince}
L=${acontact.co_City}
O=${acontact.co_OrganizationName}
emailAddress=hostmaster@${maindomain}
CN = ${maindomain}

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
`
	var i = 1
	for(var q of domains) {
	  re += `DNS.${i} = ${q}\n`
	  i++
	}

	return re
}

