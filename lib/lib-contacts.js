var lib = module.exports = function(app, knex){

    const KnexLib = require("MonsterKnex")

    var ValidatorsLib = require("MonsterValidators")
    var Common = ValidatorsLib.array()
    var vali = ValidatorsLib.ValidateJs()

    const MError = app.MError

    var re = {}

    function setDefaultContact(contact){
        var vol = app.config.get("volume")
        if(!contact)
            delete vol.defaultContact
        else
            vol.defaultContact = contact
        app.config.saveVolume(vol)
    }
    re.ClearDefault = function(){
        return setDefaultContact()
    }
    re.SetDefault = function(in_data){
        return vali.async(in_data, {co_id:{presence:true, isInteger:true}})
        .then(d=>{
             return re.GetContactById(d.co_id)
        })
        .then(d=>{
            setDefaultContact({co_id:d.co_id})
        })
    }
    re.GetDefault = function(){
        return app.config.get("volume").defaultContact || {}
    }
    re.GetDefaultContactObject = function(){
        if(!re.GetDefault().co_id) throw new Error("Default contact object is not set!");
        return re.GetContactById(re.GetDefault().co_id)
    }


    re.Insert = function(in_data, ts) {
        const misc = require("lib-misc.js")
        var d
        return vali.async(in_data, {

                co_user_id: {presence: true, isString: true, isValidUserId: {info: app.MonsterInfoAccount}},

                co_EmailAddress:{presence:true, mcEmail: true},
                co_FirstName: {presence: true, isString: true},
                co_LastName: {presence: true, isString: true},
                co_OrganizationName: {isString: true, default: {value: ""}},
                co_JobTitle: {isString: true, default: {value: ""}},
                co_Address1: {presence: true, isString: true},
                co_Address2: {isString: true, default: {value: ""}},
                co_City: {presence: true, isString: true},
                co_StateProvince: {presence: true, isString: true},
                co_PostalCode: {presence: true, isString: true},
                co_Country: {presence: true, isString: true, inclusion: misc.GetCountryCodes()},
                co_Phone:   {presence: true, isString: true, format: {pattern: /^\+[0-9]{1,3}\.[0-9]{6,}$/ }}, //+NNN.NNNNNNNNNN
        })
        .then((ad)=>{
            d = ad

            return knex("contacts").select().whereRaw("co_user_id=? AND co_deleted=0", [d.co_user_id])
        })
        .then((rows)=>{
            if(rows.length >= app.config.get("max_contacts_per_account")) throw new MError("TOO_MANY_CONTACTS")

            return knex.insert(d).into("contacts")
        })
        .then((ids)=>{
            return Promise.resolve({co_id: ids[0]})
        })

    }

    re.GetContactById = function(co_id, co_user_id){
        var filter = {co_id:co_id}
        if(co_user_id)
           filter.co_user_id = co_user_id
        return re.GetContactBy(filter)
    }

    re.GetContactBy = function(filterHash){

        return re.ListBy(filterHash)
          .then(contacts=>{
             return Common.EnsureSingleRowReturned(contacts, "CONTACT_NOT_FOUND")
          })

    }

    re.ListByRaw=function(f){
        return knex("contacts").select()
          .whereRaw(f.sqlFilterStr, f.sqlFilterValues)
          .then(rows=>{

                  rows.map(row => contactsRowToObject(row))
                  Common.AddCleanupSupportForArray(rows)

                  return Promise.resolve(rows)
          })

    }

    re.ListBy=function(filterHash){

        var f = KnexLib.filterHash(filterHash)
        return re.ListByRaw(f)
    }

    re.ListDefault=function(filterHash){
        filterHash.co_deleted = 0

        var f = KnexLib.filterHash(filterHash)
        return re.ListByRaw(f)
    }

    return re


    function contactsRowToObject(contact){

        contact.Cleanup = function(){

            return contact
        }

        contact.Delete = function(){
            return knex("contacts").update({co_deleted:true}).whereRaw("co_id=?", [contact.co_id])
        }

        return contact
    }


}
