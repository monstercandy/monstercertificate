var lib = module.exports = function(app){

    var router = app.ExpressPromiseRouter()

    const OpensslLib = require("lib-openssl.js")
    var openssl = OpensslLib(app)

    router.post("/certificates", function(req,res,next){

    	return req.sendPromResultAsIs(
    		openssl.ParseX509Certificates(req.body.json.certificatesText)
    		 .then(rs=>{
    		 	rs.forEach(r=>{
	              	 delete r["CERTIFICATE"]    		 		
    		 	})
    		 	return Promise.resolve(rs)    		 	
    		 })
    	)

    })

    router.post("/private-key", function(req,res,next){

    	return req.sendPromResultAsIs(
    		openssl.ParseRSAPrivateKey(req.body.json.privateKeyText)
    		 .then(r=>{
	          	 delete r["RSA PRIVATE KEY"]
	          	 return Promise.resolve(r)
    		 })
    	)

    })

    router.post("/certificate-signing-request", function(req,res,next){

    	return req.sendPromResultAsIs(
    		openssl.ParseCertificateSigningRequest(req.body.json.csr)
    		 .then(r=>{
	          	 delete r["CERTIFICATE REQUEST"]
	          	 return Promise.resolve(r)    		 	
    		 })
    	)

    })

    return router
}
