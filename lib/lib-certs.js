var lib = module.exports = function(app, knex){

  const moment = require("MonsterMoment")

	const OpensslLib = require("lib-openssl.js")
	var openssl = OpensslLib(app)

    const path = require("path")
    const dotq = require("MonsterDotq")
    const fs = dotq.fs()

    const Token = require("Token")
    const KnexLib = require("MonsterKnex")

    var ValidatorsLib = require("MonsterValidators")
    var Common = ValidatorsLib.array()
    var vali = ValidatorsLib.ValidateJs()

    const MError = app.MError

    const bounceFile = path.join(app.config.get("volume_directory"), ".bounce");

    var re = {}

    re.GetCertificateById = function(cert_id){
      return re.GetItemBy({c_id: cert_id})
    }

    re.GetItemBy = function(filterHash){
        if(typeof filterHash.c_deleted == "undefined") filterHash.c_deleted = "-"

        return re.ListBy(filterHash)
          .then(rows=>{
             return Common.EnsureSingleRowReturned(rows, "CERTIFICATE_NOT_FOUND")
          })
    }


    function filterValidator(filterHash){
        var v = {
            c_id: {isString: true},
            c_alias: {isString: true},
            c_user_id: {isString: {lazy:true}},
            c_webhosting: {isInteger: true},
            c_deleted: {isBooleanLazy: true},
            c_recent: {isBooleanLazy: true},
        }

        return vali.async(filterHash, v)
    }

    re.GetWebhostingIds = function(){
       return knex.raw("SELECT DISTINCT c_webhosting FROM certificates WHERE c_webhosting != 0")
    }

    re.ListByWebhosting = function(wh_id, presentOnly){
       var filters = {c_webhosting: wh_id};
       if(presentOnly)
          filters.c_recent = true;
       return re.ListBy(filters);
    }

    re.ListBy = function(filterHash){
        if(typeof filterHash.c_deleted == "undefined")
            filterHash.c_deleted = 0
        else if(filterHash.c_deleted == "-")
            delete filterHash.c_deleted

        return filterValidator(filterHash)
          .then(d=>{
                var recentOnly = false;
                if(d.c_recent) {
                   delete d.c_recent;
                   recentOnly = true;
                }

                var f = KnexLib.filterHash(d)
                if(recentOnly){
                   f.sqlFilterStr += " AND c_end > ?";
                   f.sqlFilterValues.push(moment.now());
                }
                return knex("certificates").select()
                  .whereRaw(f.sqlFilterStr, f.sqlFilterValues)

          })
          .then(rows=>{

                  rows.map(row => dbRowToObject(row))
                  Common.AddCleanupSupportForArray(rows)

                  return Promise.resolve(rows)
          })

    }

    re.CountBy = function(filterHash){
        return filterValidator(filterHash)
          .then(d=>{

                var f = KnexLib.filterHash(d)
                return knex("certificates").count("* AS c")
                  .whereRaw(f.sqlFilterStr, f.sqlFilterValues)

          })
          .then(rows=>{

              return Promise.resolve(rows[0].c)

          })

    }

    re.Insert = function(in_data, req){
    	var certificate
    	var intermediates
    	var privateKey
    	var d
    	var webhosting
        var csr
        var id

    	var forToken = ""
      var cn = ""

	    return vali.async(in_data, {
        c_id: {isString: {strictName: true}},
	    	c_user_id: {presence: true, isValidUserId: {info:app.MonsterInfoAccount}},
	    	c_webhosting: {isString: {lazy:true}},
        c_comment: {isString: true, default: {value:""}},

            c_settled: {isBooleanLazy: true},

            c_purchase: {isString: true, default: {value:""}},
            c_reseller: {isString: true, default: {value:""}},
            c_vendor: {isString: true, default: {value:""}},
            c_product: {isString: true, default: {value:""}},
            c_reseller_certificate_id: {isString: true, default: {value:""}},

	    	privateKey: {isString: true},
	    	certificate: {presence: true, isString: true},
	    	intermediate: {isString: true},
	    })
	    .then((ad)=>{
	    	d = ad
	    	return re.canBeAdded(d)
	    })
	    .then(()=>{

	    	return openssl.ParseX509Certificates(d.certificate)
	    })
	    .then(a_certificates=>{
	    	if(a_certificates.length > 1) throw new MError("CERTIFICATE_MUST_BE_SINGLE")
	    	if(a_certificates.length < 1) throw new MError("INVALID_CERTIFICATE")

	    	certificate = a_certificates[0]

	        if(!d.intermediate) return Promise.resolve()
	        return openssl.ParseX509Certificates(d.intermediate)
	    })
	    .then(a_intermediates=>{
	    	// console.log("shit!!!!", a_intermediates)
	    	intermediates = a_intermediates

	        if(!d.privateKey) return Promise.resolve()
	        return openssl.ParseRSAPrivateKey(d.privateKey)
	    })
	    .then(a_private_key=>{
	    	privateKey = a_private_key

            if(!d.csr) return Promise.resolve()
            return openssl.ParseCertificateSigningRequest(d.csr)
        })
        .then(a_csr=>{
            csr = a_csr

    		return validateBelongTogether(privateKey, certificate)
    	})
    	.then(()=>{

	    	// we hove got everything validated
	    	d.c_issuer_organization = OpensslLib.asString((certificate.Issuer || {}).O)
	    	d.c_issuer_common_name = OpensslLib.asString((certificate.Issuer || {}).CN)

	    	cn = d.c_subject_common_name = OpensslLib.asString((certificate.Subject || {}).CN)
	    	d.c_subject_organization_unit = OpensslLib.asString((certificate.Subject || {}).OU)

        forToken = JSON.stringify(certificate)

	    	if(!d.c_subject_common_name) throw new MError("NO_COMMON_NAME")

            d.c_begin = (certificate["Not Before"] || {}).iso
            if(!d.c_begin) throw new MError("NO_EXPIRATION_BEGIN")

            d.c_end = (certificate["Not After"] || {}).iso
            if(!d.c_end) throw new MError("NO_EXPIRATION_END")

            d.c_private_key = privateKey ? true : false
            d.c_certificate = true
            d.c_intermediate_chain = intermediates ? true : false
            d.c_certificate_signing_request = csr ? true : false

            d.c_deleted = false

            delete d.privateKey
            delete d.certificate
            delete d.intermediate
            delete d.csr

            // console.log("inserting",d )

        if(!d.c_id)
    		   return Token.GetPseudoTokenAsync(forToken, in_data.certificate|| forToken, req ? req.query.ts : moment.nowUnixtime(), 8, "hex");

        return re.GetCertificateById(d.c_id)
           .then(cert=>{
              throw new MError("CERT_ALREADY_EXISTS", null, {cert: cert})
           })
           .catch(ex=>{
              if(ex.message != "CERTIFICATE_NOT_FOUND")
                throw ex;

              return d.c_id;
           })


    	})
    	.then((a_id)=>{

    		id = d.c_id = a_id


    		return storeFiles(d,{
                csr: csr ? csr["CERTIFICATE_REQUEST"] : null,
    			certificate: certificate ? certificate["CERTIFICATE"] : null,
    			intermediates: intermediates ? intermediates.map(x=>x["CERTIFICATE"]) : null,
    			privateKey: privateKey ? privateKey["RSA PRIVATE KEY"] : null,
    		}, true)

    	})
    	.then(()=>{

	    	return knex.insert(d).into("certificates")
	    })
	    .then(()=>{


           app.InsertEvent(req, {e_event_type: "certificate-create",
		      e_user_id: d.c_user_id,
              e_other: {c_id:id, c_webhosting: d.c_webhosting, c_subject_common_name: cn}}
           )

           return Promise.resolve({c_id:id, c_subject_common_name: cn, c_begin: d.c_begin, c_end: d.c_end})
	    })

    }

    re.canBeAdded=function(d) {
    	// console.log("canbeadded", d)
        return Promise.resolve()
          .then(()=>{
                if((!d.c_user_id)&&(!d.c_webhosting))
                    throw new MError("USER_NOT_SPECIFIED")

                if(!d.c_webhosting) {
                    d.c_webhosting = 0
                    return Promise.resolve({max: app.config.get("max_certificates_per_account"), filterHash: {c_user_id: d.c_user_id}})
                }

                return app.MonsterInfoWebhosting.GetInfo(d.c_webhosting)
                  .then(awebhosting=>{

                     webhosting = awebhosting
                     if((d.c_user_id)&&(d.c_user_id != webhosting.wh_user_id))
                        throw new MError("USER_ID_MISMATCH")
                     if(!d.c_user_id)
                        d.c_user_id =  ""+webhosting.wh_user_id
                     if(!webhosting.template.t_x509_certificate_external_allowed)
                        throw new MError("EXTERNAL_CERTIFICATE_IS_NOT_ALLOWED")

                     return Promise.resolve({max: app.config.get("max_certificates_per_webhosting"), filterHash: {c_user_id: d.c_user_id, c_webhosting: d.c_webhosting}})
                  })

          })
          .then(constraints=>{
             return re.CountBy(constraints.filterHash)
               .then(c=>{
                  if(c >= constraints.max)
                    throw new MError("CERTIFICATE_LIMIT_REACHED")
               })
          })

    }


    re.DetachLocalCertificates = function(wh_id){
       return re.ListByWebhosting(wh_id)
         .then(certs=>{
            return dotq.linearMap({array: certs, action: function(cert){
                return cert.DetachFromWebhosting();
            }})
         })
    }

	return re


    function dbRowToObject(cert){
      var cend = moment(cert.c_end)
      var now = moment()
      // console.log("expires", cend.toISOString());console.log("after", now.add(30, 'days').toISOString());
      cert.closeToExpire = now.add(app.config.get("cert_close_to_expire_before_days"), 'days').isAfter(cend)
      cert.isExpired = now.isAfter(cend)

    	cert.Cleanup = function(){

            delete cert.c_reseller
            delete cert.c_reseller_certificate_id
            if(cert.data)
              delete cert.data.privateKey
    		    return cert
    	}

    	cert.SetAlias = function(newAlias) {
            var d
    		return vali.async({alias:newAlias}, {alias: {isString: {strictName: true, emptyOk: true}, default: {value:""}}})
              .then(ad=>{
                 d = ad
                 if(!d.alias) return Promise.resolve()

                 return re.GetItemBy({c_alias: d.alias})
                   .then(existingCert=>{
                      return existingCert.SetAlias("")
                   })
                   .catch(ex=>{
                       console.warn("no cert exist with this alias yet (this is ok)", d.alias, ex)
                   })

              })
              .then(()=>{

                 if((cert.c_alias)&&(d.alias)) {
                    // renaming alias, this is more complicated
                    return cert.SetAlias("")
                 }
                 else
                    return Promise.resolve()
              })
              .then(()=>{
                 if(d.alias) {
                    return removeSymlinks(d.alias)
                      .then(()=>{
                         return createSymlinks(d.alias)
                      })
                      .then(()=>{
                         return fs.writeFileAsync(bounceFile, "");
                      })
                 }
                 else
                    return removeSymlinks(cert.c_alias)
              })
              .then(()=>{
                 return thisKnex().update({"c_alias": d.alias})
              })
    	}

        function getSymlinkPathes(id){
            var p = getPathes({c_id: id, c_deleted: cert.c_deleted})
            return p
        }
        function removeSymlinks(id){
            return removeFiles(getSymlinkPathes(id))
        }
        function createSymlinks(id){
            var symlinks = getSymlinkPathes(id)
            var dests = getPathes(cert)
            var ps = []
            Object.keys(symlinks).forEach(c=>{
                ps.push(
                    fs.statAsync(dests[c])
                      .then(()=>{
                         return fs.symlinkAsync(dests[c], symlinks[c], "junction")
                      })
                      .catch(ex=>{
                         if(ex.code != "ENOENT")
                            throw ex
                      })
                )
            })
            return Promise.all(ps)
        }

        function thisKnex(){
            return knex("certificates").whereRaw("c_id=?",[cert.c_id])
        }

        function flipActive(){
            var before = getPathes(cert)
            cert.c_deleted = !cert.c_deleted
            var after = getPathes(cert)

            var ps = []
            Object.keys(before).forEach(c=>{
                ps.push( fs.renameAsync(before[c], after[c])
                  .catch(ex=>{
                     if(ex.code != "ENOENT")
                        throw ex
                  })
                )
            })

            return Promise.all(ps)
              .then(()=>{
                  return thisKnex().update({"c_deleted": cert.c_deleted})
              })

        }


      cert.DetachFromDocroot = function(){
            return app.GetRelayerMapiPool().deleteAsync(
               "/s/"+app.config.get("mc_server_name")+"/docrootapi/certificates/"+cert.c_id,
               {}
            )
      }

    	cert.Disable = function() {
            if(cert.c_deleted) throw new MError("CERT_IS_DISABLED")

            return cert.DetachFromDocroot()
            .then(()=>{
               return flipActive()
            })

    	}
    	cert.Enable = function() {
    		if(!cert.c_deleted) throw new MError("CERT_IS_DISABLED")
            return flipActive()
    	}
    	cert.Wipe = function() {

            if(cert.c_alias) throw new MError("REMOVE_ALIAS_FIRST")

            return cert.DetachFromDocroot()
              .then(()=>{
                  return removeFiles()
              })
              .then(()=>{
                  return thisKnex().delete()
              })
    	}
      cert.UpdateParams = function(params) {

          return knex("certificates").update(params).whereRaw("c_id=?",[cert.c_id]).then(()=>{
              Object.keys(params).forEach(k=>{
                  cert[k] = params[k]
              })
              return Promise.resolve()
          })

      }

        function removeFiles(p){
            if(!p) p = getPathes(cert)
            var ps = []
            Object.keys(p).forEach(c=>{
                ps.push( fs.ensureRemovedAsync(p[c]) )
            })

            return Promise.all(ps)
        }

        cert.ReadAndProcessCertificate = function(){
            var p = getPathes(cert)
            return fs.readFileAsync(p.certificate, "utf8")
              .then(d=>{
                 return openssl.ParseX509Certificates(d)
              })
              .then(da=>{
                return Promise.resolve(da[0])
              })
        }

    	cert.UploadPrivateKey = function(privateKey) {
    		uploadAliasLogic()

            if(cert.c_private_key) throw new MError("GOT_IT_ALREADY")

            var privateKeys

            return vali.async({privateKey: privateKey}, {privateKey:{presence:true, isString: true}})
               .then(d=>{
                  return openssl.ParseRSAPrivateKey(d.privateKey)
               })
               .then(a_privateKey=>{
                  privateKey = a_privateKey
                  return cert.ReadAndProcessCertificate()
                })
               .then((certificate)=>{
                  return validateBelongTogether(privateKey, certificate)
                })
               .then(()=>{
                  return storeFiles(cert, {privateKey: privateKey["RSA PRIVATE KEY"]})
               })
               .then(()=>{
                  cert.c_private_key = 1
                  return thisKnex().update({c_private_key:1})
               })

    	}
    	cert.UploadIntermediates = function(intermediates) {
    		uploadAliasLogic()

            return vali.async({intermediates: intermediates}, {intermediates:{presence:true, isString: true}})
               .then(d=>{
                  return openssl.ParseX509Certificates(d.intermediates)
               })
               .then(x=>{
                  var intermediates = x.map(x=>x["CERTIFICATE"])
                  return storeFiles(cert, {intermediates: intermediates})
               })
               .then(()=>{
                  cert.c_intermediate_chain = 1
                  return thisKnex().update({c_intermediate_chain:1})
               })
    	}

        cert.DetachFromWebhosting = function(){
            if(!cert.c_webhosting) throw new MError("NOT_ATTACHED")
            cert.c_webhosting = 0
            return thisKnex().update({c_webhosting:0}).return();

        }
        cert.AttachToWebhosting = function(c_webhosting){
            if(cert.c_webhosting) throw new MError("ALREADY_ATTACHED")

            var d
            return vali.async({c_webhosting:c_webhosting}, {c_webhosting: {presence:true, isInteger: true}})
               .then(ad=>{
                 d = ad
                 return re.canBeAdded({c_user_id: cert.c_user_id, c_webhosting: d.c_webhosting})
               })
               .then(()=>{
                  cert.c_intermediate_chain = 1
                  return thisKnex().update({c_webhosting:d.c_webhosting})
               })
        }

        cert.Read = function(){

            var p = getPathes(cert)
            var ps = []
            cert.data = {}
            if(cert.c_certificate)
                ps.push(fs.readFileAsync(p.certificate, "utf8").then(d=>{cert.data.certificate = d}))
            if(cert.c_private_key)
                ps.push(fs.readFileAsync(p.privateKey, "utf8").then(d=>{cert.data.privateKey = d}))
            if(cert.c_intermediate_chain)
                ps.push(fs.readFileAsync(p.intermediates, "utf8").then(d=>{cert.data.intermediates = OpensslLib.SplitX509Certificates(d)}))
            if(cert.c_certificate_signing_request)
                ps.push(fs.readFileAsync(p.csr, "utf8").then(d=>{cert.data.csr = d}))
            return Promise.all(ps)
        }

        cert.SetSettled = function(settled) {
          return vali.async({settled: settled}, {settled:{presence:true, isBooleanLazy: true}})
            .then(d=>{
                 return thisKnex().update({"c_settled": d.settled})
            })
        }

    	return cert

    	function uploadAliasLogic(){
    		  if(cert.c_alias) throw new MError("HAS_ALIAS")
          if(cert.c_settled) throw new MError("ALREADY_SETTLED")
    	}
    }

    function validateBelongTogether(privateKey, certificate){
    	if(!privateKey) return Promise.resolve()

		var err = OpensslLib.arePrivateKeyAndCertMatching(privateKey, certificate)
		// console.log("checking whether they belong together", privateKey, certificate)
		if(err)
		   return Promise.reject(new MError("VALIDATION_ERROR", err))

		return Promise.resolve()
    }

    function getCertStoreDirectory(active) {
    	return app.config.get("dir_cert_store_"+(active?"active":"archive"))
    }

    function storeFiles(cert, fileData, writeNfo) {
        return vali.async(fileData,{
             privateKey: {isString: true},
             certificate: {isString: true},
             csr: {isString: true},
             intermediates: {isArray: true},
        })
        .then(data=>{
    		var ps = []

            if(data.intermediates)
    		  data.intermediates = data.intermediates.join("\n")

    		var p = getPathes(cert)


            if(writeNfo) {
    	 	    ps.push(
      		   	  fs.writeFileAsync(p.nfo, JSON.stringify(cert, null, 4))
      		   	)
            }

            if(data.csr) {
    	 	    ps.push(
      		   	  fs.writeFileAsync(p.csr, data.csr)
      		   	)
            }

    		if(data.privateKey) {
    		   ps.push(
    		   	  fs.writeFileAsync(p.privateKey, data.privateKey)
    		   	    .then(()=>{
    		   	    	return fs.chmodAsync(p.privateKey, '0600')
    		   	    })
    		   )
    		}

    		if(data.certificate) {
    		   ps.push(
    		   	  fs.writeFileAsync(p.certificate, data.certificate)
    		   )
    		}

    		if(data.intermediates) {
    		   ps.push(
    		   	  fs.writeFileAsync(p.intermediates, data.intermediates)
    		   )
    		}

    		if(data.certificate) {
           // &&(data.intermediates))
           var d = data.certificate+"\n"+(data.intermediates||"")
    		   ps.push(
    		   	  fs.writeFileAsync(p.full_chain, d)
    		   )
    		}

    		if((data.certificate)&&(data.privateKey)) {
    		   ps.push(
    		   	  fs.writeFileAsync(p.pem, data.certificate+data.privateKey)
    		   	    .then(()=>{
    		   	    	return fs.chmodAsync(p.pem, '0600')
    		   	    })
    		   )
    		}

    		return Promise.all(ps)
    	})
    }

    function getPathes(d) {
        var re = {}
   		var dir = getCertStoreDirectory(!d.c_deleted)
	    re.nfo = path.join(dir, d.c_id+".nfo")
		re.privateKey = path.join(dir, d.c_id+".key")
	    re.certificate = path.join(dir, d.c_id+".crt")
		re.intermediates = path.join(dir, d.c_id+".intermediate.crt")
		re.full_chain = path.join(dir, d.c_id+".fullchain.crt")
		re.pem = path.join(dir, d.c_id+".pem")
		re.csr = path.join(dir, d.c_id+".csr")

		return re
    }


}
