// permissions needed: ["CERT_ATTACH","EMIT_LOG","INFO_ACCOUNT","INFO_WEBHOSTING","SEND_EMAIL_TO_USER"]
module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "certificate"

    const xml2js = require("xml2js")
    const dotq = require("MonsterDotq")

    var fs = dotq.fs();

    require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

    const path = require('path')

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');

    config.defaults({
      "max_certificates_per_webhosting": 100,
      "max_certificates_per_account": 10000,
      "max_contacts_per_account": 100,

      "rsa_key_length_bit": 2048,

      "balance_warnings": {
         "USD": 100,
      },

      "certbot_webroot_param": true,

      "cron_balance_poll": "30 */4 * * *",
      "cron_purchase_poll": "0 * * * *",
      "cron_letsencrypt_renew": "30 7 * * *",
      "cron_remove_expired_purchases": "40 7 * * *",

      "namecheap_api_url":"https://api.namecheap.com/xml.response",

      "namecheap_buy_at_activation_only": false,
      "cert_close_to_expire_before_days": 30,
      "namecheap_purchase_renew_period": 30,
      "letsencrypt_auto_reissue_days_before_expiration": 5,
      "letsencrypt_auto_reissue_sleep_between": 10000,
      "letsencrypt_auto_reissue_max_retries": 6,

      "notify_about_expired_purchase_after_days": 1,
      "remove_purchase_after_expiration_days": 14,

      "cmd_openssl_req_csr": {"executable": "[openssl_path]", args: ["req", "-new","-key","[path_rsa_private_key]","-config","[path_config_file]"]},
      "cmd_openssl_gen_rsa": {"executable": "[openssl_path]", args: ["genrsa", "[keylength]"]},
      "cmd_openssl_x509_text": {"executable": "[openssl_path]", args: ["x509", "-text"]},
      "cmd_openssl_rsa_private_text": {"executable": "[openssl_path]", args: ["rsa", "-text","-passin","pass:"]},
      "cmd_openssl_csr_text": {"executable": "[openssl_path]", args: ["req", "-text"]},
      "cmd_letsencrypt_create": {
          executable: "[letsencrypt_binary_path]",
          args: [
            "certonly",
            "--config-dir",
            "[letsencrypt_config_dir_path]",
            "--work-dir",
            "[letsencrypt_work_dir_path]",
            "--logs-dir",
            "[letsencrypt_logs_dir_path]",
            "-w",
            "[domain_validation_letsencrypt_webroot_directory]"
          ]
       },
      "cmd_letsencrypt_revoke": {
          executable: "[letsencrypt_binary_path]",
          args: [
            "revoke",
            "--config-dir",
            "[letsencrypt_config_dir_path]",
            "--work-dir",
            "[letsencrypt_work_dir_path]",
            "--logs-dir",
            "[letsencrypt_logs_dir_path]",
            "--key-path",
            "[private_key_path]",
            "--cert-path",
            "[certificate_path]",
          ]
       },

    })

    var storeDirs = Array("dir_cert_store_active", "dir_cert_store_archive")
    storeDirs.forEach(k=>{
      if(!config.get(k)) throw new Error("Mandatory configuration option is missing: "+k)
      var s =fs.lstatSync(config.get(k))
      if((!s)||(!s.isDirectory()))
        throw new Error("Must be a directory:"+k)
    })

    var cp = config.get("common_params")
    Array("domain_validation_letsencrypt_live_directory","domain_validation_letsencrypt_webroot_directory").forEach(x=>{
       if((!cp)||(!cp[x])) throw new Error("Mandatory configuration option is missing: common_params."+x)
    })

    Array("mc_server_name").forEach(c=>{
       if(!config.get(c)) throw new Error(c+" not defined in config file")

    })


    config.appRestPrefix = "/cert"

    var app = me.Express(config, moduleOptions);
    app.config = config
    app.MError = me.Error
    app.knex = require("MonsterKnex")(app.config)


    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)


    // this is a dependency for the resellers
    const request = require("RequestCommon")
    app.PostForm = request.PostForm

    app.resellers = getResellers()

    var router = app.PromiseRouter(app.config.appRestPrefix)

    var commanderOptions = app.config.get("commander") || {}
    commanderOptions.routerFn = app.ExpressPromiseRouter
    app.commander = require("Commander")(commanderOptions)
    router.use("/tasks", app.commander.router)

    router.RegisterPublicApis(["/tasks/"])
    router.RegisterDontParseRequestBody(["/tasks/"])

    const MonsterInfoLib = require("MonsterInfo")

    app.MonsterInfoAccount = MonsterInfoLib.account({config: app.config, accountServerRouter: app.ServersRelayer})
    app.MonsterInfoWebhosting = MonsterInfoLib.webhosting({config: app.config, webhostingServerRouter: app.ServersRelayer})

    router.use("/info", require("cert-info-router.js")(app, app.knex))

    var certificatesRouter = require("cert-certificates-router.js")
    router.use("/certificates/by-user/:user_id", certificatesRouter(app, app.knex))
    router.use("/certificates/by-webhosting/:webhosting_id", certificatesRouter(app, app.knex))
    router.use("/certificates/all", certificatesRouter(app, app.knex))

    var purchasesRouter = require("cert-purchases-router.js")
    router.use("/purchases/by-user/:user_id", purchasesRouter(app, app.knex))
    router.use("/purchases/all", purchasesRouter(app, app.knex))

    router.use("/cleanup/", require("cert-cleanup-router.js")(app, app.knex))

    var contactsRouter = require("cert-contacts-router.js")
    router.use("/contacts/by-user/:user_id", contactsRouter(app, app.knex))
    router.use("/contacts/all", contactsRouter(app, app.knex))

    var configRouter = require("cert-config-router.js")
    router.use("/config", configRouter(app, app.knex))

    require("Wie")(app, router, require("lib-wie.js")(app, app.knex));

    app.Prepare = function() {

       return app.knex.migrate.latest()
    }

    app.Cleanup = function() {

       var fn = config.get("db").connection.filename
       if(!fn) return Promise.resolve()

       return fs.unlinkAsync(fn).catch(x=>{}) // the database might not exists, which is not an error in this case
         .then(()=>{
            var patternsToDel = []
            storeDirs.forEach(d=>{
               patternsToDel.push(path.join(config.get(d), "*.crt"))
               patternsToDel.push(path.join(config.get(d), "*.key"))
               patternsToDel.push(path.join(config.get(d), "*.csr"))
               patternsToDel.push(path.join(config.get(d), "*.pem"))
               patternsToDel.push(path.join(config.get(d), "*.nfo"))
            })


            const del = require('MonsterDel');
            // console.log("xxx",patternsToDel)
            return del(patternsToDel,{"force": true})

         })

    }

  app.fixDomainNamesWww=function (d) {
      var helper = [];
      d.domains.forEach(dom=>{
        var relay = true;
        if(dom.startsWith("www.")) {
          var f = dom.substr(4);
          if(d.domains.indexOf(f) > -1)
            relay = false;
        }

        if(relay)
           helper.push(dom);
      });

      d.domains = helper;
    }

  app.getPurchases = function() {
      return require("lib-purchases.js")(app, app.knex)
  }
  app.getContacts = function() {
      return require("lib-contacts.js")(app, app.knex)
  }

  app.getXmlParser = function(){
     var parser = xml2js.Parser()
     parser.parseStringAsync = dotq.single(parser.parseString)
     return parser
  }
  app.parseXml = function(str){
     return app.getXmlParser().parseStringAsync(str)
  }



  return app


    function getResellers(){
        var r = fs.readdirSync(__dirname)
        var RegExp = /^reseller-(.*)\.js$/
        var re = {}
        r.forEach(x=>{
          var m = RegExp.exec(x)
          if((!m)||(!m[1])) return

          var l = require(x)
          re[m[1]] = l(app, app.knex)
        })

        // console.log(re);process.reallyExit()
        return re
    }


}
