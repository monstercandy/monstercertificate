require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../cert-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

  const spawnParameters = [app.config.get("cmd_pathes"), app.config.get("common_params")]


  var user = "23401"
  var pc_id
  var co_id

  const expected_webhosting_id = 99998
  const expected_server = "s3"
  const expected_certificate_id = "deadbeef"

  var realCredentials = {
     ApiKey:"key",
     ApiUser: "user1",
     UserName: "user2"
  }
  var expectedCredentials = simpleCloneObject(realCredentials)
  delete expectedCredentials.ApiKey

    const common = require("00-common.js")
    var m = common.mocking(assert, app, user)
    m.setupMonsterInfoAccount()


  describe("credentials/config", function(){

       it("www fixture", function(){
          var d = {domains: ["yourpetbox.hu", "www.yourpetbox.hu"]};
          app.fixDomainNamesWww(d);
          assert.deepEqual(d, {domains: ["yourpetbox.hu"]})

          d = {domains: ["www.yourpetbox.hu", "yourpetbox.hu"]};
          app.fixDomainNamesWww(d);
          assert.deepEqual(d, {domains: ["yourpetbox.hu"]})


          d = {domains: ["www.yourpetbox.hu", "y0urpetbox.hu"]};
          app.fixDomainNamesWww(d);
          assert.deepEqual(d, {domains: ["www.yourpetbox.hu", "y0urpetbox.hu"]})

       })


        it("deleteing credentials", function(done){

                 mapi.delete( "/config/namecheap/credentials", {}, function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result, "ok")
                    done()

                 })

        })

        it("fetching credentials should be empty at the beginning", function(done){

                 mapi.get( "/config/namecheap/credentials", function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.deepEqual(result, {})
                    done()

                 })

        })

        it("methods that need calls to the upstream should throw", function(done){

                 mapi.get( "/config/namecheap/balance", function(err, result, httpResponse){
                    assert.propertyVal(err,"message", "SERVICE_NOT_CONFIGURED")
                    done()

                 })

        })

        it("saving new credentials", function(done){

                 mapi.post( "/config/namecheap/credentials", realCredentials, function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result, "ok")
                    done()

                 })

        })

        it("fetching credentials should return the just saved one; without the key", function(done){

                 mapi.get( "/config/namecheap/credentials", function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.deepEqual(result, expectedCredentials)
                    done()

                 })

        })

        it("methods that need calls to the upstream should work now", function(done){

            app.PostForm = function(url, data) {
               assert.equal(url, app.config.get("namecheap_api_url"))
               var expected = extend({Command: 'namecheap.users.getBalances'}, realCredentials)

               assert.deepEqual(data, expected)
               return Promise.resolve(common.namecheapGetBalanceResponse)
            }

                 mapi.get( "/config/namecheap/balance", function(err, result, httpResponse){
                    assert.isNull(err)
                    // console.dir(result)
                    assert.deepEqual(result, {account:expectedCredentials, balance:{ Currency: 'USD',
  AvailableBalance: '29.86',
  AccountBalance: '29.86',
  EarnedAmount: '0.00',
  WithdrawableAmount: '0.00',
  FundsRequiredForAutoRenew: '0.00' }})
                    done()

                 })

        })



  })

  describe("purchasing", function(){

      buyPositiveSslImmediately()

            it("the certificate just added should have the upstream certificte id", function(done){

                     mapi.get( "/purchases/all/item/"+pc_id, function(err, result, httpResponse){

                        assert.propertyVal(result, "pc_reseller_certificate_id", "500393")
                        done()

                     })

            })


        it("adding a positivessl without buying it actually", function(done){

            app.config.set("namecheap_buy_at_activation_only", true)

            app.PostForm = function(url, data) {
               throw new Error("Should have not called")
            }

            var payload = {pc_user_id: user, pc_reseller: "namecheap", pc_vendor:"Comodo", pc_product:"PositiveSSL", pc_product_parameters:{Years:1}}

                 mapi.put( "/purchases/all", payload, function(err, result, httpResponse){
                    assert.isNull(err)
                    // console.dir(result)
                    assert.property(result, "pc_id")
                    pc_id = result.pc_id
                    done()

                 })

        })

            it("the certificate just added should NOT have the upstream certificte id", function(done){

                     mapi.get( "/purchases/all/item/"+pc_id, function(err, result, httpResponse){

                        assert.property(result, "pc_reseller_certificate_id")
                        assert.notOk(result.pc_reseller_certificate_id)
                        done()

                     })

            })


        common.putContact(mapi, user, function(result){
            co_id = result.co_id
        })

        it("saving it as default", function(done){

                 mapi.post( "/config/contacts/default", {co_id:co_id}, function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result, "ok")
                    done()

                 })

        })

        it("parse_csr should return the expected comodo filenames and content", function(done){

            const csr_content = "some_csr_content"
            var namecheapCalls = 0
            app.PostForm = function(url, data){
              namecheapCalls++
              // console.log(require("util").inspect(data, false, null))
              assert.deepEqual(data, extend({}, realCredentials, {Command: 'namecheap.ssl.parseCSR',HTTPDCValidation: 'TRUE',  csr: csr_content, CertificateType: 'PositiveSSL'}))
              return Promise.resolve(common.namecheapParseCsrResponse)
            }

             mapi.post( "/purchases/by-user/"+user+"/item/"+pc_id+"/dispatch", {method:"parse_csr", csr:csr_content},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)

                    assert.deepEqual(result, { csr:
   { CommonName: [ 'domain.com' ],
     DomainName: [ 'domain.com' ],
     Country: [ 'US' ],
     OrganisationUnit: [ 'com' ],
     Organisation: [ 'Org' ],
     ValidTrueDomain: [ 'false' ],
     State: [ 'California' ],
     Locality: [ 'la' ],
     Email: [ 'someemail@domain.com' ] },
  http_validation:
   { FileName: '776DE8TT1FA13356DAB9JJ0F1ACDXSD2.txt',
     FileContent: 'UUU2310A0C5S55223FC560CT432259CB4177C79D\n    comodoca.com' } })
                    assert.equal(namecheapCalls, 1)

                    done()
                 })
        })


        it("querying email addresses belonging to this domain (text 'none' should be filtered out; method name in request body)", function(done){

            var namecheapCalls = 0
            app.PostForm = function(url, data){
              namecheapCalls++
              // console.log(require("util").inspect(data, false, null))
              assert.deepEqual(data, extend({}, realCredentials, {Command: 'namecheap.ssl.getApproverEmailList', DomainName: 'www.index.hu', CertificateType: 'PositiveSSL'}))
              return Promise.resolve(common.namecheapSslGetApproverEmailList2)
            }

             mapi.post( "/purchases/by-user/"+user+"/item/"+pc_id+"/dispatch", {method:"get_approver_emails", DomainName:"www.index.hu"},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.property(result, "emails")
                    assert.deepEqual(result, {emails:["foobar@domain.com"]})
                    assert.equal(namecheapCalls, 1)

                    done()
                 })
        })


        it("querying email addresses belonging to this domain (method name in query string)", function(done){

            var namecheapCalls = 0
            app.PostForm = function(url, data){
              namecheapCalls++
              // console.log(require("util").inspect(data, false, null))
              assert.deepEqual(data, extend({}, realCredentials, {Command: 'namecheap.ssl.getApproverEmailList', DomainName: 'www.index.hu', CertificateType: 'PositiveSSL'}))
              return Promise.resolve(common.namecheapSslGetApproverEmailList)
            }

             mapi.post( "/purchases/by-user/"+user+"/item/"+pc_id+"/dispatch/get_approver_emails", {DomainName:"www.index.hu"},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.property(result, "emails")
                    assert.deepEqual(result, {emails:["3db85a8e21b54bab848eb8f01d5d78c5.protect@whoisguard.com","postmaster@domain.com","sslwebmaster@domain.com","ssladministrator@domain.com","mis@domain.com"]})
                    assert.equal(namecheapCalls, 1)

                    done()
                 })
        })


        it("query_validation should be the same as getapproveremails", function(done){

            const csr_content = "some_csr_content"
            var namecheapCalls = 0
            app.PostForm = function(url, data){
              namecheapCalls++
              //  console.log("CRAP", require("util").inspect(data, false, null))
              if(namecheapCalls == 1) {
                  assert.deepEqual(data, extend({}, realCredentials, {Command: 'namecheap.ssl.getApproverEmailList', DomainName: 'www.index.hu', CertificateType: 'PositiveSSL'}))
                  return Promise.resolve(common.namecheapSslGetApproverEmailList)
              }
              else
                throw new Error("too much calls")
            }

             mapi.post( "/purchases/by-user/"+user+"/item/"+pc_id+"/dispatch/query_validation", {DomainName:"www.index.hu",csr:csr_content},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)

                    assert.deepEqual(result, {
                        email_validation: ["3db85a8e21b54bab848eb8f01d5d78c5.protect@whoisguard.com","postmaster@domain.com","sslwebmaster@domain.com","ssladministrator@domain.com","mis@domain.com"]
                    })
                    assert.equal(namecheapCalls, 1)

                    done()
                 })
        })



        it("activating this namecheap purchase in external mode (should buy the certificate in upstream!)", function(done){

            var csr = "fooocsr"

            var namecheapCalls = 0
            app.PostForm = function(url, data){
              namecheapCalls++
              console.log("mocked namecheap call", url, data)
              if(namecheapCalls == 1) {
                 return sslCreateExpected(url, data)
              }
              else
              if(namecheapCalls == 2){
                assert.deepEqual(data, extend({}, realCredentials, {
                    Command:"namecheap.ssl.activate",
                    CertificateID: '500393',
                    ApproverEmail: 'foobar@foo.fo',
                    csr: '-----BEGIN CERTIFICATE REQUEST-----\nfoobar\n-----END CERTIFICATE REQUEST-----\n',
                    WebServerType: 'apacheopenssl',
                    AdminEmailAddress: 'email@email.hu',
                    AdminJobTitle: 'Job Title',
                    AdminFirstName: 'First',
                    AdminLastName: 'Last',
                    AdminOrganizationName: 'Organization',
                    AdminAddress1: 'Cim 1',
                    AdminAddress2: 'Cim 2',
                    AdminCity: 'City',
                    AdminStateProvince: 'State',
                    AdminPostalCode: '1234',
                    AdminCountry: 'HU',
                    AdminPhone: '+36.302679426',
                }))
                return Promise.resolve(common.namecheapSslActivateResponse)
              }
              else
                throw new Error("Unexpected call")

            }


             var oCommander = app.commander
             app.commander = {
              spawn: function(app, params) {
                // console.log("foo!", app, params)
                assert.deepEqual(app, { removeImmediately: true, chain:
   { executable: '[openssl_path]',
     args: [ 'req', '-text' ],
     stdin: csr } })
                assert.deepEqual(params, spawnParameters)
                return Promise.resolve({
                   executionPromise: Promise.resolve({output: common.mainCsr})
                })
              }
             }

             // console.log("sending http request");
             mapi.put( "/purchases/by-user/"+user+"/item/"+pc_id+"/activations", {csr:csr, ApproverEmail: "foobar@foo.fo", Admin_contact_id: co_id},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.property(result, "a_id")
                    assert.equal(namecheapCalls, 2)

                    app.commander = oCommander

                    done()
                 })

        })


            it("the certificate just activated should have the upstream certificte id and also a subject common name!", function(done){

                     mapi.get( "/purchases/all/item/"+pc_id, function(err, result, httpResponse){

                        assert.propertyVal(result, "pc_subject_common_name", "www.monstermedia.hu")
                        assert.propertyVal(result, "pc_reseller_certificate_id", "500393")
                        assert.propertyVal(result, "pc_status", "processing")
                        assert.property(result, "pc_activation")
                        assert.ok(result.pc_activation)
                        assert.propertyVal(result, "pc_local", 0)
                        done()

                     })

            })

            it("activation parameters should look like as expected", function(done){

                     mapi.get( "/purchases/all/item/"+pc_id+"/activations", function(err, result, httpResponse){

                        assert.property(result, "activations")
                        assert.ok(Array.isArray(result.activations))
                        assert.equal(result.activations.length, 1)

                        var a = result.activations[0]
                        Array("csr", "ApproverEmail", "Admin_contact_id").forEach(x=>{
                           assert.property(a.a_payload, x)
                           assert.ok(a.a_payload[x])
                        })

                        assert.propertyVal(a.a_payload, "approvalType", "email")

                        assert.deepEqual(a.a_extra, {mainDomain: "www.monstermedia.hu"})

                        done()

                     })

            })

            it("getting approver email resent", function(done){

                var namecheapCalls = 0
                app.PostForm = function(url, data){
                  namecheapCalls++
                  // console.log(require("util").inspect(data, false, null))
                  assert.deepEqual(data, extend({}, realCredentials, {Command: 'namecheap.ssl.resendApproverEmail', ApproverEmail: 'some@thing.hu', CertificateID: '500393'}))
                  return Promise.resolve(common.namecheapSslResendApproverEmail)
                }

                 mapi.post( "/purchases/by-user/"+user+"/item/"+pc_id+"/dispatch", {method:"resend_approver_email", ApproverEmail: 'some@thing.hu'},
                     function(err, result, httpResponse){
                        // console.log("re", result, err)
                        assert.equal(result, "ok")
                        assert.equal(namecheapCalls, 1)

                        done()
                     })
            })


            it("changing status forcefully should be possible", function(done){

                     mapi.post( "/purchases/all/item/"+pc_id+"/raw", {pc_status: "issued"}, function(err, result, httpResponse){

                        assert.equal(result, "ok")

                        done()

                     })

            })

            shouldHaveTheResellerCertificateId(function(result){
                assert.propertyVal(result, "pc_status", "issued")
                assert.propertyVal(result, "pc_local", 0)
                assert.deepEqual(result.actions, ["vendor2-revoke","vendor2-reissue-general"])

            })


            it("revoking the certificate", function(done){

                var namecheapCalls = 0
                app.PostForm = function(url, data){
                  namecheapCalls++
                  // console.log(require("util").inspect(data, false, null))
                  assert.deepEqual(data, extend({}, realCredentials, {Command: 'namecheap.ssl.revokecertificate', CertificateType: "PositiveSSL", CertificateID: '500393'}))
                  return Promise.resolve(common.namecheapSslResendApproverEmail)
                }

                 mapi.post( "/purchases/by-user/"+user+"/item/"+pc_id+"/revoke", {},
                     function(err, result, httpResponse){
                        // console.log("re", result, err)
                        assert.equal(result, "ok")
                        assert.equal(namecheapCalls, 1)

                        done()
                     })
            })

            shouldHaveTheResellerCertificateId(function(result){
                assert.equal(result.pc_status, "revoked")
                assert.deepEqual(result.actions, ["vendor2-reissue-general"])
            })


       buyPositiveSslImmediately()


        const certSigningRequest = "-- BEGIN SIGNING REQUEST --\nfoo\n-- END SIGNING REQUEST --\n"
        const rsaPrivateKey = "-- begin rsa private key --\ngreat\n-- end of rsa private key --\n"
        const mainDomain = "storyteller.hu"



            it("activation should relay upstream error message", function(done){

                var oPostForm = app.PostForm
                var namecheapCalls = 0
                app.PostForm = function(url, data){
                  namecheapCalls++
                  console.log("mocked namecheap call", url, data)

                  if(namecheapCalls == 1) {
                    assert.propertyVal(data, "Command", "namecheap.ssl.activate")

                    return Promise.resolve(common.namecheapSslActivateResponseFailed)
                  }
                  else
                    throw new Error("Unexpected call")

                }


             var oCommander = app.commander
             app.commander = {
              spawn: function(app, params) {
                return Promise.resolve({
                   executionPromise: Promise.resolve({output: common.mainCsr})
                })
              }
             }

             mapi.put( "/purchases/by-user/"+user+"/item/"+pc_id+"/activations", {csr:"foocsr", ApproverEmail: "foobar@foo.fo", Admin_contact_id: co_id},
                 function(err, result, httpResponse){
                    console.log("re", result, err)
                    assert.propertyVal(err, "message", 'UPSTREAM_SERVICE_PROCESSING_ERROR')
                    assert.deepEqual(err.errorParameters, ["Invalid CSR. -7:  \"UK\" is not a valid ISO-3166 country code! Do you mean GB?"])

                    assert.equal(namecheapCalls, 1)

                    app.commander = oCommander

                    done()
                 })

            })


        activateReissueTest("activating a namecheap purchase in local mode","activate")


        activationParamsShouldBeOk()


            it("rsa private key should be hidden in activation parameters", function(done){

                     mapi.get( "/purchases/all/item/"+pc_id+"/activation", function(err, result, httpResponse){

                        var a = result
                        // console.log(a)

                        // note the www one should be missing! , "www."+mainDomain
                        assert.deepEqual(a.a_payload, {local:true,domains:[mainDomain], attach: {server: expected_server, webhosting_id: expected_webhosting_id, docroot_domain: mainDomain}})
                        assert.deepEqual(a.a_extra, {privateKey: "***", csr: certSigningRequest, mainDomain: mainDomain})
                        // ApproverEmail:"hostmaster@"+mainDomain,

                        done()

                     })

            })

            shouldHaveTheResellerCertificateId(function(result){
                assert.propertyVal(result, "pc_local", 1)
                assert.deepEqual(result.actions, ["vendor2-resend-approver"])//,"vendor2-activate-general","vendor2-activate-local"
            })


            it("polling shouldnt touch anything when enrollment hasnt finished yet", function(done){

                var oPostForm = app.PostForm
                var namecheapCalls = 0
                app.PostForm = function(url, data){
                  namecheapCalls++
                  console.log("mocked namecheap call", url, data)

                  if(namecheapCalls == 1) {
                    assert.deepEqual(data, extend({}, realCredentials, { Command: 'namecheap.ssl.getInfo', CertificateID: '500393',returncertificate: 'true', returntype: 'individual' }))

                    return Promise.resolve(common.namecheapSslGetInfoStillPendingRespone);
                  }
                  else
                    throw new Error("Unexpected call")

                }


                     mapi.post( "/purchases/all/item/"+pc_id+"/poll", {}, function(err, result, httpResponse){

                        assert.equal(namecheapCalls, 1)
                        assert.propertyVal(err, "message", "NOT_YET_READY")
                        assert.isUndefined(err.errorParameters)

                        app.PostForm = oPostForm
                        done()

                     })

            })

        activationParamsShouldBeOk()



            it("polling should finish the process when the certificate is finally enrolled", function(done){

               var stat = common.setupRelayerMapiPoolForCertInsert(assert, app, expected_certificate_id, expected_server, expected_webhosting_id, mainDomain)

                var oPostForm = app.PostForm
                var namecheapCalls = 0
                app.PostForm = function(url, data){
                  namecheapCalls++
                  console.log("mocked namecheap call", url, data)

                  if(namecheapCalls == 1) {
                    assert.deepEqual(data, extend({}, realCredentials, { Command: 'namecheap.ssl.getInfo', CertificateID: '500393', returncertificate: 'true', returntype: 'individual'}))

                    return Promise.resolve(common.namecheapSslGetInfoSuccessRespone);
                  }
                  else
                    throw new Error("Unexpected call")

                }


                     mapi.post( "/purchases/all/item/"+pc_id+"/poll", {}, function(err, result, httpResponse){

                        assert.equal(namecheapCalls, 1)
                        assert.equal(result, "ok")

                        app.PostForm = oPostForm
                        done()

                     })

            })

            shouldHaveTheResellerCertificateId(function(result){
                assert.propertyVal(result, "pc_subject_common_name", mainDomain)
                assert.propertyVal(result, "pc_status", "issued")
                assert.propertyVal(result, "pc_local", 1)
                assert.deepEqual(result.actions, ["vendor2-revoke", "vendor2-reissue-general", "vendor2-reissue-local"])
            })


        activateReissueTest("reissuing latest purchase","reissue")

            shouldHaveTheResellerCertificateId(function(result){
                assert.equal(result.pc_status, "processing")
            })


       function activationParamsShouldBeOk(){
            it("activation parameters should look like as expected in this latest activation", function(done){

                     mapi.get( "/purchases/all/item/"+pc_id+"/activations", function(err, result, httpResponse){

                        assert.property(result, "activations")
                        assert.ok(Array.isArray(result.activations))
                        assert.equal(result.activations.length, 2) // the first is the failed one

                        var a = result.activations[1]
                        // console.log(a)


                        // note the www one should be missing! , "www."+mainDomain
                        assert.deepEqual(a.a_payload, {local:true,domains:[mainDomain], attach: {server: expected_server, webhosting_id: expected_webhosting_id, docroot_domain: mainDomain}})
                        assert.deepEqual(a.a_extra, {privateKey: "***", csr: certSigningRequest, mainDomain: mainDomain})
                        assert.property(a.a_result, "HttpDCValidation")
                        //ApproverEmail:"hostmaster@"+mainDomain,

                        done()

                     })

            })


       }


        function activateReissueTest(desc, ncCmd) {

            it(desc, function(done){

                var oPostForm = app.PostForm
                var namecheapCalls = 0
                app.PostForm = function(url, data){
                  namecheapCalls++
                  console.log("mocked namecheap call", url, data)

                  if(namecheapCalls == 1) {
                    assert.deepEqual(data, extend({}, realCredentials, {
                        Command:"namecheap.ssl."+ncCmd,
                        CertificateID: '500393',
                        HTTPDCValidation: 'TRUE',
                        csr: certSigningRequest,
                        WebServerType: 'apacheopenssl',
                        AdminEmailAddress: 'email@email.hu',
                        AdminJobTitle: 'Job Title',
                        AdminFirstName: 'First',
                        AdminLastName: 'Last',
                        AdminOrganizationName: 'Organization',
                        AdminAddress1: 'Cim 1',
                        AdminAddress2: 'Cim 2',
                        AdminCity: 'City',
                        AdminStateProvince: 'State',
                        AdminPostalCode: '1234',
                        AdminCountry: 'HU',
                        AdminPhone: '+36.302679426',
                    }))
                    return Promise.resolve(common.namecheapSslActivateResponse)
                  }
                  else
                    throw new Error("Unexpected call")

                }


               var commanderCalls = 0
               var oCommander = app.commander
               app.commander = {
                 spawn: function(cmd, params){
                    commanderCalls++
                    if(commanderCalls == 1){
                       return Promise.resolve({id: "rsa",executionPromise: Promise.resolve({output: rsaPrivateKey})})
                    }
                    else
                    if(commanderCalls == 2){
                       return Promise.resolve({id: "csr",executionPromise: Promise.resolve({output: certSigningRequest})})
                    }
                    else
                       throw new Error("unexpected call")
                 }
               }

               var cb = function(err, result, httpResponse){
                        // console.log("re", result, err)
                        assert.property(result, "a_id")
                        assert.equal(namecheapCalls, 1)
                        assert.equal(commanderCalls, 2)
                        app.PostForm = oPostForm

                        done()
                }
                var payload = {local:true, domains:[mainDomain,"www."+mainDomain], attach: {server: expected_server, webhosting_id: expected_webhosting_id, docroot_domain: mainDomain}}

                if(ncCmd == "activate")
                    mapi.put("/purchases/by-user/"+user+"/item/"+pc_id+"/activations", payload, cb)
                else
                    mapi.post("/purchases/by-user/"+user+"/item/"+pc_id+"/reissue", payload, cb)


            })

        }


  })

  describe("renewal", function(){
      buyPositiveSslImmediately()

      changeStatusToIssued()

        var renewed_pc_id

        it("calling renew (first response type)", function(done){


                var oPostForm = app.PostForm
                var namecheapCalls = 0
                app.PostForm = function(url, data){
                  namecheapCalls++
                  console.log("mocked namecheap call", url, data)

                  if(namecheapCalls == 1) {
                    assert.deepEqual(data, extend({}, realCredentials, {
                        Command:"namecheap.ssl.renew",
                        CertificateID: '500393',
                        SSLType: 'PositiveSSL',
                        Years: 1,
                    }))
                    return Promise.resolve(common.namecheapSslRenewResponse1)
                  }
                  else
                    throw new Error("Unexpected call")

                }

                 mapi.post( "/purchases/by-user/"+user+"/item/"+pc_id+"/renew", {pc_product_parameters:{Years: 1}},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.equal(namecheapCalls, 1)
                    assert.property(result, "pc_id")
                    assert.ok(result.pc_id)
                    renewed_pc_id = result.pc_id
                    done()
                 })

        })



        it("status of the old one should be renewed", function(done){

                 mapi.get( "/purchases/by-user/"+user+"/item/"+pc_id,
                 function(err, result, httpResponse){
                    assert.propertyVal(result, "pc_status", "renewed")
                    assert.deepEqual(result.actions, [])
                    done()
                 })

        })

        it("status of the new one should be pending", function(done){

                 mapi.get( "/purchases/by-user/"+user+"/item/"+renewed_pc_id,
                 function(err, result, httpResponse){
                    assert.propertyVal(result, "pc_status", "pending")
                    assert.property(result, "pc_comment")
                    assert.propertyVal(result, "pc_reseller_certificate_id", "501904") // this one should come from the namecheap response
                    assert.ok(result.pc_comment)
                    assert.property(result, "pc_activation")
                    assert.deepEqual(result.pc_product_parameters, {Years:1})
                    assert.notOk(result.pc_activation)
                    done()
                 })

        })



      buyPositiveSslImmediately()

      changeStatusToIssued()

        it("calling renew (second response type)", function(done){


                var oPostForm = app.PostForm
                var namecheapCalls = 0
                app.PostForm = function(url, data){
                  namecheapCalls++
                  console.log("mocked namecheap call", url, data)

                  if(namecheapCalls == 1) {
                    assert.deepEqual(data, extend({}, realCredentials, {
                        Command:"namecheap.ssl.renew",
                        CertificateID: '500393',
                        SSLType: 'PositiveSSL',
                        Years: 1,
                    }))
                    return Promise.resolve(common.namecheapSslRenewResponse2)
                  }
                  else
                    throw new Error("Unexpected call")

                }

                 mapi.post( "/purchases/by-user/"+user+"/item/"+pc_id+"/renew", {pc_product_parameters:{Years: 1}},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.equal(namecheapCalls, 1)
                    assert.property(result, "pc_id")
                    assert.ok(result.pc_id)
                    renewed_pc_id = result.pc_id
                    done()
                 })

        })
  })


  describe("http validation", function(){

     buyPositiveSslImmediately()

        it("activating this namecheap purchase in external mode, using http validation", function(done){

            var csr = "fooocsr"

            var namecheapCalls = 0
            app.PostForm = function(url, data, options){
              namecheapCalls++
              console.log("mocked namecheap call", url, data, options)

              if(namecheapCalls == 1){
                assert.deepEqual(data, extend({}, realCredentials, {
                    Command:"namecheap.ssl.activate",
                    CertificateID: '500393',
                    HTTPDCValidation: 'TRUE',
                    csr: '-----BEGIN CERTIFICATE REQUEST-----\nfoobar\n-----END CERTIFICATE REQUEST-----\n',
                    WebServerType: 'apacheopenssl',
                    AdminEmailAddress: 'email@email.hu',
                    AdminJobTitle: 'Job Title',
                    AdminFirstName: 'First',
                    AdminLastName: 'Last',
                    AdminOrganizationName: 'Organization',
                    AdminAddress1: 'Cim 1',
                    AdminAddress2: 'Cim 2',
                    AdminCity: 'City',
                    AdminStateProvince: 'State',
                    AdminPostalCode: '1234',
                    AdminCountry: 'HU',
                    AdminPhone: '+36.302679426',
                }))
                return Promise.resolve(common.namecheapSslActivateResponse)
              }
              else
                throw new Error("Unexpected call")

            }


             var oCommander = app.commander
             app.commander = {
              spawn: function(app, params) {
                assert.deepEqual(app, { removeImmediately: true, chain:
   { executable: '[openssl_path]',
     args: [ 'req', '-text' ],
     stdin: csr } })
                assert.deepEqual(params, spawnParameters)
                return Promise.resolve({
                   executionPromise: Promise.resolve({output: common.mainCsr})
                })
              }
             }

             mapi.put( "/purchases/by-user/"+user+"/item/"+pc_id+"/activations", {csr:csr, approvalType: "http", Admin_contact_id: co_id},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.property(result, "a_id")
                    assert.equal(namecheapCalls, 1)

                    app.commander = oCommander

                    done()
                 })

        })

  })



    function sslCreateExpected(url,data){
                   assert.equal(url, app.config.get("namecheap_api_url"))
                   var expected = extend({Command: 'namecheap.ssl.create', Years: 1, Type: 'PositiveSSL'}, realCredentials)

                   assert.deepEqual(data, expected)
                   return Promise.resolve(common.namecheapSslCreateResponse)

    }

    function buyPositiveSslImmediately(){

        it("buying a positivessl", function(done){

            app.config.set("namecheap_buy_at_activation_only", false)

            app.PostForm = function(url, data) {
                return sslCreateExpected(url, data)
            }

            var payload = {pc_user_id: user, pc_reseller: "namecheap", pc_vendor:"Comodo", pc_product:"PositiveSSL", pc_product_parameters:{Years:1}}

                 mapi.put( "/purchases/all", payload, function(err, result, httpResponse){
                    assert.isNull(err)
                    // console.dir(result)
                    assert.property(result, "pc_id")
                    pc_id = result.pc_id
                    done()

                 })

        })

        shouldHaveTheResellerCertificateId()

    }

    function shouldHaveTheResellerCertificateId(extraCb) {
        it("should have the reseller certificate id", function(done){


            var payload = {pc_user_id: user, pc_reseller: "namecheap", pc_vendor:"Comodo", pc_product:"PositiveSSL", pc_product_parameters:{Years:1}}

                 mapi.get( "/purchases/all/item/"+pc_id, function(err, result, httpResponse){
                    assert.isNull(err)
                    // console.dir(result)
                    assert.propertyVal(result, "pc_reseller_certificate_id", '500393')
                    if(extraCb) extraCb(result)
                    done()

                 })

        })

    }

    function changeStatusToIssued(){
            it("changing status forcefully should be possible", function(done){

                     mapi.post( "/purchases/all/item/"+pc_id+"/raw", {pc_status: "issued"}, function(err, result, httpResponse){

                        assert.equal(result, "ok")

                        done()

                     })

            })
    }


}, 10000)


