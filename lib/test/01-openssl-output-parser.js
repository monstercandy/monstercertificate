require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../cert-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

  const common = require("00-common.js")
  const OpensslLib = require("../lib-openssl.js")

  const spawnParameters = [app.config.get("cmd_pathes"), app.config.get("common_params")]

  const rsaPrivateKey = "-- begin rsa private key --\ngreat\n-- end of rsa private key --\n"

  const rsaPasswordProtectedPrivateKey = `-----BEGIN ENCRYPTED PRIVATE KEY-----
MIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQIhV/yebnkyJ8CAggA
2qc=
-----END ENCRYPTED PRIVATE KEY-----
`


	describe('certificates', function() {

       it('pure cert should return the pure cert', function() {
        var cert = `
-----BEGIN CERTIFICATE-----
cert0
-----END CERTIFICATE-----
`

           var re = OpensslLib.ParseX509TextOutput(cert)
           assert.deepEqual(re, {CERTIFICATE: "-----BEGIN CERTIFICATE-----\ncert0\n-----END CERTIFICATE-----\n"})

      })

        it('regular certificate should be parsed well', function() {

           var re = OpensslLib.ParseX509TextOutput(common.mainCert)

            assert.deepEqual(re,     {
             "CERTIFICATE": "-----BEGIN CERTIFICATE-----\ncert0\n-----END CERTIFICATE-----\n",
             "Modulus": "00:b3:42:35:26:05:e3:f3:98:b7:a6:95:9a:5d:8a:f0:5c:7f:b1:0e:78:25:c1:83:ec:9f:3a:e8:39:22:df:43:5e:bd:46:6d:72:89:9c:97:d5:be:2f:e4:14:54:6d:19:34:79:19:9b:41:66:b0:0b:ab:8d:73:46:ec:ca:1e:87:26:66:cb:ca:ed:af:bd:bf:fb:3c:ad:c2:aa:df:d1:7e:39:d8:2b:dd:90:15:45:a8:b5:65:c1:ed:b3:75:50:3e:46:1f:d4:e4:81:1e:5f:9d:0d:1f:b8:39:2c:92:91:f4:8c:60:c9:f6:f5:64:c6:84:f0:f4:5b:d7:a6:6b:d1:f7:01:1d:16:9b:10:16:73:49:73:52:e8:99:e5:7c:da:d5:63:2e:92:60:dd:a7:1a:69:65:85:42:c8:15:c9:55:29:6f:9d:ae:4f:09:2f:2f:a4:cb:fb:5c:9f:04:d8:23:b9:19:cf:55:33:b6:29:1b:42:57:91:7a:b6:34:72:9f:c9:71:17:78:cc:f4:3c:6d:a1:d8:13:67:95:6d:da:85:9d:5f:2a:27:82:57:cd:c0:bb:29:b5:38:17:d5:64:85:be:fa:00:32:a7:f1:11:2a:b6:59:77:71:5a:20:ce:dc:6b:23:8d:05:c0:d1:57:61:b9:aa:0d:ec:6f:4c:73:84:15:29",
              Subject:
   { text: 'OU=GT46628488, OU=See www.rapidssl.com/resources/cps (c)15, OU=Domain Control Validated - RapidSSL(R), CN=www.monstermedia.hu',
     OU:
      [ 'GT46628488',
        'See www.rapidssl.com/resources/cps (c)15',
        'Domain Control Validated - RapidSSL(R)' ],
     CN: 'www.monstermedia.hu' },
  Issuer:
   { text: 'C=US, O=GeoTrust Inc., CN=RapidSSL SHA256 CA - G3',
     C: 'US',
     O: 'GeoTrust Inc.',
     CN: 'RapidSSL SHA256 CA - G3' },
    "Not After": {
          "iso": "2017-05-18T21:37:09.000Z",
          "text": "May 18 21:37:09 2017 GMT",
    },
    "Not Before": {
          "iso": "2015-05-16T05:28:23.000Z",
         "text": "May 16 05:28:23 2015 GMT",
    },
  'X509v3 Subject Alternative Name':
   { text: 'DNS:www.monstermedia.hu, DNS:monstermedia.hu',
     DNS: [ 'www.monstermedia.hu', 'monstermedia.hu' ] } })


            assert.equal(OpensslLib.asString(re.Subject.OU), "GT46628488 / See www.rapidssl.com/resources/cps (c)15 / Domain Control Validated - RapidSSL(R)")

            assert.equal(OpensslLib.asString(re.Subject.text), re.Subject.text)


        })

        it("new style openssl output", function(){
          var cert = `
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            03:53:e2:76:2d:b1:2d:75:0d:4d:4a:b2:66:84:0e:32:c9:f1
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = US, O = Let's Encrypt, CN = Let's Encrypt Authority X3
        Validity
            Not Before: Mar  3 12:44:55 2018 GMT
            Not After : Jun  1 12:44:55 2018 GMT
        Subject: CN = mysql.stan.monstermedia.hu
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:b7:3f:bf:d7:5f:d3:b5:b1:1d:bc:3d:15:0d:bf:
                    4a:54:3f:ca:0d:ad:b2:05:7c:40:e8:6f:39:dd:7c:
                    ef:96:47:51:1f:67:92:29:95:b9:8f:4f:0e:67:76:
                    b7:ee:d6:f8:fd:f3:6c:41:98:cf:49:5e:2d:0b:46:
                    1e:12:90:d1:3f:90:81:6d:b4:50:57:1e:13:29:ed:
                    e2:51:2f:8f:2a:8a:90:0f:6e:74:1c:30:1d:59:72:
                    1a:ff:ae:5a:8a:bf:8c:0b:68:25:60:6c:1d:1f:d1:
                    a6:56:ef:66:9b:ab:83:f9:19:3f:cd:18:07:e4:e5:
                    41:13:1f:5f:86:61:65:f4:5e:58:93:9a:fd:4c:a6:
                    2c:60:60:16:72:8e:fb:e9:f9:9b:89:a5:97:1d:d4:
                    8d:d3:d7:eb:92:eb:c9:68:1c:e2:90:80:c7:8b:4c:
                    d5:5c:a4:05:24:62:89:5c:c5:29:92:07:b6:d6:48:
                    94:f4:55:88:2d:ef:e8:f2:2a:78:c1:e0:03:de:28:
                    6c:2e:71:a0:e5:d8:41:c0:d1:5c:25:07:72:43:5f:
                    51:47:42:db:67:7d:7f:f3:12:15:5e:cb:c7:69:83:
                    04:3d:89:55:1b:aa:08:47:73:8e:cd:62:e7:5a:f8:
                    42:01:2a:49:f6:bd:1b:42:53:e7:d1:cd:6d:e5:23:
                    0d:f7
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Key Usage: critical
                Digital Signature, Key Encipherment
            X509v3 Extended Key Usage: 
                TLS Web Server Authentication, TLS Web Client Authentication
            X509v3 Basic Constraints: critical
                CA:FALSE
            X509v3 Subject Key Identifier: 
                94:34:7F:C4:66:62:79:A1:3E:46:58:15:07:19:0A:DE:E6:AE:EE:B9
            X509v3 Authority Key Identifier: 
                keyid:A8:4A:6A:63:04:7D:DD:BA:E6:D1:39:B7:A6:45:65:EF:F3:A8:EC:A1

            Authority Information Access: 
                OCSP - URI:http://ocsp.int-x3.letsencrypt.org
                CA Issuers - URI:http://cert.int-x3.letsencrypt.org/

            X509v3 Subject Alternative Name: 
                DNS:mysql.stan.monstermedia.hu
            X509v3 Certificate Policies: 
                Policy: 2.23.140.1.2.1
                Policy: 1.3.6.1.4.1.44947.1.1.1
                  CPS: http://cps.letsencrypt.org
                  User Notice:
                    Explicit Text: This Certificate may only be relied upon by Relying Parties and only in accordance with the Certificate Policy found at https://letsencrypt.org/repository/

    Signature Algorithm: sha256WithRSAEncryption
         85:2d:6f:e9:3a:c9:77:5d:76:b0:a9:94:2e:b8:35:db:24:2b:
         8c:40:8c:ae:42:69:fb:e3:8e:a7:01:02:34:3f:9d:1e:03:42:
         9e:aa:aa:d9:e4:03:83:d9:92:c0:3a:e3:f7:83:62:8d:d0:3a:
         f8:49:2b:00:da:cb:bd:5b:90:71:37:aa:26:ab:b0:01:a2:c0:
         1d:8e:f6:84:58:27:34:ce:3f:6b:75:19:9d:dd:33:b3:25:3a:
         1f:7f:c7:45:dd:4d:bf:61:9c:f0:45:d7:0b:45:8d:65:f5:27:
         f3:21:5c:ad:02:4a:87:b5:98:71:d8:d4:8a:c2:35:ec:4d:e2:
         dd:b7:bf:90:43:27:fa:0d:ef:4d:80:8f:71:8f:92:8f:d5:da:
         d8:00:74:16:bd:3b:63:af:f2:1a:89:b0:1c:d6:1c:ef:fd:0d:
         29:e9:d3:fe:db:6d:f5:e4:d5:af:2f:6f:be:be:06:92:1c:66:
         dd:27:33:5d:06:94:7f:46:53:51:d8:da:dc:7f:b2:2f:29:c3:
         0a:8c:d4:07:d8:95:ee:a3:88:4a:7e:f8:6f:6d:c0:ff:9d:1c:
         5d:5f:31:b2:7a:d1:2c:e7:c2:2a:25:db:1d:3c:86:10:8a:d7:
         b1:ef:3b:96:89:ce:41:b4:16:89:92:81:a9:9f:92:09:5c:95:
         e9:13:80:3a
-----BEGIN CERTIFICATE-----
MIIFFzCCA/+gAwIBAgISA1Pidi2xLXUNTUqyZoQOMsnxMA0GCSqGSIb3DQEBCwUA
MEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQD
ExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMzAeFw0xODAzMDMxMjQ0NTVaFw0x
ODA2MDExMjQ0NTVaMCUxIzAhBgNVBAMTGm15c3FsLnN0YW4ubW9uc3Rlcm1lZGlh
Lmh1MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtz+/11/TtbEdvD0V
Db9KVD/KDa2yBXxA6G853XzvlkdRH2eSKZW5j08OZ3a37tb4/fNsQZjPSV4tC0Ye
EpDRP5CBbbRQVx4TKe3iUS+PKoqQD250HDAdWXIa/65air+MC2glYGwdH9GmVu9m
m6uD+Rk/zRgH5OVBEx9fhmFl9F5Yk5r9TKYsYGAWco776fmbiaWXHdSN09frkuvJ
aBzikIDHi0zVXKQFJGKJXMUpkge21kiU9FWILe/o8ip4weAD3ihsLnGg5dhBwNFc
JQdyQ19RR0LbZ31/8xIVXsvHaYMEPYlVG6oIR3OOzWLnWvhCASpJ9r0bQlPn0c1t
5SMN9wIDAQABo4ICGjCCAhYwDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsG
AQUFBwMBBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBSUNH/EZmJ5
oT5GWBUHGQre5q7uuTAfBgNVHSMEGDAWgBSoSmpjBH3duubRObemRWXv86jsoTBv
BggrBgEFBQcBAQRjMGEwLgYIKwYBBQUHMAGGImh0dHA6Ly9vY3NwLmludC14My5s
ZXRzZW5jcnlwdC5vcmcwLwYIKwYBBQUHMAKGI2h0dHA6Ly9jZXJ0LmludC14My5s
ZXRzZW5jcnlwdC5vcmcvMCUGA1UdEQQeMByCGm15c3FsLnN0YW4ubW9uc3Rlcm1l
ZGlhLmh1MIH+BgNVHSAEgfYwgfMwCAYGZ4EMAQIBMIHmBgsrBgEEAYLfEwEBATCB
1jAmBggrBgEFBQcCARYaaHR0cDovL2Nwcy5sZXRzZW5jcnlwdC5vcmcwgasGCCsG
AQUFBwICMIGeDIGbVGhpcyBDZXJ0aWZpY2F0ZSBtYXkgb25seSBiZSByZWxpZWQg
dXBvbiBieSBSZWx5aW5nIFBhcnRpZXMgYW5kIG9ubHkgaW4gYWNjb3JkYW5jZSB3
aXRoIHRoZSBDZXJ0aWZpY2F0ZSBQb2xpY3kgZm91bmQgYXQgaHR0cHM6Ly9sZXRz
ZW5jcnlwdC5vcmcvcmVwb3NpdG9yeS8wDQYJKoZIhvcNAQELBQADggEBAIUtb+k6
yXdddrCplC64NdskK4xAjK5CafvjjqcBAjQ/nR4DQp6qqtnkA4PZksA64/eDYo3Q
OvhJKwDay71bkHE3qiarsAGiwB2O9oRYJzTOP2t1GZ3dM7MlOh9/x0XdTb9hnPBF
1wtFjWX1J/MhXK0CSoe1mHHY1IrCNexN4t23v5BDJ/oN702Aj3GPko/V2tgAdBa9
O2Ov8hqJsBzWHO/9DSnp0/7bbfXk1a8vb76+BpIcZt0nM10GlH9GU1HY2tx/si8p
wwqM1AfYle6jiEp++G9twP+dHF1fMbJ60Sznwiol2x08hhCK17HvO5aJzkG0FomS
gamfkglclekTgDo=
-----END CERTIFICATE-----
`


           var re = OpensslLib.ParseX509TextOutput(cert);
           assert.equal(re.Subject.CN, "mysql.stan.monstermedia.hu");

        })




        it('multiple certificates should be parsed well', function() {

           var re = OpensslLib.SplitX509Certificates(common.intermediateCert1+common.intermediateCert2)

           assert.ok(Array.isArray(re))
           for(var i = 0; i < re.length; i++){
              re[i] = re[i].trim()
           }
           assert.deepEqual(re,    [common.intermediateCert1.trim(), common.intermediateCert2.trim()])


        })


    })


 describe("certificate signing requests", function(){
     it("should be parsed well", function(){

           var re = OpensslLib.ParseCsrTextOutput(common.mainCsr)

            assert.deepEqual(re,     {
              "Subject": {
                 "C": "HU",
                 "CN": "www.monstermedia.hu",
                 "L": "Budapest",
                 "O": "Noname Domain Kft.",
                 "OU": "Monster Media",
                 "ST": "Hungary",
                 "text": "C=HU, ST=Hungary, L=Budapest, O=Noname Domain Kft., OU=Monster Media, CN=www.monstermedia.hu",
               },
              "Modulus": "00:b3:42:35:26:05:e3:f3:98:b7:a6:95:9a:5d:8a:f0:5c:7f:b1:0e:78:25:c1:83:ec:9f:3a:e8:39:22:df:43:5e:bd:46:6d:72:89:9c:97:d5:be:2f:e4:14:54:6d:19:34:79:19:9b:41:66:b0:0b:ab:8d:73:46:ec:ca:1e:87:26:66:cb:ca:ed:af:bd:bf:fb:3c:ad:c2:aa:df:d1:7e:39:d8:2b:dd:90:15:45:a8:b5:65:c1:ed:b3:75:50:3e:46:1f:d4:e4:81:1e:5f:9d:0d:1f:b8:39:2c:92:91:f4:8c:60:c9:f6:f5:64:c6:84:f0:f4:5b:d7:a6:6b:d1:f7:01:1d:16:9b:10:16:73:49:73:52:e8:99:e5:7c:da:d5:63:2e:92:60:dd:a7:1a:69:65:85:42:c8:15:c9:55:29:6f:9d:ae:4f:09:2f:2f:a4:cb:fb:5c:9f:04:d8:23:b9:19:cf:55:33:b6:29:1b:42:57:91:7a:b6:34:72:9f:c9:71:17:78:cc:f4:3c:6d:a1:d8:13:67:95:6d:da:85:9d:5f:2a:27:82:57:cd:c0:bb:29:b5:38:17:d5:64:85:be:fa:00:32:a7:f1:11:2a:b6:59:77:71:5a:20:ce:dc:6b:23:8d:05:c0:d1:57:61:b9:aa:0d:ec:6f:4c:73:84:15:29",
              "CERTIFICATE REQUEST": "-----BEGIN CERTIFICATE REQUEST-----\nfoobar\n-----END CERTIFICATE REQUEST-----\n"
            })

     })

     it("should reject invalid country codes", function(){

           try{
            OpensslLib.ParseCsrTextOutput(common.csrWithInvalidCountryCode)
            throw new Error("should not be here")
           }catch(ex){
            assert.propertyVal(ex, "message", "SUBJECT_COUNTRY_MUST_BE_ISO_3166_1")
           }


     })

     it("even when there is email address in the output", function(){
        var csrOutput = `
Certificate Request:
    Data:
        Version: 0 (0x0)
        Subject: C=HU, ST=Zala, L=Nagykanizsa, O=ETIT[nwpro] KFT, OU=ETC1, CN=*.etit.hu/emailAddress=info@etit.hu
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (4096 bit)
                Modulus:
                    00:b2:eb:5b:44:c0:66:5e:38:6e:79:f4:71:b3:0a:
                    99:3f:66:03:68:90:47:d9:4d:b0:38:f9:0a:4c:5e:
                    92:eb:40:c2:a5:63:6f:38:28:2d:e8:ed:d1:4b:d8:
                    2d:68:a9:cc:dc:90:29:38:26:01:6d:8a:53:05:37:
                    e8:35:ed:4a:d3:b8:a3:2a:58:c2:3e:3d:8d:dd:55:
                    af:63:19:e6:74:9a:50:5d:ef:8c:7a:2d:8d:33:2c:
                    e8:37:9b:1b:c5:68:1a:ad:d1:00:00:26:48:8b:53:
                    21:e7:42:ad:1e:9a:06:cc:b2:7a:45:c2:be:a4:be:
                    9a:5b:4e:54:ea:78:0a:2a:5f:75:53:0c:b4:1c:bb:
                    a0:5d:a4:0c:26:95:7e:ce:ad:46:27:22:da:44:90:
                    97:1d:02:93:62:70:d9:23:6a:6d:28:1c:bd:b9:2d:
                    a6:a9:13:11:ff:fa:2b:1c:ed:e4:58:b9:c1:bb:b8:
                    df:cb:ec:81:e7:99:da:c1:02:14:bf:2d:e0:f6:36:
                    6a:48:7f:9d:3d:ff:16:c8:b1:63:28:c7:1b:de:c8:
                    7e:5a:89:92:5f:d2:63:7e:c7:ff:de:0a:af:9c:35:
                    c8:a2:6c:97:dc:80:c1:df:0a:40:0e:ce:9d:9e:7a:
                    42:f2:c4:98:ca:b6:dc:01:3c:05:87:76:19:e3:2d:
                    83:18:db:34:78:94:c3:04:e2:29:69:52:66:26:cc:
                    22:17:ed:9b:0b:8b:ce:27:c3:41:df:bb:d8:9f:de:
                    86:a6:c9:6e:73:d8:cb:85:cf:8d:74:a2:aa:5a:f6:
                    1e:f8:f2:fe:f7:f0:ff:dc:4b:ee:38:8c:8c:ed:38:
                    79:4d:09:27:b5:89:be:e8:cf:f8:42:17:24:3d:bb:
                    37:44:25:e9:75:99:77:69:e3:16:f2:2f:96:bc:ed:
                    9d:b9:69:47:22:4d:38:16:09:0a:46:d2:15:68:d2:
                    f7:6a:99:c6:b6:c9:29:51:94:65:e8:6f:c6:20:7e:
                    09:a7:33:cd:1d:32:ed:61:a9:cb:52:d6:07:2e:9e:
                    31:c3:97:49:13:f0:05:30:ad:fc:6a:fb:84:55:fb:
                    b5:70:08:f0:6b:e9:ab:53:41:e3:e3:7c:fb:9f:5b:
                    68:fa:ca:f9:14:bb:63:ee:a2:99:aa:e9:39:cc:b6:
                    f2:c9:4d:2c:53:8e:73:ec:ff:bc:1a:83:2e:93:38:
                    02:5e:19:b9:ba:4c:7c:63:c1:eb:90:d8:3a:1e:b5:
                    ae:76:63:54:35:26:20:b2:dd:1d:8d:db:92:d9:d2:
                    c3:91:20:c8:ac:19:19:0b:63:9d:88:8f:ae:38:19:
                    d9:ff:c6:65:92:d1:6f:d0:5e:28:7a:cf:c8:b5:47:
                    21:24:5f
                Exponent: 65537 (0x10001)
        Attributes:
            challengePassword        :unable to print attribute
    Signature Algorithm: sha256WithRSAEncryption
         7c:a4:81:10:de:d9:3b:d5:f3:e8:22:f8:20:12:9c:44:11:fb:
         10:59:68:97:c0:59:25:58:8e:b3:7b:4a:e9:53:b9:58:2b:3e:
         b6:04:cc:a8:8e:55:69:90:bc:c1:3f:25:6e:bd:7b:91:35:69:
         fc:60:f0:93:b2:7e:b1:40:96:fa:2e:f7:55:e9:7b:c5:c9:16:
         3b:cf:7b:f6:33:55:de:a6:5d:eb:c7:87:3e:82:7c:28:18:04:
         0f:37:54:18:7b:0f:57:b5:01:93:4a:ef:df:a8:38:a3:e1:2f:
         65:c2:a3:7b:03:c4:5e:5f:00:68:8a:62:4f:f0:0d:d7:9b:f8:
         cb:39:b6:eb:48:69:87:8d:c0:e2:dd:61:2e:ea:54:1d:99:b6:
         bb:16:0f:e0:34:68:7d:b9:0e:1e:32:ca:7e:1f:5c:75:09:85:
         89:e5:30:21:3b:5a:12:73:81:8d:1c:6d:bb:78:bf:92:f0:7b:
         23:4f:be:ca:92:66:f1:db:d6:d0:71:97:fc:e9:2f:bc:c3:67:
         8a:21:01:69:55:af:c8:fa:59:aa:f7:d4:64:76:e6:9d:92:c5:
         46:ab:c8:d9:e2:ea:95:f7:9c:36:06:f3:64:45:66:49:63:f8:
         7b:37:40:97:f0:b1:9b:68:0b:b5:fe:c2:98:9f:76:a3:29:10:
         a9:63:6b:df:9b:1a:91:c3:b5:e0:99:92:7d:52:58:a9:3f:3c:
         0f:8e:0a:a2:e9:a7:ba:18:3f:2f:e8:1c:fa:be:84:1b:1f:bd:
         35:7a:53:9d:17:ee:cc:25:b0:3a:d1:37:29:24:02:92:5b:6e:
         a3:e7:99:46:e1:bc:b9:04:5a:64:66:7d:15:ed:8f:42:65:72:
         d7:4a:28:37:44:35:90:89:54:e0:89:52:a3:2a:2b:b8:6f:f0:
         a2:e1:92:29:20:7f:75:9e:87:b5:05:91:8c:7d:62:c5:c6:86:
         4c:87:1e:20:f8:61:88:2f:bc:70:b4:e9:bf:a3:71:70:f3:f4:
         1f:3d:ed:df:18:63:ec:d8:d2:c7:63:30:2f:28:87:28:78:80:
         f1:6a:89:ce:ca:7b:88:15:6a:ca:c5:16:c6:13:54:a9:66:b6:
         c9:62:ad:ba:f0:de:ef:27:3c:dc:2e:f8:11:91:e4:62:28:00:
         ed:66:ff:00:7c:cf:c9:39:cc:74:02:ff:3c:55:6e:27:4f:31:
         8c:21:ae:c1:13:78:b1:94:67:aa:0b:c4:3b:1c:a7:3b:f4:cc:
         c9:cc:e8:59:09:dc:28:d9:40:ac:46:be:e5:61:5a:11:84:98:
         e1:e5:3e:d5:a0:c0:51:a0:a1:d6:4f:2d:a9:59:84:c3:b1:3b:
         27:f0:72:b2:86:84:a5:90
-----BEGIN CERTIFICATE REQUEST-----
MIIE5zCCAs8CAQAwgYwxCzAJBgNVBAYTAkhVMQ0wCwYDVQQIDARaYWxhMRQwEgYD
VQQHDAtOYWd5a2FuaXpzYTEYMBYGA1UECgwPRVRJVFtud3Byb10gS0ZUMQ0wCwYD
VQQLDARFVEMxMRIwEAYDVQQDDAkqLmV0aXQuaHUxGzAZBgkqhkiG9w0BCQEWDGlu
Zm9AZXRpdC5odTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBALLrW0TA
Zl44bnn0cbMKmT9mA2iQR9lNsDj5CkxekutAwqVjbzgoLejt0UvYLWipzNyQKTgm
AW2KUwU36DXtStO4oypYwj49jd1Vr2MZ5nSaUF3vjHotjTMs6DebG8VoGq3RAAAm
SItTIedCrR6aBsyyekXCvqS+mltOVOp4CipfdVMMtBy7oF2kDCaVfs6tRici2kSQ
lx0Ck2Jw2SNqbSgcvbktpqkTEf/6Kxzt5Fi5wbu438vsgeeZ2sECFL8t4PY2akh/
nT3/FsixYyjHG97IflqJkl/SY37H/94Kr5w1yKJsl9yAwd8KQA7OnZ56QvLEmMq2
3AE8BYd2GeMtgxjbNHiUwwTiKWlSZibMIhftmwuLzifDQd+72J/ehqbJbnPYy4XP
jXSiqlr2Hvjy/vfw/9xL7jiMjO04eU0JJ7WJvujP+EIXJD27N0Ql6XWZd2njFvIv
lrztnblpRyJNOBYJCkbSFWjS92qZxrbJKVGUZehvxiB+CaczzR0y7WGpy1LWBy6e
McOXSRPwBTCt/Gr7hFX7tXAI8Gvpq1NB4+N8+59baPrK+RS7Y+6imarpOcy28slN
LFOOc+z/vBqDLpM4Al4ZubpMfGPB65DYOh61rnZjVDUmILLdHY3bktnSw5EgyKwZ
GQtjnYiPrjgZ2f/GZZLRb9BeKHrPyLVHISRfAgMBAAGgFTATBgkqhkiG9w0BCQcx
BgwEcGluYTANBgkqhkiG9w0BAQsFAAOCAgEAfKSBEN7ZO9Xz6CL4IBKcRBH7EFlo
l8BZJViOs3tK6VO5WCs+tgTMqI5VaZC8wT8lbr17kTVp/GDwk7J+sUCW+i73Vel7
xckWO8979jNV3qZd68eHPoJ8KBgEDzdUGHsPV7UBk0rv36g4o+EvZcKjewPEXl8A
aIpiT/AN15v4yzm260hph43A4t1hLupUHZm2uxYP4DRofbkOHjLKfh9cdQmFieUw
ITtaEnOBjRxtu3i/kvB7I0++ypJm8dvW0HGX/OkvvMNniiEBaVWvyPpZqvfUZHbm
nZLFRqvI2eLqlfecNgbzZEVmSWP4ezdAl/Cxm2gLtf7CmJ92oykQqWNr35sakcO1
4JmSfVJYqT88D44Koumnuhg/L+gc+r6EGx+9NXpTnRfuzCWwOtE3KSQCkltuo+eZ
RuG8uQRaZGZ9Fe2PQmVy10ooN0Q1kIlU4IlSoyoruG/wouGSKSB/dZ6HtQWRjH1i
xcaGTIceIPhhiC+8cLTpv6NxcPP0Hz3t3xhj7NjSx2MwLyiHKHiA8WqJzsp7iBVq
ysUWxhNUqWa2yWKtuvDe7yc83C74EZHkYigA7Wb/AHzPyTnMdAL/PFVuJ08xjCGu
wRN4sZRnqgvEOxynO/TMyczoWQncKNlArEa+5WFaEYSY4eU+1aDAUaCh1k8tqVmE
w7E7J/BysoaEpZA=
-----END CERTIFICATE REQUEST-----
`

              var re = OpensslLib.ParseCsrTextOutput(csrOutput)

              console.log(re.Subject)

              assert.property(re, "Subject")

              assert.deepEqual(re.Subject, { text: 'C=HU, ST=Zala, L=Nagykanizsa, O=ETIT[nwpro] KFT, OU=ETC1, CN=*.etit.hu/emailAddress=info@etit.hu',
     C: 'HU',
     ST: 'Zala',
     L: 'Nagykanizsa',
     O: 'ETIT[nwpro] KFT',
     OU: 'ETC1',
     emailAddress: 'info@etit.hu',
     CN: '*.etit.hu' })




     })
 })

describe('private keys', function() {


        it('private key should be parsed well', function() {

           var re = OpensslLib.ParseRsaTextOutput(common.mainPrivKey)

            assert.deepEqual(re,     {
              "Private-Key": "(2048 bit)",
              "Modulus": "00:b3:42:35:26:05:e3:f3:98:b7:a6:95:9a:5d:8a:f0:5c:7f:b1:0e:78:25:c1:83:ec:9f:3a:e8:39:22:df:43:5e:bd:46:6d:72:89:9c:97:d5:be:2f:e4:14:54:6d:19:34:79:19:9b:41:66:b0:0b:ab:8d:73:46:ec:ca:1e:87:26:66:cb:ca:ed:af:bd:bf:fb:3c:ad:c2:aa:df:d1:7e:39:d8:2b:dd:90:15:45:a8:b5:65:c1:ed:b3:75:50:3e:46:1f:d4:e4:81:1e:5f:9d:0d:1f:b8:39:2c:92:91:f4:8c:60:c9:f6:f5:64:c6:84:f0:f4:5b:d7:a6:6b:d1:f7:01:1d:16:9b:10:16:73:49:73:52:e8:99:e5:7c:da:d5:63:2e:92:60:dd:a7:1a:69:65:85:42:c8:15:c9:55:29:6f:9d:ae:4f:09:2f:2f:a4:cb:fb:5c:9f:04:d8:23:b9:19:cf:55:33:b6:29:1b:42:57:91:7a:b6:34:72:9f:c9:71:17:78:cc:f4:3c:6d:a1:d8:13:67:95:6d:da:85:9d:5f:2a:27:82:57:cd:c0:bb:29:b5:38:17:d5:64:85:be:fa:00:32:a7:f1:11:2a:b6:59:77:71:5a:20:ce:dc:6b:23:8d:05:c0:d1:57:61:b9:aa:0d:ec:6f:4c:73:84:15:29",
              "RSA PRIVATE KEY": "-----BEGIN RSA PRIVATE KEY-----\nBlaBlaBla\n-----END RSA PRIVATE KEY-----\n"
            })


        })

        it("matching", function(){
            assert.equal("", OpensslLib.arePrivateKeyAndCertMatching(OpensslLib.ParseRsaTextOutput(common.mainPrivKey), OpensslLib.ParseX509TextOutput(common.mainCert)))
        })



        it("fake private keys should be rejected", function(){


          try{
            OpensslLib.ParseRsaTextOutput(common.fakeRsaPrivateKey)
            throw new Error("should have failed")
          }catch(ex){
            assert.propertyVal(ex, "message", "FAKE_PRIVATE_KEY")
          }


        })

        it("password protected private keys should be rejected", function(){


          try{
            OpensslLib.ParseRsaTextOutput(rsaPasswordProtectedPrivateKey)
            throw new Error("should have failed")
          }catch(ex){
            assert.propertyVal(ex, "message", "INVALID_PRIVATE_KEY")
          }


        })

        it('GenerateRawRSAPrivateKey', function(){

           var o = OpensslLib(app)
           var oCommander = app.commander
           app.commander = {
             spawn: function(cmd, params){
                var e = simpleCloneObject(spawnParameters)
                e = e.concat({ keylength: 2048 })
                assert.deepEqual(cmd, { omitControlMessages: true,omitAggregatedStderr: true,
                  removeImmediately: true,
  chain:
   { executable: '[openssl_path]',
     args: [ 'genrsa', '[keylength]' ] } })
                assert.deepEqual(params, e)

                return Promise.resolve({id: "crap",executionPromise: Promise.resolve({output: rsaPrivateKey})})
             }
           }
           return o.GenerateRawRSAPrivateKey(2048).then(p=>{
              assert.equal(p, rsaPrivateKey)
              app.commander = oCommander
           })
        })


    })



  describe('certificates through rest', function() {
    it("certificate", function(done){
        var cert = `
-----BEGIN CERTIFICATE-----
cert0
-----END CERTIFICATE-----
`

             var oCommander = app.commander
             app.commander = {
              spawn: function(app, params) {
                assert.deepEqual(app, { removeImmediately: true, chain:
   { executable: '[openssl_path]',
     args: [ 'x509', '-text' ],
     stdin: '-----BEGIN CERTIFICATE-----\ncert0\n-----END CERTIFICATE-----\n' } })
                assert.deepEqual(params, spawnParameters)
                return Promise.resolve({
                   executionPromise: Promise.resolve({output: "Not Before: foobar\n"+cert})
                })
              }
             }

             mapi.post( "/info/certificates", {certificatesText: cert}, function(err, result, httpResponse){
                // console.log("here",err, result)

                // note: CERTIFICATE should be not present
                assert.deepEqual(result, [ {"Not Before": {text:"foobar", iso: "Invalid date"}} ])

                app.commander = oCommander
                done()

             })


    })
  })

  describe('private keys through rest', function() {

    it("private key", function(done){
        var privKey = `
-----BEGIN RSA PRIVATE KEY-----
BlaBlaBla
-----END RSA PRIVATE KEY-----
`

             var oCommander = app.commander
             app.commander = {
              spawn: function(app, params) {
                assert.deepEqual(app, { removeImmediately: true, 
     dontLog_stdout: true,
                    chain:
   { executable: '[openssl_path]',
     args: [ 'rsa', '-text', '-passin', 'pass:' ],
     stdin: privKey } })
                assert.deepEqual(params, spawnParameters)
                return Promise.resolve({
                   executionPromise: Promise.resolve({output: "Private-Key: (2048 bit)\n"+privKey})
                })
              }
             }

             mapi.post( "/info/private-key", {privateKeyText: privKey}, function(err, result, httpResponse){
                // console.log("here",err, result)

                // note: RSA PRIVATE KEY should be not present
                assert.deepEqual(result, {"Private-Key": "(2048 bit)"})

                app.commander = oCommander
                done()

             })


    })

    it("csr through rest", function(done){
        var csr = `
-----BEGIN CERTIFICATE REQUEST-----
foobar
-----END CERTIFICATE REQUEST-----
`

             var oCommander = app.commander
             app.commander = {
              spawn: function(app, params) {
                assert.deepEqual(app, { removeImmediately: true, chain:
   { executable: '[openssl_path]',
     args: [ 'req', '-text' ],
     stdin: csr } })
                assert.deepEqual(params, spawnParameters)
                return Promise.resolve({
                   executionPromise: Promise.resolve({output: common.mainCsr})
                })
              }
             }

             mapi.post( "/info/certificate-signing-request", {csr: csr}, function(err, result, httpResponse){
                // console.log("here",err, result)

                // note: RSA PRIVATE KEY should be not present
                assert.property(result, "Subject")

                app.commander = oCommander
                done()

             })


    })

  })


  describe("generating conf", function(){
    it("for some domains", function(){

       var t = OpensslLib.generateOpensslConfForDomains(common.typicalContact, ["domain1.hu", "domain2.hu"])
       const expected = `[req]
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
C=HU
ST=State
L=City
O=Organization
emailAddress=hostmaster@domain1.hu
CN = domain1.hu

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = domain1.hu
DNS.2 = domain2.hu
`

       assert.equal(t, expected)

    })

  })

  describe("generating csr", function(){
    it("for some domains", function(){
       const certSigningRequest = "-- BEGIN SIGNING REQUEST --\nfoo\n-- END SIGNING REQUEST --\n"
       var oCommander = app.commander
       app.commander = {
        spawn: function(cmd, params) {
           /*
           console.log("cmd", cmd)
           console.log("params", params)
           */

           assert.deepEqual(cmd, {omitControlMessages: true, removeImmediately: true, chain: {executable: '[openssl_path]',
  args:
   [ 'req',
     '-new',
     '-key',
     '[path_rsa_private_key]',
     '-config',
     '[path_config_file]' ] }})

           assert.equal(params.length, spawnParameters.length+1)
           assert.property(params[spawnParameters.length], "path_rsa_private_key")
           assert.property(params[spawnParameters.length], "path_config_file")


            return Promise.resolve({id:"csr", executionPromise: Promise.resolve({output: certSigningRequest})})


        }
       }
       var o = OpensslLib(app)
       return o.GenerateCsrForDomains(rsaPrivateKey, common.typicalContact, ["domain1.hu", "domain2.hu"])
         .then(csr=>{
              assert.equal(csr, certSigningRequest)
              app.commander = oCommander

         })

    })

  })


}, 10000)


