require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../cert-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const common = require("00-common.js")

    const user = 12346
    const webhosting = 16388

    const spawnParameters = [app.config.get("cmd_pathes"), app.config.get("common_params")]

    const fs = require("MonsterDotq").fs()
    const path = require("path")



    const expectedPurchase= {
          "actions": [
            "vendor1-activate"
          ],
          "pc_activation": null,
          "pc_vendor": "Let's Encrypt",
          "pc_product": "Let's Encrypt",
          "pc_reseller": "letsencrypt",
          "pc_reseller_certificate_id": null,
          "pc_local": null,
          "pc_certificate": null,
          "pc_subject_common_name": null,
          "pc_product_parameters": {years:2},
          "pc_misc": {},
          "pc_comment": "",
          "pc_begin": null,
          "pc_end": null,
          "pc_status": "pending",
          "pc_user_id": ""+user,
    }

    var m = common.mocking(assert, app, user)
    m.setupMonsterInfoWebhosting(webhosting, user)
    m.setupMonsterInfoAccount()


    const taskId = "letsEncrypt"
    var spawnCalls
    var activationCommander = function(expectedDomain, closeCallback){
      var emitter;
      if(closeCallback)
        emitter = {
                    send_stdout_ln: function(msg){
                        console.log("msg via send_stoud_ln:", msg);
                    },
                    close: function(x){
                      return closeCallback(x);
                    },
                    spawn: function(){
                       console.log("spawn was called");
                       return Promise.resolve({id:"emitter-id"});
                    }
                  }
      return {
                EventEmitter: function(){
                  return emitter;
                },
                spawn: function(cmd, params){
                    spawnCalls++
                    // console.log("c", cmd, "p", params)

                    if(spawnCalls == 1) {
                        // console.log(cmd)

                        assert.deepEqual(cmd, { chain:
                               { executable: '[letsencrypt_binary_path]',
                                 args:
                                  [ 'certonly',
                                    '--config-dir',
                                    '[letsencrypt_config_dir_path]',
                                    '--work-dir',
                                    '[letsencrypt_work_dir_path]',
                                    '--logs-dir',
                                    '[letsencrypt_logs_dir_path]',
                                    '-w',
                                    '[domain_validation_letsencrypt_webroot_directory]',
                                    '-d',
                                    expectedDomain,
                                    '-d',
                                    'www.'+expectedDomain ] },
                              emitter: emitter,
                              executeImmediately: true,
                              removeImmediately: true })
                        assert.deepEqual(params,spawnParameters)

                        var mainDir = app.config.get("common_params").domain_validation_letsencrypt_live_directory
                        var createFilesProm = fs.mkdirAsync(path.join(mainDir, expectedDomain)).catch(ex=>{})
                          .then(()=>{
                            var ps = []
                            Array({f:"cert",k:common.mainCert},{f:"privkey",k:common.mainPrivKey},{f:"chain",k:common.intermediateCert1}).forEach(a=>{
                                ps.push(fs.writeFileAsync(path.join(mainDir,expectedDomain,a.f+".pem"), a.k))
                            })
                            return Promise.all(ps)
                           })

                        return Promise.resolve({id:taskId, executionPromise: createFilesProm})
                    }

                    return Promise.resolve({id:taskId+spawnCalls, executionPromise: Promise.resolve({output: cmd.chain.stdin})})

                }
        }
    }

    var pc_id
    var c_id
    describe("purchases", function(){

        shouldBeEmpty()

        putPurchase()


        Array("/purchases/by-user/"+user, "/purchases/all").forEach(uri=>{
            it("the certificate just added should be returned: "+uri, function(done){

                     mapi.get( uri, function(err, result, httpResponse){
                        assert.property(result, "purchases")
                        assert.ok(Array.isArray(result.purchases))
                        assert.equal(result.purchases.length, 1)

                        assert.propertyVal(result.purchases[0], "pc_id", pc_id)
                        Array("pc_id","created_at").forEach(r=>{

                           assert.property(result.purchases[0], r)
                           delete result.purchases[0][r]
                        })

                        assert.deepEqual(result, { purchases:[expectedPurchase]})
                        done()

                     })

            })

        })

        it("changing some params", function(done){

                 mapi.post( "/purchases/by-user/"+user+"/item/"+pc_id+"/raw", {pc_comment:"foobar"},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.equal(result, "ok")
                    done()
                 })

        })

            it("the certificate just added should be returned with the new comment", function(done){

                     mapi.get( "/purchases/all/item/"+pc_id, function(err, result, httpResponse){

                        assert.propertyVal(result, "pc_comment", "foobar")
                        done()

                     })

            })

        it("listing activations should be empty", function(done){

                 mapi.get( "/purchases/by-user/"+user+"/item/"+pc_id+"/activations",
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.deepEqual(result, {activations:[]})
                    done()
                 })

        })

        it("activating this letsencrypt purchase", function(done){


            var oGetRelayerMapiPool = app.GetRelayerMapiPool
            app.GetRelayerMapiPool = function(){
              throw new Error("should not have called")
            }

            var mailWasSent = false
            var httpReturned = false

            var oGetMailer = app.GetMailer
            app.GetMailer = function() {
              return {
                SendMailAsync: function(d){
                   assert.propertyVal(d, "template", 'cert/certificate-enrolled')
                   assert.property(d, "context")
                   assert.property(d.context, "purchase")
                   assert.propertyVal(d.context.purchase, "pc_subject_common_name", "www.monstermedia.hu")
                   assert.ok(d.context.purchase.pc_end)

                   assert.property(d, "attachments")
                   assert.equal(d.attachments.length, 2)

                   mailWasSent = true
                   checkForDone()
                   return Promise.resolve()
                }
              }
            }

            var oCommander = app.commander
            spawnCalls = 0
            var mainDomain = "maindomain1.hu"
            app.commander = activationCommander(mainDomain)

                 mapi.put( "/purchases/by-user/"+user+"/item/"+pc_id+"/activations", {local:true, domains:[mainDomain,"www."+mainDomain]},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.property(result, "a_id")
                    assert.property(result, "c_id")
                    c_id = result.c_id

                    assert.equal(spawnCalls, 1+3)

                    httpReturned = true
                    checkForDone()
                 })

            function checkForDone(){
               if(!httpReturned) return
               if(!mailWasSent) return

               app.commander = oCommander
               app.GetMailer = oGetMailer
               return done()
            }

        })

        certificateForTheActivationShouldBeFound()

        it("listing purchases by user should list this one with the new parameters", function(done){

                 mapi.get( "/purchases/by-user/"+user, function(err, result, httpResponse){

                    assert.equal(result.purchases.length, 1)
                    assert.property(result.purchases[0], "pc_activation")
                    assert.property(result.purchases[0], "pc_certificate")
                    assert.propertyVal(result.purchases[0], "pc_status", "issued")
                    Array("pc_begin","pc_end","pc_subject_common_name").forEach(c=>{
                        assert.ok(result.purchases[0][c])
                    })
                    done()

                 })

        })


        removeIt()

        certificateForTheActivationShouldBeFound()

        shouldBeEmpty()


    })

    describe("activating certificate along with attaching", function(){

        putPurchase()

        it("activating letsencrypt purchases should be rejected when attempting to attach to external server", function(done){


                 var mainDomain = "whatever.com"
                 mapi.put( "/purchases/by-user/"+user+"/item/"+pc_id+"/activations",
                  {
                    domains:[mainDomain,"www."+mainDomain],
                    attach: {server: "s3", webhosting_id: webhosting, docroot_domain: mainDomain}
                  },
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.propertyVal(err, "message", "LETSENCRYPT_IS_LOCAL_ONLY")
                    done()
                 })

        })

        it("activating purchase", function(done){

            var c_id = "abcd1234"
            var webhosting_id = webhosting
            var mainDomain = "maindomain2.hu"
            var server = app.config.get("mc_server_name")
            var stat = common.setupRelayerMapiPoolForCertInsert(assert, app, c_id, server, webhosting_id, mainDomain)

            var oCommander = app.commander
            const taskId = "letsEncrypt"
            spawnCalls = 0
            app.commander = activationCommander(mainDomain)

                 mapi.put( "/purchases/by-user/"+user+"/item/"+pc_id+"/activations",
                  {
                    local: true,
                    domains:[mainDomain,"www."+mainDomain],
                    attach: {server: server, webhosting_id: webhosting_id, docroot_domain: mainDomain}
                  },
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.property(result, "c_id")
                    c_id = result.c_id

                    assert.equal(stat.putCert, 1)
                    assert.equal(stat.postDocroot, 1)
                    assert.equal(spawnCalls, 1)
                    app.commander = oCommander
                    done()
                 })

        })

        it("listing purchases by user should list this one with the new parameters", function(done){

                 mapi.get( "/purchases/by-user/"+user, function(err, result, httpResponse){

                    assert.equal(result.purchases.length, 1)
                      //  console.log("foo!?", result)

                    assert.property(result.purchases[0], "pc_activation")
                    assert.property(result.purchases[0], "pc_certificate")
                    assert.propertyVal(result.purchases[0], "pc_status", "issued")
                    Array("pc_begin","pc_end","pc_subject_common_name").forEach(c=>{
                        assert.ok(result.purchases[0][c])
                    })
                    done()

                 })

        })

        removeIt()

        shouldBeEmpty()


    })

    describe("dedicated letsencrypt feature", function(){

        var webhosting_id = webhosting
        var mainDomain = "letsencrypt.hu"
        var server = app.config.get("mc_server_name")

        var expected_original_c_id = "abcd1235"
        var expected_reissued_c_id = "reissued12"
        var payload = {
                    domains:[mainDomain,"www."+mainDomain],
                    attach: {server: server, webhosting_id: webhosting_id, docroot_domain: mainDomain}
                  }

        letsencrypt_add_and_activate_test(payload, expected_original_c_id, server, webhosting_id, mainDomain);


        var letsencrypt_pc_id
        it("listing purchases by user should list this one just added/activated", function(done){

                 mapi.get( "/purchases/by-user/"+user, function(err, result, httpResponse){

                    assert.equal(result.purchases.length, 1)
                    // console.log("xxx foo!?", result)

                    assert.property(result.purchases[0], "pc_activation")
                    assert.property(result.purchases[0], "pc_certificate")
                    assert.propertyVal(result.purchases[0], "pc_status", "issued")
                    Array("pc_begin","pc_end","pc_subject_common_name").forEach(c=>{
                        assert.ok(result.purchases[0][c])
                    })
                    letsencrypt_pc_id = result.purchases[0].pc_id
                    done()

                 })

        })

        it("activation should have the original parameters", function(done){

                 mapi.get( "/purchases/by-user/"+user+"/item/"+letsencrypt_pc_id+"/activations", function(err, result, httpResponse){

                    assert.property(result, "activations")
                    assert.equal(result.activations.length, 1)
                    // console.log("xxx foo!?", result.activations[0]); process.reallyExit();

                    assert.propertyVal(result.activations[0], "a_purchase", letsencrypt_pc_id)
                    assert.propertyVal(result.activations[0], "a_status", "successful")
                    console.log(JSON.stringify(payload))
                    console.log(JSON.stringify(result.activations[0].a_payload))
                    assert.deepEqual(result.activations[0].a_payload, extend({},payload,{local:true}))
                    assert.deepEqual(result.activations[0].a_extra, {mainDomain: mainDomain})

                    done()

                 })

        })

        it("calling the letsencrypt route with the same params once again should fail", function(done){

                 mapi.put( "/purchases/by-user/"+user+"/letsencrypt", payload,
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.propertyVal(err, "message", "LETSENCRYPT_ALREADY_ACTIVATED")
                    done()
                 })



        })


        it("reissue should reissue", function(done){

            var c_id = "abcd1236"
            var stat = common.setupRelayerMapiPoolForCertInsert(assert, app, expected_reissued_c_id, server, webhosting_id, mainDomain)

            var oCommander = app.commander
            const taskId = "letsEncrypt"
            spawnCalls = 0
            app.commander = activationCommander(mainDomain)

                 mapi.post( "/purchases/by-user/"+user+"/item/"+letsencrypt_pc_id+"/reissue", {}, function(err, result, httpResponse){

                    assert.propertyVal(result, "c_id", expected_reissued_c_id)

                    assert.equal(stat.putCert, 1)
                    assert.equal(stat.postDocroot, 1)
                    assert.equal(spawnCalls, 1)
                    app.commander = oCommander
                    done()

                 })

        })

        it("listing purchases again should list this very latest one just reissued", function(done){

                 mapi.get( "/purchases/by-user/"+user, function(err, result, httpResponse){

                    assert.equal(result.purchases.length, 1)
                    // console.log("xxx foo!?", result)

                    assert.property(result.purchases[0], "pc_activation")
                    assert.propertyVal(result.purchases[0], "pc_certificate", expected_reissued_c_id)
                    assert.propertyVal(result.purchases[0], "pc_status", "issued")
                    Array("pc_begin","pc_end","pc_subject_common_name").forEach(c=>{
                        assert.ok(result.purchases[0][c])
                    })
                    done()

                 })

        })


            it("revoking the certificate", function(done){

                var spawnCalls = 0
                var oCommander = app.commander
                app.commander =  {
                        spawn: function(cmd, params){
                            spawnCalls++
                            // console.log("c", cmd, "p", params)

                            if(spawnCalls == 1) {
                                // console.log(cmd)

                                assert.deepEqual(cmd, { chain:
                                       { executable: '[letsencrypt_binary_path]',
                                         args:
                                          [ 'revoke',
                                            '--config-dir',
                                            '[letsencrypt_config_dir_path]',
                                            '--work-dir',
                                            '[letsencrypt_work_dir_path]',
                                            '--logs-dir',
                                            '[letsencrypt_logs_dir_path]',
                                            "--key-path",
                                            "[private_key_path]",
                                            "--cert-path",
                                            "[certificate_path]",
                                          ] },
                                      executeImmediately: true,
                                      removeImmediately: true })
                                assert.ok(Array.isArray(params))
                                assert.equal(params.length, 3)
                                assert.property(params[2], "private_key_path")
                                assert.property(params[2], "certificate_path")

                                return Promise.resolve({id:"revoketask", executionPromise: Promise.resolve()})
                            }

                            throw new Error("Unexpected call")

                        }
                }

                app.PostForm = function(url, data){
                   throw new Error("no")
                }

                 mapi.post( "/purchases/by-user/"+user+"/item/"+letsencrypt_pc_id+"/revoke", {},
                     function(err, result, httpResponse){
                        // console.log("re", result, err)
                        assert.equal(result, "ok")
                        assert.equal(spawnCalls, 1)
                        app.commander = oCommander

                        done()
                     })
            })

        it("should be revoked", function(done){

                 mapi.get( "/purchases/by-user/"+user, function(err, result, httpResponse){

                    assert.propertyVal(result.purchases[0], "pc_status", "revoked")
                    done()

                 })

        })


    })



    describe("renewal", function(){

        var webhosting_id = webhosting
        var mainDomain = "renewal.hu"
        var server = app.config.get("mc_server_name")

        var expected_original_c_id = "abcd1235"
        var expected_reissued_c_id = "reissued12"
        var payload = {
                    domains:[mainDomain,"www."+mainDomain],
                    attach: {server: server, webhosting_id: webhosting_id, docroot_domain: mainDomain}
                  }

        var original_pc_id
        var renewed_pc_id

        letsencrypt_add_and_activate_test(payload, expected_original_c_id, server, webhosting_id, mainDomain);

        it("listing purchases by user should list this one with the new parameters", function(done){

                 mapi.get( "/purchases/all", function(err, result, httpResponse){

                    assert.equal(result.purchases.length, 2)
                    original_pc_id =result.purchases[1].pc_id;
                    done()

                 })

        })


        it("calling renew", function(done){

                 mapi.post( "/purchases/by-user/"+user+"/item/"+original_pc_id+"/renew", {},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.property(result, "pc_id")
                    assert.ok(result.pc_id)
                    renewed_pc_id = result.pc_id
                    done()
                 })

        })


        it("status of the old one should be renewed", function(done){

                 mapi.get( "/purchases/by-user/"+user+"/item/"+original_pc_id,
                 function(err, result, httpResponse){
                    assert.propertyVal(result, "pc_status", "renewed")
                    assert.deepEqual(result.actions, [])
                    done()
                 })

        })

        it("status of the new one should be pending", function(done){

                 mapi.get( "/purchases/by-user/"+user+"/item/"+renewed_pc_id,
                 function(err, result, httpResponse){
                    assert.propertyVal(result, "pc_status", "pending")
                    assert.property(result, "pc_comment")
                    assert.ok(result.pc_comment)
                    assert.property(result, "pc_activation")
                    assert.notOk(result.pc_activation)
                    done()
                 })

        })


            it("listing purchases which have a turndate coming", function(done){

                     mapi.post( "/purchases/all/turndatecoming", {}, function(err, result, httpResponse){

                        assert.property(result, "purchases")
                        assert.equal(result.purchases.length, 0)
                        done()

                     })

            })

    })


    describe("cleanup", function(){

        var webhosting_id = 33333
        var mainDomain = "cleanup.hu"
        var server = app.config.get("mc_server_name")

        var expected_original_c_id = "333331"
        var payload = {
                    domains:[mainDomain,"www."+mainDomain],
                    attach: {server: server, webhosting_id: webhosting_id, docroot_domain: mainDomain}
                  }

        it("prepare", function(){
            m.setupMonsterInfoWebhosting(webhosting_id, user, true)
        })
            
        letsencrypt_add_and_activate_test(payload, expected_original_c_id, server, webhosting_id, mainDomain);


        var justPut;
        it("listing purchases by user, should be present", function(done){

                 mapi.get( "/purchases/by-user/"+user, function(err, result, httpResponse){
                    result.purchases.some(pc=>{
                      if(pc.pc_subject_common_name == mainDomain) {
                         justPut = pc;
                         return true;
                      }
                    });
                    assert.ok(justPut);
                    done();

                 })
        })


        // note: the certificate does not show up as the call was mocked; we put a certificate by hand instead

        it("removing the relevant webstore", function(done){

                 mapi.delete( "/cleanup/"+webhosting_id, {},
                 function(err, result, httpResponse){
                     assert.equal(result, "ok");
                     done();
                 })

        });

        it("listing purchases by user, should not be present", function(done){

                 mapi.get( "/purchases/by-user/"+user, function(err, result, httpResponse){
                    justPut = null;
                    result.purchases.some(pc=>{
                      if(pc.pc_subject_common_name == mainDomain) {
                         justPut = pc;
                         return true;
                      }
                    })
                    assert.notOk(justPut);
                    done();

                 })

        })


        it("the certificate should disappear as well", function(done){

                 mapi.get( "/certificates/by-webhosting/"+webhosting_id+"/", function(err, result, httpResponse){

                    assert.equal(result.certificates.length, 0);
                    done();

                 })

        })

    })

    function letsencrypt_add_and_activate_test(payload, expected_original_c_id, server, webhosting_id, mainDomain){

        it("calling the letsencrypt route", function(done){

            var stat = common.setupRelayerMapiPoolForCertInsert(assert, app, expected_original_c_id, server, webhosting_id, mainDomain)
            var commandClosed = false;
            var mainCallReturned = false;

            var oCommander = app.commander
            spawnCalls = 0
            app.commander = activationCommander(mainDomain, function(x){
                assert.isUndefined(x);
                commandClosed = true;
                doneLogic()
            })

                 mapi.put( "/purchases/by-user/"+user+"/letsencrypt", payload,
                 function(err, result, httpResponse){
                     console.log("re", result, err)
                     assert.propertyVal(result, "id", "emitter-id")

                     mainCallReturned= true;

                    doneLogic();
                 })

            function doneLogic(){
               if(!commandClosed) return;
               if(!mainCallReturned) return;

                assert.equal(stat.putCert, 1)
                assert.equal(stat.postDocroot, 1)
                assert.equal(spawnCalls, 1)
                app.commander = oCommander

               done()
            }

        })

    }

    function certificateForTheActivationShouldBeFound() {

            it("listing certificates of the user, one should be there for the activated purchase", function(done){

                     mapi.get( "/certificates/by-user/"+user, function(err, result, httpResponse){

                        assert.equal(result.certificates.length, 1)
                        done()

                     })

            })


            it("fetching the certificate by id", function(done){

                     mapi.get( "/certificates/by-user/"+user+"/item/"+c_id+"/full", function(err, result, httpResponse){

                        assert.propertyVal(result, "c_reseller", "letsencrypt")
                        assert.propertyVal(result, "c_vendor", "Let's Encrypt")
                        assert.propertyVal(result, "c_product", "Let's Encrypt")
                        done()

                     })

            })
    }


    function shouldBeEmpty(){
        it("listing purchases by user, they should be empty", function(done){

                 mapi.get( "/purchases/by-user/"+user, function(err, result, httpResponse){

                    assert.deepEqual(result, { purchases:[]})
                    done()

                 })

        })

    }

    function putPurchase(comment){
        it("put a new raw purchase", function(done){
            var params = {pc_reseller: "letsencrypt", pc_vendor:"Let's Encrypt", pc_product:"Let's Encrypt", pc_product_parameters:{years:2}}
            if(comment)
              params.pc_comment= "foobar"

                 mapi.put( "/purchases/by-user/"+user, params,
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.property(result, "pc_id")

                    pc_id = result.pc_id

                    done()

                 })

        })

    }



    function removeIt(){
        it("removing the purchase", function(done){

                 mapi.delete( "/purchases/by-user/"+user+"/item/"+pc_id, {},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.equal(result, "ok")

                    done()

                 })

        })

    }


}, 10000)


