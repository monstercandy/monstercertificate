require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../cert-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const user_id = 8123;
    const storage_id = "812132";

    const fs = require("MonsterDotq").fs()
    const path = require("path")

    var common = require("00-common.js")
    var m = common.mocking(assert, app, user_id)
    m.setupMonsterInfoAccount()

    const server = app.config.get("mc_server_name");


    var pc_id;
    var latestBackup;
    var getInfoCalls = 0;
            app.MonsterInfoWebhosting = {
               GetInfo: function(id){


                   getInfoCalls++

                   return Promise.resolve({
                     wh_id: id,
                     wh_user_id: user_id,
                     wh_storage: "/web/w3",
                     extras: {
                         web_path: "/web/w3/"+id+"-sdfsdfsdf"
                     },
                     template:{
                       t_ftp_max_number_of_accounts:1,
                       t_storage_max_quota_soft_mb:10,
                       t_x509_certificate_letsencrypt_allowed: 1,
                       t_x509_certificate_external_allowed: 1,
                     },
                     wh_tally_web_storage_mb: 5.0,
                     wh_tally_sum_storage_mb: 5.2
                   })
               }
            }



  const jsonInput = {
          privateKey:   "-----BEGIN RSA PRIVATE KEY-----\npkey\n-----END RSA PRIVATE KEY-----\n",
          certificate:  "-----BEGIN CERTIFICATE-----cert\n-----END CERTIFICATE-----\n",
          intermediate: "-----BEGIN CERTIFICATE-----\nintermediate1\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nintermediate2\n-----END CERTIFICATE-----\n"
       }

    const expectedBackup = { certs: [
     {
       c_id: "todo",
       c_issuer_organization: 'GeoTrust Inc.',
       c_issuer_common_name: 'RapidSSL SHA256 CA - G3',
       c_subject_common_name: 'www.monstermedia.hu',
       c_subject_organization_unit: 'GT46628488 / See www.rapidssl.com/resources/cps (c)15 / Domain Control Validated - RapidSSL(R)',
       c_begin: '2015-05-16T05:28:23.000Z',
       c_end: '2017-05-18T21:37:09.000Z',
       c_settled: 1,
       c_private_key: 1,
       c_certificate: 1,
       c_intermediate_chain: 1,
       c_certificate_signing_request: 0,
       c_alias: '',
       c_reseller: 'letsencrypt',
       c_vendor: 'Let\'s Encrypt',
       c_product: 'Let\'s Encrypt',
       c_reseller_certificate_id: '',
       c_comment: '',
       c_deleted: 0,
       data: {
          certificate: '-----BEGIN CERTIFICATE-----\ncert0\n-----END CERTIFICATE-----\n',
          privateKey: '-----BEGIN RSA PRIVATE KEY-----\nBlaBlaBla\n-----END RSA PRIVATE KEY-----\n',
          intermediates: [ '-----BEGIN CERTIFICATE-----\ncert1\n-----END CERTIFICATE-----\n' ]
       }
      }
    ],

   purchases:
   [ { purchase: {
       pc_status: 'issued',
       pc_local: 1,
       pc_reseller: 'letsencrypt',
       pc_vendor: 'Let\'s Encrypt',
       pc_product: 'Let\'s Encrypt',
       pc_reseller_certificate_id: null,
       pc_product_parameters: {},
       pc_subject_common_name: 'www.monstermedia.hu',
       pc_begin: '2015-05-16T05:28:23.000Z',
       pc_end: '2017-05-18T21:37:09.000Z',
       pc_comment: '',
       pc_misc: {},
       },
       activation: {
       a_status: 'successful',
       a_payload: '{"domains":["wie.hu","www.wie.hu"],"attach":{"server":"[[server]]","webhosting_id":"[[wh_id]]","docroot_domain":"wie.hu"},"local":true}',
       a_extra: '{"mainDomain":"wie.hu"}',
       a_result: '' }
       } ]

   };




	describe('preparation', function() {


        it("calling the letsencrypt route", function(done){

          mainDomain = "wie.hu";
          var payload = {
                    domains:[mainDomain,"www."+mainDomain],
                    attach: {server: server, webhosting_id: storage_id, docroot_domain: mainDomain}
                  }

            var stat = common.setupRelayerMapiPoolForCertInsert(assert, app, null, server, storage_id, mainDomain, function(url,data){
               // console.log("HERE!!!", data)
               return mapi.putAsync("/certificates/by-webhosting/"+storage_id, data)
                 .then(res=>{
                     expectedBackup.certs[0].c_id = res.result.c_id
                     return res;
                 })
            })
            var commandClosed = false;
            var mainCallReturned = false;

            var spawnCalls = 0
            var taskId = "taskid";

            var oGetMailer = app.GetMailer
            app.GetMailer = function() {
              return {
                SendMailAsync: function(d){
                   return Promise.resolve()
                }
              }
            }


            var oCommander = app.commander
            app.commander = {
                EventEmitter: function(){
                  return {
                    send_stdout_ln: function(msg){
                        console.log("msg via send_stoud_ln:", msg);
                    },
                    close: function(x){
                        assert.isUndefined(x);
                        commandClosed = true;
                        return doneLogic()
                    },
                    spawn: function(){
                       console.log("spawn was called");
                       return Promise.resolve({id:"emitter-id"});
                    }
                  };
                },
                spawn: function(task, params){
                    spawnCalls++

                    // console.log(spawnCalls, task, params);

                    if(spawnCalls == 1) {
                        var mainDir = app.config.get("common_params").domain_validation_letsencrypt_live_directory
                        return fs.mkdirAsync(path.join(mainDir, mainDomain)).catch(ex=>{})
                          .then(()=>{
                            var ps = []
                            Array({f:"cert",k:common.mainCert},{f:"privkey",k:common.mainPrivKey},{f:"chain",k:common.intermediateCert1}).forEach(a=>{
                                var p = path.join(mainDir,mainDomain,a.f+".pem");
                                console.log("writing file!", p)
                                ps.push(fs.writeFileAsync(p, a.k))
                            })
                            return Promise.all(ps)
                           })
                           .then(()=>{
                              return Promise.resolve({id:taskId+spawnCalls, executionPromise: Promise.resolve({output: "fo."})})

                           })

                    }


                    // console.log("spawn!", task, params);

                    return Promise.resolve({
                       executionPromise: Promise.resolve({output: task.chain.stdin})
                    })

                }
            }



                 mapi.put( "/purchases/by-user/"+user_id+"/letsencrypt", payload,
                 function(err, result, httpResponse){
                     console.log("re", result, err)
                     assert.propertyVal(result, "id", "emitter-id")

                     mainCallReturned= true;

                     doneLogic();
                 })

            function doneLogic(){
               if(!commandClosed) return;
               if(!mainCallReturned) return;

                assert.equal(stat.putCert, 1)
                assert.equal(stat.postDocroot, 1)
                assert.equal(spawnCalls, 4)
                app.commander = oCommander
                app.GetMailer = oGetMailer

               done()
            }

        })



        it('get list of purchases to learn the pc_id', function(done) {

             mapi.get( "/purchases/by-user/"+user_id,function(err, result, httpResponse){
                // console.log("here",err, result)
                pc_id = result.purchases[0].pc_id;
                assert.ok(pc_id)
                done()

             })

        })

	})


  describe("backup", function(){

        backupShouldWork();

  })



  describe("restore", function(){

        cleanup();

        it('get list of certificates should be empty', function(done) {

             mapi.get( "/certificates/by-webhosting/"+storage_id,function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, { canBeAdded: true, certificates: [] })
                done()

             })

        })

        restoreShouldWork();

        // even twice, without clenaup!
        restoreShouldWork();

        backupShouldWork();

  });

  describe("site-wide backup", function(){

       it('generating backup for the complete site', function(done) {

             mapi.get( "/wie/", function(err, result, httpResponse){

                // console.log("foo", result)
                assert.ok(result[storage_id]);

                done()

             })

        })


       cleanup();

  })

  function restoreShouldWork(){
        it('restoring a specific webstore', function(done) {

           var oCommander = app.commander
           app.commander =  {
                spawn: function(task, params) {
                  // console.log("!!!", task, params)

                  var re = ""
                  if((task.chain.stdin == jsonInput.privateKey)||(task.chain.stdin == expectedBackup.certs[0].data.privateKey))
                      re = common.mainPrivKey
                  else if((task.chain.stdin.indexOf("intermediate1") > -1)|| (task.chain.stdin.indexOf("\ncert1\n") > -1))
                      re = common.intermediateCert1
                  else if (task.chain.stdin.indexOf("intermediate2") > -1)
                      re = common.intermediateCert2
                  else if((task.chain.stdin == jsonInput.certificate)||(task.chain.stdin == "-----BEGIN CERTIFICATE-----\ncert0\n-----END CERTIFICATE-----\n"))
                      re = common.mainCert

                  // console.log("%%% gona return", re)
                  return Promise.resolve({
                     executionPromise: Promise.resolve({output: re})
                  })
                }
               }



             mapi.post( "/wie/"+storage_id, latestBackup, function(err, result, httpResponse){

                assert.equal(result, "ok");
                app.commander = oCommander;
                done()

             })

        })
  }


  function cleanup(){

       it("removing the certificate", function(done){


                 var certId = expectedBackup.certs[0].c_id;

                 var stats = setupRelayerForDocroot(certId)

                 mapi.delete( "/certificates/by-user/"+user_id+"/item/"+certId, {}, function(err, result, httpResponse){

                    assert.equal(result, "ok")
                    app.GetRelayerMapiPool = stats.oGetRelayerMapiPool
                    done()

                 })

        })

        it("removing the purchase", function(done){

                 mapi.delete( "/purchases/by-user/"+user_id+"/item/"+pc_id, {},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.equal(result, "ok")

                    done()

                 })

        })


  }

    function setupRelayerForDocroot(ac_id){
        var re = {disableWasCalled: 0, oGetRelayerMapiPool: app.GetRelayerMapiPool}
        disableWasCalled = 0
        app.GetRelayerMapiPool = function(){
          return {
             deleteAsync: function(url, data) {
               // console.log("delete", url, data)
               assert.equal(url, "/s/s111/docrootapi/certificates/"+(ac_id||c_id))
               re.disableWasCalled++
               return Promise.resolve({result:"ok"})
             },
          }
        }
        return re
    }


  function backupShouldWork(){
       it('generating backup for a specific webstore', function(done) {

             mapi.get( "/wie/"+storage_id, function(err, result, httpResponse){

                // console.log("wie!", result.purchases)

                latestBackup = simpleCloneObject(result);

                var actual = result;

                actual.certs.forEach(cert=>{
                   Array("created_at", "c_purchase").forEach(x=>{
                      assert.ok(cert[x]);
                      delete cert[x];
                   })

                })

                actual.purchases.forEach(cert=>{
                   Array("created_at", "pc_id").forEach(x=>{
                      assert.ok(cert.purchase[x]);
                      delete cert.purchase[x];
                   })

                   Array("a_id").forEach(x=>{
                      assert.ok(cert.activation[x]);
                      delete cert.activation[x];
                   })

                })

                assert.deepEqual(actual, expectedBackup);
                done()

             })

        })
  }


}, 10000)

