require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../cert-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const common = require("00-common.js")

    const user = 12346

    var m = common.mocking(assert, app, user)
    m.setupMonsterInfoAccount()

    var co_id

    var expectedContact = { co_id: 1,
       co_user_id: '12346',
       co_deleted: 0,
       co_EmailAddress: 'email@email.hu',
       co_JobTitle: 'Job Title',
       co_FirstName: 'First',
       co_LastName: 'Last',
       co_OrganizationName: 'Organization',
       co_Address1: 'Cím 1',
       co_Address2: 'Cím 2',
       co_City: 'City',
       co_StateProvince: 'State',
       co_PostalCode: '1234',
       co_Country: 'HU',
       co_Phone: '+36.302679426'
     }

    describe("managing contacts", function(){

        shouldBeEmpty()

        putContact()

        it("listing contacts by user, it should be there", function(done){

                 mapi.get( "/contacts/by-user/"+user, function(err, result, httpResponse){
                    delete result.contacts[0].created_at
                    assert.deepEqual(result, { contacts:[expectedContact]})
                    done()

                 })

        })

        it("fetching cntact by id", function(done){

                 mapi.get( "/contacts/by-user/"+user+"/item/"+co_id, function(err, result, httpResponse){
                    delete result.created_at

                    assert.deepEqual(result, expectedContact)
                    done()

                 })

        })

        removeIt()

        shouldBeEmpty()


    })

    describe("default contacts", function(){

        shouldBeEmpty()

        putContact()


        it("deleteing default contact", function(done){

                 mapi.delete( "/config/contacts/default", {}, function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result, "ok")
                    done()

                 })

        })

        it("fetching default contact should be empty", function(done){

                 mapi.get( "/config/contacts/default", function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.deepEqual(result, {})
                    done()

                 })

        })


        it("saving new default", function(done){

                 mapi.post( "/config/contacts/default", {co_id:co_id}, function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result, "ok")
                    done()

                 })

        })


        it("fetching default contact should return it", function(done){

                 mapi.get( "/config/contacts/default", function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.deepEqual(result, {co_id: co_id})
                    done()

                 })

        })

        removeIt()

        shouldBeEmpty()

    })

    function shouldBeEmpty(){
        it("listing contacts by user, they should be empty", function(done){

                 mapi.get( "/contacts/by-user/"+user, function(err, result, httpResponse){

                    assert.deepEqual(result, { contacts:[]})
                    done()

                 })

        })

    }

    function putContact(){
        common.putContact(mapi, user, function(result){
                    co_id = result.co_id
        })

    }



    function removeIt(){
        it("removing the contact", function(done){

                 mapi.delete( "/contacts/by-user/"+user+"/item/"+co_id, {},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.equal(result, "ok")

                    done()

                 })

        })

    }


}, 10000)


