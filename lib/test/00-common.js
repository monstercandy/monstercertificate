var lib = module.exports = {}

lib.putCertJsonInput = {
            privateKey:   "-----BEGIN RSA PRIVATE KEY-----\npkey\n-----END RSA PRIVATE KEY-----\n",
            certificate:  "-----BEGIN CERTIFICATE-----cert\n-----END CERTIFICATE-----\n",
            intermediate: "-----BEGIN CERTIFICATE-----\nintermediate1\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nintermediate2\n-----END CERTIFICATE-----\n"
    };

lib.typicalContact = {
                co_EmailAddress: "email@email.hu",
                co_FirstName: "First",
                co_LastName: "Last",
                co_OrganizationName: "Organization",
                co_JobTitle: "Job Title",
                co_Address1: "Cím 1",
                co_Address2: "Cím 2",
                co_City: "City",
                co_StateProvince: "State",
                co_PostalCode: "1234",
                co_Country: "HU",
                co_Phone:   "+36.302679426"
            }

lib.namecheapParseCsrResponse = `
    <ApiResponse Status="OK">
      <Errors />
      <Warnings />
      <RequestedCommand>namecheap.ssl.parseCSR</RequestedCommand>
      <CommandResponse Type="namecheap.ssl.parseCSR">
        <SSLParseCSRResult>
          <CSRDetails>
            <CommonName>domain.com</CommonName>
            <DomainName>domain.com</DomainName>
            <Country>US</Country>
            <OrganisationUnit>com</OrganisationUnit>
            <Organisation>Org</Organisation>
            <ValidTrueDomain>false</ValidTrueDomain>
            <State>California</State>
            <Locality>la</Locality>
            <Email>someemail@domain.com</Email>
          </CSRDetails>
          <HttpDCValidation ValueAvailable="true">
            <FileName><![CDATA[776DE8TT1FA13356DAB9JJ0F1ACDXSD2.txt]]></FileName>
            <FileContent><![CDATA[UUU2310A0C5S55223FC560CT432259CB4177C79D
    comodoca.com]]></FileContent>
          </HttpDCValidation>
        </SSLParseCSRResult>
      </CommandResponse>
      <Server>WEB1-SANDBOX1</Server>
      <GMTTimeDifference>--5:00</GMTTimeDifference>
      <ExecutionTime>0.711</ExecutionTime>
    </ApiResponse>
`

lib.namecheapSslRenewResponse2 = `
<?xml version="1.0" encoding="UTF-8"?>
<ApiResponse Status="OK" xmlns="http://api.namecheap.com/xml.response">
 <Errors/>
 <Warnings/>
 <RequestedCommand>namecheap.ssl.renew</RequestedCommand>
 <CommandResponse Type="namecheap.ssl.renew">
  <SSLRenewResult CertificateID="2807581" SSLType="PositiveSSL Wildcard" Years="1" OrderId="29167117" TransactionId="34395895" ChargedAmount="67.6000"/>
 </CommandResponse>
 <Server>007c35ee65da</Server>
 <GMTTimeDifference>--5:00</GMTTimeDifference>
 <ExecutionTime>0.240</ExecutionTime>
</ApiResponse>
`

lib.namecheapSslRenewResponse1 = `
    <?xml version="1.0" encoding="UTF-8"?>
    <ApiResponse Status="OK">
      <Errors />
      <Warnings />
      <RequestedCommand>namecheap.ssl.renew</RequestedCommand>
      <CommandResponse>
        <SSLRenewResult CertificateID="501904" Years="1" OrderId="2345545" TransactionId="32487444" ChargedAmount="9.98" />
      </CommandResponse>
      <Server>IMWS-A03</Server>
      <GMTTimeDifference>+5:30</GMTTimeDifference>
      <ExecutionTime>0.219</ExecutionTime>
    </ApiResponse>
`

lib.namecheapSslRevokeCertificate = `
    <?xml version="1.0" encoding="UTF-8"?>
    <ApiResponse Status="OK">
    <Errors />
    <Warnings />
    <RequestedCommand>namecheap.ssl.revokecertificate</RequestedCommand>
    <CommandResponse Type="namecheap.ssl.revokecertificate">
    <RevokeCertificateResult IsSuccess="true" ID="8111110" >
    </CommandResponse>
    <Server>API02</Server>
    <GMTTimeDifference>--5:00</GMTTimeDifference>
    <ExecutionTime>0.874</ExecutionTime>
    </ApiResponse>
`

lib.namecheapSslGetInfoStillPendingRespone = `
<?xml version="1.0" encoding="UTF-8"?>
<ApiResponse Status="OK" xmlns="http://api.namecheap.com/xml.response">
 <Errors/>
 <Warnings/>
 <RequestedCommand>namecheap.ssl.getInfo</RequestedCommand>
 <CommandResponse Type="namecheap.ssl.getInfo">
  <SSLGetInfoResult Status="purchased" StatusDescription="Being Processed." Type="positivessl" IssuedOn="" Years="3" Expires="" ActivationExpireDate="" OrderId="28330373" SANSCount="0">
   <CertificateDetails>
    <CSR><![CDATA[-----BEGIN CERTIFICATE REQUEST-----
MIIDOzCCAiMCAQAwga0xCzAJBgNVBAYTAkhVMRAwDgYDVQQIDAdIdW5nYXJ5MREw
DwYDVQQHDAhCdWRhcGVzdDERMA8GA1UECgwIQnVkYXBlc3QxGzAZBgNVBAsMEmh1
bmdhcm9yaW5nc2hvcC5odTEsMCoGCSqGSIb3DQEJARYdaG9zdG1hc3RlckBodW5n
YXJvcmluZ3Nob3AuaHUxGzAZBgNVBAMMEmh1bmdhcm9yaW5nc2hvcC5odTCCASIw
DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALpemyFma3tIefVIAi2a16BEqYdp
rlcT0RnL9H9cZQmrSysYQGSJGbFdFEZzsmwS1kIfLaLG0pInumZxNJgfg34QyjS5
2dJ3F5q7w577wOy5dnhCUZE1WkWtGWr6oM73EL6RnWzC+cvkDpiW7HvfXXCRI/yN
6rIHojwS5tl29Ze7GtSoxhRkT20NxE6pTHObE9Lw1Hxrnw298lrpOhpTgt8Sa64a
8Dr1J96Ujnl/dVLW3A2u/frtYgl2IKkeHmtLDzft8qfpmo565aTrJNYeaHivWqWF
3umFOt/VbgtIPlmMrX2H/rmKd3mmo7VBe8jOquHXGZYtFdqupP3fD0zUz78CAwEA
AaBIMEYGCSqGSIb3DQEJDjE5MDcwNQYDVR0RBC4wLIISaHVuZ2Fyb3JpbmdzaG9w
Lmh1ghZ3d3cuaHVuZ2Fyb3JpbmdzaG9wLmh1MA0GCSqGSIb3DQEBCwUAA4IBAQBU
pWj4mbOhZlEa0fVjDB4L4QM2jAV/QNWMlPy8e0e0ciAs57JU2eSbLrghOSaxqyCT
oxb7Jqu3qMqS3KMDo/TM4vw6rSs4+LdGrBUbfNt6IwDEbE5xWcND/QwnqRWnODgV
48+lFmL//ZmFrXtRz1Uhi1Uyd4C30K1BYhwvqbgi+pR+XhKRWXaRmh9nHBRXW8KK
o5vXO3hJWDCVFxUh2oXpx0GoOkE5vw/xBpjSCsmZyoQtTF/pWGt9LFnxsxPnsR4U
BIkSwzS4HPt+A3ppEdnLjZ4GmublgIvebI4On8wSuh0h4BrIHiipl7rGqhDcgKlU
++CnHg9ZY4+l7XCExu+7
-----END CERTIFICATE REQUEST-----]]></CSR>
    <ApproverEmail>hostmaster@hungaroringshop.hu</ApproverEmail>
    <CommonName>hungaroringshop.hu</CommonName>
    <AdministratorEmail>kalmanczi.sandor@virt24h.com</AdministratorEmail>
   </CertificateDetails>
   <Provider>
    <OrderID>84178394</OrderID>
    <Name>COMODO</Name>
   </Provider>
  </SSLGetInfoResult>
 </CommandResponse>
 <Server>b3e4231f6520</Server>
 <GMTTimeDifference>--5:00</GMTTimeDifference>
 <ExecutionTime>0.066</ExecutionTime>
</ApiResponse>
`

lib.namecheapSslResendApproverEmail = `
    <ApiResponse Status="OK">
    <Errors/>
    <RequestedCommand>namecheap.ssl.resendApproverEmail</RequestedCommand>
      <CommandResponse Type="namecheap.ssl.resendApproverEmail">
        <SSLResendApproverEmailResult ID="500532" IsSuccess="true"/>
      </CommandResponse>
    <Server>SERVER-NAME</Server>
    <GMTTimeDifference>+5:30</GMTTimeDifference>
    <ExecutionTime>3.826</ExecutionTime>
    </ApiResponse>
`

lib.namecheapSslGetApproverEmailList = `
    <?xml version="1.0" encoding="UTF-8"?>
    <ApiResponse Status="OK">
      <Errors />
      <RequestedCommand>namecheap.ssl.getApproverEmailList</RequestedCommand>
      <CommandResponse Type="namecheap.ssl.getApproverEmailList">
        <GetApproverEmailListResult Domain="domain.com">
          <Domainemails>
            <email>3db85a8e21b54bab848eb8f01d5d78c5.protect@whoisguard.com</email>
          </Domainemails>
          <Genericemails>
            <email>postmaster@domain.com</email>
            <email>sslwebmaster@domain.com</email>
            <email>ssladministrator@domain.com</email>
            <email>mis@domain.com</email>
          </Genericemails>
        </GetApproverEmailListResult>
      </CommandResponse>
      <Server>SERVER-NAME</Server>
      <GMTTimeDifference>--6:00</GMTTimeDifference>
      <ExecutionTime>3.615</ExecutionTime>
    </ApiResponse>
`

lib.namecheapSslGetApproverEmailList2 = `
    <?xml version="1.0" encoding="UTF-8"?>
    <ApiResponse Status="OK">
      <Errors />
      <RequestedCommand>namecheap.ssl.getApproverEmailList</RequestedCommand>
      <CommandResponse Type="namecheap.ssl.getApproverEmailList">
        <GetApproverEmailListResult Domain="domain.com">
          <Domainemails>
            <email>none</email>
          </Domainemails>
          <Genericemails>
            <email>foobar@domain.com</email>
          </Genericemails>
        </GetApproverEmailListResult>
      </CommandResponse>
      <Server>SERVER-NAME</Server>
      <GMTTimeDifference>--6:00</GMTTimeDifference>
      <ExecutionTime>3.615</ExecutionTime>
    </ApiResponse>
`

lib.namecheapSslGetInfoSuccessRespone = `
    <ApiResponse Status="OK">
    <Errors />
    <Warnings />
    <RequestedCommand>namecheap.ssl.getInfo</RequestedCommand>
    <CommandResponse Type="namecheap.ssl.getInfo">
     <SSLGetInfoResult Status="active" StatusDescription="Certificate is Active." Type="positivessl" IssuedOn="1/20/2014" Expires="1/20/2015" ActivationExpireDate="" OrderId="10439081" ReplacedBy="747253" SANSCount="0">
     <CertificateDetails>
     <CSR>
     <![CDATA[
    -----BEGIN CERTIFICATE REQUEST-----
    MIIC3jCCAcYCAQAwgZgxGTAXBgNVBAMMEGxhbWFyaW9vbmVhbC5jb20xFTATBgNV
    BAoMDGxhbWFyaW9vbmVhbDERMA8GA1UECwwIc2VjdXJpdHkxCzAJBgNVBAcMAkto
    5eu2KxLOJtaRL++ur5foTTe9
    -----END CERTIFICATE REQUEST-----
    ]]>
     </CSR>
    <ApproverEmail>example@domain.com</ApproverEmail>
    <CommonName>domain.com</CommonName>
    <AdministratorName>John Doe</AdministratorName>
    <AdministratorEmail>example@domain.com</AdministratorEmail>
     <Certificates CertificateReturned="true" ReturnType="Individual">
     <Certificate>
     <![CDATA[
    -----BEGIN CERTIFICATE-----
    MIIFBDCCA+ygAwIBAgIQJXnrY7043QfanPVrLcKPczANBgkqhkiG9w0BAQUFADBz
    MQswCQYDVQQGEwJHQjEbMBkGA1UECBMSR3JlYXRlciBNYW5jaGVzdGVyMRAwDgYD
    LDGeQmFIuHBMs878DkdOKZUsR4Cs7AzcYOxRWZUuAHpDrHQQiR5QHTg6Mc2ZtFvr
    QwQg7hdY7gQDuoC94Ndm/2LrNbY9ZFz4dCV2+E8BYBsL0GlPMlSKxw==
    -----END CERTIFICATE-----
    ]]>
     </Certificate>
     <CaCertificates>
    <Certificate Type="INTERMEDIATE">
     <Certificate>
     <![CDATA[
    -----BEGIN CERTIFICATE-----
    MIIENjCCAx6gAwIBAgIBATANBgkqhkiG9w0BAQUFADBvMQswCQYDVQQGEwJTRTEU
    MBIGA1UEChMLQWRkVHJ1c3QgQUIxJjAkBgNVBAsTHUFkZFRydXN0IEV4dGVybmFs
    c4g/VhsxOBi0cQ+azcgOno4uG+GMmIPLHzHxREzGBHNJdmAPx/i9F4BrLunMTA5a
    mnkPIAou1Z5jJh5VkpTYghdae9C8x49OhgQ=
    -----END CERTIFICATE-----
    ]]>
     </Certificate>
    </Certificate>
    <Certificate Type="INTERMEDIATE">
     <Certificate>
     <![CDATA[
    -----BEGIN CERTIFICATE-----
    MIIE5TCCA82gAwIBAgIQB28SRoFFnCjVSNaXxA4AGzANBgkqhkiG9w0BAQUFADBv
    uuGtm87fM04wO+mPZn+C+mv626PAcwDj1hKvTfIPWhRRH224hoFiB85ccsJP81cq
    cdnUl4XmGFO3
    -----END CERTIFICATE-----
    ]]>
     </Certificate>
    </Certificate>
     </CaCertificates>
     </Certificates>
     </CertificateDetails>
     <Provider>
    <OrderID>111111</OrderID>
    <Name>COMODO</Name>
     </Provider>
     </SSLGetInfoResult>
    </CommandResponse>
    <Server>API01</Server>
    <GMTTimeDifference>--5:00</GMTTimeDifference>
    <ExecutionTime>0.542</ExecutionTime>
     </ApiResponse>
`

lib.namecheapSslActivateResponseFailed = `
<?xml version="1.0" encoding="UTF-8"?>
<ApiResponse Status="ERROR" xmlns="http://api.namecheap.com/xml.response">
 <Errors>
  <Error Number="3011296">Invalid CSR. -7:  &quot;UK&quot; is not a valid ISO-3166 country code! Do you mean GB?</Error>
 </Errors>
 <Warnings/>
 <RequestedCommand>namecheap.ssl.activate</RequestedCommand>
 <CommandResponse Type="namecheap.ssl.activate">
  <SSLActivateResult ID="0" IsSuccess="false"/>
 </CommandResponse>
 <Server>7a84e6ae0032</Server>
 <GMTTimeDifference>--5:00</GMTTimeDifference>
 <ExecutionTime>0.729</ExecutionTime>
</ApiResponse>
`

lib.namecheapSslActivateResponse = `
    <?xml version="1.0" encoding="UTF-8"?>
    <ApiResponse Status="OK">
      <Errors />
      <RequestedCommand>namecheap.ssl.activate</RequestedCommand>
      <CommandResponse Type="namecheap.ssl.activate">
        <SSLActivateResult ID="123455" IsSuccess="true" >
   <HttpDCValidation ValueAvailable="true">
    <DNS domain="loremipsum.pw">
     <FileName><![CDATA[8D74EE158718C552568B8B7D79F6FD9E.txt]]></FileName>
     <FileContent><![CDATA[C9C863405FE7675A3988B97664EA6BAF442019E4E52FA335F406F7C5F26CF14F  comodoca.com 10AF9DB9TU231]]></FileContent>
    </DNS>
   </HttpDCValidation>
   <DNSDCValidation ValueAvailable="true">
    <DNS domain="loremipsum.pw">
     <HostName><![CDATA[_8D74EE158718C552568B8B7D79F6FD9E.loremipsum.pw]]></HostName>
     <Target><![CDATA[C9C863405FE7675A3988B97664EA6BAF.442019E4E52FA335F406F7C5F26CF14F.10AF9DB9TU231.comodoca.com]]></Target>
    </DNS>
   </DNSDCValidation>
        </SSLActivateResult>
      </CommandResponse>
      <Server>SERVER-NAME</Server>
      <GMTTimeDifference>+5:30</GMTTimeDifference>
      <ExecutionTime>55.5</ExecutionTime>
    </ApiResponse>
`

lib.namecheapGetBalanceResponse = `
<?xml version="1.0" encoding="utf-8"?>
<ApiResponse Status="OK" xmlns="http://api.namecheap.com/xml.response">
  <Errors />
  <Warnings />
  <RequestedCommand>namecheap.users.getbalances</RequestedCommand>
  <CommandResponse Type="namecheap.users.getBalances">
    <UserGetBalancesResult Currency="USD" AvailableBalance="29.86" AccountBalance="29.86" EarnedAmount="0.00" WithdrawableAmount="0.00" FundsRequiredForAutoRenew="0.00" />
  </CommandResponse>
  <Server>PHX01APIEXT03</Server>
  <GMTTimeDifference>--4:00</GMTTimeDifference>
  <ExecutionTime>0.058</ExecutionTime>
</ApiResponse>
`

lib.namecheapSslCreateResponse = `
    <?xml version="1.0" encoding="UTF-8"?>
    <ApiResponse Status="OK">
      <Errors />
      <RequestedCommand>namecheap.ssl.create</RequestedCommand>
      <CommandResponse Type="namecheap.ssl.create">
        <SSLCreateResult IsSuccess="true" OrderId="3186" TransactionId="4211" ChargedAmount="30.2000">
          <SSLCertificate CertificateID="500393" Created="06/26/2010" Expires="" SSLType="PositiveSSL" Years="2" Status="NewPurchase" />
        </SSLCreateResult>
      </CommandResponse>
      <Server>IMWS-A03</Server>
      <GMTTimeDifference>+5:30</GMTTimeDifference>
      <ExecutionTime>0.219</ExecutionTime>
    </ApiResponse>
`

lib.putContact = function(mapi, user, callback){
        it("put a new contact", function(done){
            var params = lib.typicalContact

                 mapi.put( "/contacts/by-user/"+user, params,
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    if(!result.co_id)
                        throw new Error("no co_id returned")
                    if(callback)
                        callback(result)

                    done()

                 })

        })

}

lib.mocking = function(assert, app, user_id){

    var re = {};
    re.setupMonsterInfoAccount = function(){
         user_id = ""+user_id

            app.MonsterInfoAccount = {
                GetInfo: function(account_id){
                    assert.equal(account_id, user_id)
                    return Promise.resolve({"u_id":user_id})
                }
            }

      }
    re.setupMonsterInfoWebhosting = function(wh_id, user_id, external_allowed){

            app.MonsterInfoWebhosting = {
                GetInfo: function(d){
                    assert.equal(""+d, wh_id || "12345")

                    return Promise.resolve(
                    {
                       wh_id: wh_id,
                       wh_user_id: user_id || 12345,
                       wh_is_expired: false,
                       wh_server: "dbstest_withmysqlrole",
                       wh_max_execution_second: 30,
                       wh_php_fpm_conf_extra_lines: "line1\nline2\n",
                       wh_name: "some name",
                       wh_template: "WEB10000",
                       wh_template_override: "{\"t_storage_max_quota_hard_mb\":20000}",
                       wh_php_version: "5.6",
                       template: {
                        t_db_max_number_of_users:2,
                        t_db_max_number_of_databases:2,
                        t_x509_certificate_letsencrypt_allowed: 1,
                        t_x509_certificate_external_allowed: external_allowed ? 1 : 0,
                       },
                       extras: {
                         web_path: "/web/w3/"+d+"-12345678789"
                       }
                    })
                }
            }

      }

   return re

}

lib.intermediateCert1 =             `
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 290124 (0x46d4c)
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C=US, O=GeoTrust Inc., CN=RapidSSL SHA256 CA - G3
        Validity
            Not Before: May 1 05:28:23 2015 GMT
            Not After : May 1 21:37:09 2017 GMT
        Subject: OU=GT46628488, OU=See www.rapidssl.com/resources/cps (c)15, OU=Domain Control Validated - RapidSSL(R), CN=www.monstermedia.hu
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:b3:42:35:26:05:e3:f3:98:b7:a6:95:9a:5d:8a:
                    f0:5c:7f:b1:0e:78:25:c1:83:ec:9f:3a:e8:39:22:
                    df:43:5e:bd:46:6d:72:89:9c:97:d5:be:2f:e4:14:
                    54:6d:19:34:79:19:9b:41:66:b0:0b:ab:8d:73:46:
                    ec:ca:1e:87:26:66:cb:ca:ed:af:bd:bf:fb:3c:ad:
                    c2:aa:df:d1:7e:39:d8:2b:dd:90:15:45:a8:b5:65:
                    c1:ed:b3:75:50:3e:46:1f:d4:e4:81:1e:5f:9d:0d:
                    1f:b8:39:2c:92:91:f4:8c:60:c9:f6:f5:64:c6:84:
                    f0:f4:5b:d7:a6:6b:d1:f7:01:1d:16:9b:10:16:73:
                    49:73:52:e8:99:e5:7c:da:d5:63:2e:92:60:dd:a7:
                    1a:69:65:85:42:c8:15:c9:55:29:6f:9d:ae:4f:09:
                    2f:2f:a4:cb:fb:5c:9f:04:d8:23:b9:19:cf:55:33:
                    b6:29:1b:42:57:91:7a:b6:34:72:9f:c9:71:17:78:
                    cc:f4:3c:6d:a1:d8:13:67:95:6d:da:85:9d:5f:2a:
                    27:82:57:cd:c0:bb:29:b5:38:17:d5:64:85:be:fa:
                    00:32:a7:f1:11:2a:b6:59:77:71:5a:20:ce:dc:6b:
                    23:8d:05:c0:d1:57:61:b9:aa:0d:ec:6f:4c:73:84:
                    15:29
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Authority Key Identifier:
                keyid:C3:9C:F3:FC:D3:46:08:34:BB:CE:46:7F:A0:7C:5B:F3:E2:08:CB:59

            Authority Information Access:
                OCSP - URI:http://gv.symcd.com
                CA Issuers - URI:http://gv.symcb.com/gv.crt

            X509v3 Key Usage: critical
                Digital Signature, Key Encipherment
            X509v3 Extended Key Usage:
                TLS Web Server Authentication, TLS Web Client Authentication
            X509v3 Subject Alternative Name:
                DNS:www.monstermedia.hu, DNS:monstermedia.hu
            X509v3 CRL Distribution Points:

                Full Name:
                  URI:http://gv.symcb.com/gv.crl

            X509v3 Basic Constraints: critical
                CA:FALSE
            X509v3 Certificate Policies:
                Policy: 2.23.140.1.2.1
                  CPS: https://www.rapidssl.com/legal

    Signature Algorithm: sha256WithRSAEncryption
         0c:ff:22:d5:0d:66:c3:e7:2f:db:cf:8e:3c:cc:03:e5:8d:b4:
         6a:cb:66:f1:cd:9e:a4:b0:21:48:d4:9b:f3:35:70:b1:9b:a1:
         44:1d:98:cd:fb:14:3d:b4:6f:1a:10:0d:37:8b:3a:d8:c2:ae:
         6f:9d:ab:09:a8:e7:ed:0a:0c:e5:24:f4:02:7d:28:b2:54:8d:
         4b:e8:e7:1f:e9:e2:75:00:d5:16:f3:0e:28:50:cb:8d:36:86:
         8d:d3:42:1c:d0:4a:44:a4:3b:50:cc:d9:cc:0d:60:64:2f:86:
         44:2d:b6:87:bf:8e:48:78:90:90:17:5d:44:0f:aa:5a:59:ca:
         f5:76:0f:db:63:07:4e:48:ab:eb:9d:d7:e7:9d:97:7d:bc:04:
         85:b4:4e:d2:e0:1f:bd:b3:12:5a:7b:be:be:31:e6:44:ec:b0:
         ab:1b:41:f6:5f:3b:c0:87:9b:2b:16:01:d6:23:02:e6:08:18:
         a1:63:84:42:bb:84:c8:47:6e:36:ab:a7:d4:42:03:cd:82:5f:
         de:62:3e:c0:49:33:3d:07:f5:bb:f4:8c:55:23:4e:1b:26:99:
         f8:2e:75:c9:c7:58:ea:48:be:ed:dd:ed:fd:91:21:c1:9e:c3:
         bd:21:b0:fb:12:55:5a:ea:4e:e7:ae:a9:f9:bd:e3:6a:a2:92:
         f2:4b:22:0a
-----BEGIN CERTIFICATE-----
cert1
-----END CERTIFICATE-----
`

lib.intermediateCert2 = `
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 290124 (0x46d4c)
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C=US, O=GeoTrust Inc., CN=RapidSSL SHA256 CA - G3
        Validity
            Not Before: May 2 05:28:23 2015 GMT
            Not After : May 2 21:37:09 2017 GMT
        Subject: OU=GT46628488, OU=See www.rapidssl.com/resources/cps (c)15, OU=Domain Control Validated - RapidSSL(R), CN=www.monstermedia.hu
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:b3:42:35:26:05:e3:f3:98:b7:a6:95:9a:5d:8a:
                    f0:5c:7f:b1:0e:78:25:c1:83:ec:9f:3a:e8:39:22:
                    df:43:5e:bd:46:6d:72:89:9c:97:d5:be:2f:e4:14:
                    54:6d:19:34:79:19:9b:41:66:b0:0b:ab:8d:73:46:
                    ec:ca:1e:87:26:66:cb:ca:ed:af:bd:bf:fb:3c:ad:
                    c2:aa:df:d1:7e:39:d8:2b:dd:90:15:45:a8:b5:65:
                    c1:ed:b3:75:50:3e:46:1f:d4:e4:81:1e:5f:9d:0d:
                    1f:b8:39:2c:92:91:f4:8c:60:c9:f6:f5:64:c6:84:
                    f0:f4:5b:d7:a6:6b:d1:f7:01:1d:16:9b:10:16:73:
                    49:73:52:e8:99:e5:7c:da:d5:63:2e:92:60:dd:a7:
                    1a:69:65:85:42:c8:15:c9:55:29:6f:9d:ae:4f:09:
                    2f:2f:a4:cb:fb:5c:9f:04:d8:23:b9:19:cf:55:33:
                    b6:29:1b:42:57:91:7a:b6:34:72:9f:c9:71:17:78:
                    cc:f4:3c:6d:a1:d8:13:67:95:6d:da:85:9d:5f:2a:
                    27:82:57:cd:c0:bb:29:b5:38:17:d5:64:85:be:fa:
                    00:32:a7:f1:11:2a:b6:59:77:71:5a:20:ce:dc:6b:
                    23:8d:05:c0:d1:57:61:b9:aa:0d:ec:6f:4c:73:84:
                    15:29
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Authority Key Identifier:
                keyid:C3:9C:F3:FC:D3:46:08:34:BB:CE:46:7F:A0:7C:5B:F3:E2:08:CB:59

            Authority Information Access:
                OCSP - URI:http://gv.symcd.com
                CA Issuers - URI:http://gv.symcb.com/gv.crt

            X509v3 Key Usage: critical
                Digital Signature, Key Encipherment
            X509v3 Extended Key Usage:
                TLS Web Server Authentication, TLS Web Client Authentication
            X509v3 Subject Alternative Name:
                DNS:www.monstermedia.hu, DNS:monstermedia.hu
            X509v3 CRL Distribution Points:

                Full Name:
                  URI:http://gv.symcb.com/gv.crl

            X509v3 Basic Constraints: critical
                CA:FALSE
            X509v3 Certificate Policies:
                Policy: 2.23.140.1.2.1
                  CPS: https://www.rapidssl.com/legal

    Signature Algorithm: sha256WithRSAEncryption
         0c:ff:22:d5:0d:66:c3:e7:2f:db:cf:8e:3c:cc:03:e5:8d:b4:
         6a:cb:66:f1:cd:9e:a4:b0:21:48:d4:9b:f3:35:70:b1:9b:a1:
         44:1d:98:cd:fb:14:3d:b4:6f:1a:10:0d:37:8b:3a:d8:c2:ae:
         6f:9d:ab:09:a8:e7:ed:0a:0c:e5:24:f4:02:7d:28:b2:54:8d:
         4b:e8:e7:1f:e9:e2:75:00:d5:16:f3:0e:28:50:cb:8d:36:86:
         8d:d3:42:1c:d0:4a:44:a4:3b:50:cc:d9:cc:0d:60:64:2f:86:
         44:2d:b6:87:bf:8e:48:78:90:90:17:5d:44:0f:aa:5a:59:ca:
         f5:76:0f:db:63:07:4e:48:ab:eb:9d:d7:e7:9d:97:7d:bc:04:
         85:b4:4e:d2:e0:1f:bd:b3:12:5a:7b:be:be:31:e6:44:ec:b0:
         ab:1b:41:f6:5f:3b:c0:87:9b:2b:16:01:d6:23:02:e6:08:18:
         a1:63:84:42:bb:84:c8:47:6e:36:ab:a7:d4:42:03:cd:82:5f:
         de:62:3e:c0:49:33:3d:07:f5:bb:f4:8c:55:23:4e:1b:26:99:
         f8:2e:75:c9:c7:58:ea:48:be:ed:dd:ed:fd:91:21:c1:9e:c3:
         bd:21:b0:fb:12:55:5a:ea:4e:e7:ae:a9:f9:bd:e3:6a:a2:92:
         f2:4b:22:0a
-----BEGIN CERTIFICATE-----
cert2
-----END CERTIFICATE-----
`


lib.mainCsr = `
Certificate Request:
    Data:
        Version: 0 (0x0)
        Subject: C=HU, ST=Hungary, L=Budapest, O=Noname Domain Kft., OU=Monster Media, CN=www.monstermedia.hu
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:b3:42:35:26:05:e3:f3:98:b7:a6:95:9a:5d:8a:
                    f0:5c:7f:b1:0e:78:25:c1:83:ec:9f:3a:e8:39:22:
                    df:43:5e:bd:46:6d:72:89:9c:97:d5:be:2f:e4:14:
                    54:6d:19:34:79:19:9b:41:66:b0:0b:ab:8d:73:46:
                    ec:ca:1e:87:26:66:cb:ca:ed:af:bd:bf:fb:3c:ad:
                    c2:aa:df:d1:7e:39:d8:2b:dd:90:15:45:a8:b5:65:
                    c1:ed:b3:75:50:3e:46:1f:d4:e4:81:1e:5f:9d:0d:
                    1f:b8:39:2c:92:91:f4:8c:60:c9:f6:f5:64:c6:84:
                    f0:f4:5b:d7:a6:6b:d1:f7:01:1d:16:9b:10:16:73:
                    49:73:52:e8:99:e5:7c:da:d5:63:2e:92:60:dd:a7:
                    1a:69:65:85:42:c8:15:c9:55:29:6f:9d:ae:4f:09:
                    2f:2f:a4:cb:fb:5c:9f:04:d8:23:b9:19:cf:55:33:
                    b6:29:1b:42:57:91:7a:b6:34:72:9f:c9:71:17:78:
                    cc:f4:3c:6d:a1:d8:13:67:95:6d:da:85:9d:5f:2a:
                    27:82:57:cd:c0:bb:29:b5:38:17:d5:64:85:be:fa:
                    00:32:a7:f1:11:2a:b6:59:77:71:5a:20:ce:dc:6b:
                    23:8d:05:c0:d1:57:61:b9:aa:0d:ec:6f:4c:73:84:
                    15:29
                Exponent: 65537 (0x10001)
        Attributes:
            a0:00
    Signature Algorithm: sha1WithRSAEncryption
         7f:b6:2c:ba:53:85:1a:43:51:cb:58:f9:37:67:9f:e1:7b:97:
         e3:c3:8f:fe:b0:01:4e:ae:af:06:32:52:b8:67:a4:ce:4a:a6:
         85:3f:60:92:78:2f:15:f4:13:b4:27:bb:4b:1e:67:98:83:30:
         ce:d0:18:b4:94:fc:9b:6b:1a:db:75:b5:0e:85:0e:e7:9e:18:
         25:d6:1a:0f:29:14:fb:dc:c6:37:73:db:18:cf:88:a9:23:ca:
         c9:73:f6:f4:c4:34:c3:52:a9:55:50:7e:d9:0f:aa:ef:e5:0f:
         40:3c:67:7b:14:1b:55:3f:17:c7:bb:71:9a:f2:1c:bb:8f:88:
         60:18:9a:25:95:4f:dd:44:e0:14:77:65:8b:30:e4:40:f7:b9:
         85:d7:c3:db:2e:b2:10:1a:f5:1d:e4:23:30:5e:41:a9:31:25:
         21:fc:76:ee:ad:26:d4:a3:f7:d4:d8:8a:41:0d:ca:8b:28:a1:
         55:6e:5c:39:58:c3:05:96:b0:9f:67:df:a3:d7:0c:01:bd:ae:
         4c:b2:19:92:7b:b5:a8:08:21:30:49:a9:f4:9f:9a:21:65:31:
         44:6a:a7:c1:17:c5:48:f9:55:30:1e:da:ee:5d:8f:8e:a5:79:
         cf:77:d9:34:53:59:ba:e4:a0:e7:2d:34:e2:db:9e:49:f5:20:
         99:67:1a:4c
-----BEGIN CERTIFICATE REQUEST-----
foobar
-----END CERTIFICATE REQUEST-----
`

lib.csrWithInvalidCountryCode = `
Certificate Request:
    Data:
        Version: 0 (0x0)
        Subject: C=XX, ST=Hungary, L=Budapest, O=Noname Domain Kft., OU=Monster Media, CN=www.monstermedia.hu
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:b3:42:35:26:05:e3:f3:98:b7:a6:95:9a:5d:8a:
                    f0:5c:7f:b1:0e:78:25:c1:83:ec:9f:3a:e8:39:22:
                    df:43:5e:bd:46:6d:72:89:9c:97:d5:be:2f:e4:14:
                    54:6d:19:34:79:19:9b:41:66:b0:0b:ab:8d:73:46:
                    ec:ca:1e:87:26:66:cb:ca:ed:af:bd:bf:fb:3c:ad:
                    c2:aa:df:d1:7e:39:d8:2b:dd:90:15:45:a8:b5:65:
                    c1:ed:b3:75:50:3e:46:1f:d4:e4:81:1e:5f:9d:0d:
                    1f:b8:39:2c:92:91:f4:8c:60:c9:f6:f5:64:c6:84:
                    f0:f4:5b:d7:a6:6b:d1:f7:01:1d:16:9b:10:16:73:
                    49:73:52:e8:99:e5:7c:da:d5:63:2e:92:60:dd:a7:
                    1a:69:65:85:42:c8:15:c9:55:29:6f:9d:ae:4f:09:
                    2f:2f:a4:cb:fb:5c:9f:04:d8:23:b9:19:cf:55:33:
                    b6:29:1b:42:57:91:7a:b6:34:72:9f:c9:71:17:78:
                    cc:f4:3c:6d:a1:d8:13:67:95:6d:da:85:9d:5f:2a:
                    27:82:57:cd:c0:bb:29:b5:38:17:d5:64:85:be:fa:
                    00:32:a7:f1:11:2a:b6:59:77:71:5a:20:ce:dc:6b:
                    23:8d:05:c0:d1:57:61:b9:aa:0d:ec:6f:4c:73:84:
                    15:29
                Exponent: 65537 (0x10001)
        Attributes:
            a0:00
    Signature Algorithm: sha1WithRSAEncryption
         7f:b6:2c:ba:53:85:1a:43:51:cb:58:f9:37:67:9f:e1:7b:97:
         e3:c3:8f:fe:b0:01:4e:ae:af:06:32:52:b8:67:a4:ce:4a:a6:
         85:3f:60:92:78:2f:15:f4:13:b4:27:bb:4b:1e:67:98:83:30:
         ce:d0:18:b4:94:fc:9b:6b:1a:db:75:b5:0e:85:0e:e7:9e:18:
         25:d6:1a:0f:29:14:fb:dc:c6:37:73:db:18:cf:88:a9:23:ca:
         c9:73:f6:f4:c4:34:c3:52:a9:55:50:7e:d9:0f:aa:ef:e5:0f:
         40:3c:67:7b:14:1b:55:3f:17:c7:bb:71:9a:f2:1c:bb:8f:88:
         60:18:9a:25:95:4f:dd:44:e0:14:77:65:8b:30:e4:40:f7:b9:
         85:d7:c3:db:2e:b2:10:1a:f5:1d:e4:23:30:5e:41:a9:31:25:
         21:fc:76:ee:ad:26:d4:a3:f7:d4:d8:8a:41:0d:ca:8b:28:a1:
         55:6e:5c:39:58:c3:05:96:b0:9f:67:df:a3:d7:0c:01:bd:ae:
         4c:b2:19:92:7b:b5:a8:08:21:30:49:a9:f4:9f:9a:21:65:31:
         44:6a:a7:c1:17:c5:48:f9:55:30:1e:da:ee:5d:8f:8e:a5:79:
         cf:77:d9:34:53:59:ba:e4:a0:e7:2d:34:e2:db:9e:49:f5:20:
         99:67:1a:4c
-----BEGIN CERTIFICATE REQUEST-----
foobar
-----END CERTIFICATE REQUEST-----
`

lib.mainPrivKey = `
Private-Key: (2048 bit)
modulus:
                    00:b3:42:35:26:05:e3:f3:98:b7:a6:95:9a:5d:8a:
                    f0:5c:7f:b1:0e:78:25:c1:83:ec:9f:3a:e8:39:22:
                    df:43:5e:bd:46:6d:72:89:9c:97:d5:be:2f:e4:14:
                    54:6d:19:34:79:19:9b:41:66:b0:0b:ab:8d:73:46:
                    ec:ca:1e:87:26:66:cb:ca:ed:af:bd:bf:fb:3c:ad:
                    c2:aa:df:d1:7e:39:d8:2b:dd:90:15:45:a8:b5:65:
                    c1:ed:b3:75:50:3e:46:1f:d4:e4:81:1e:5f:9d:0d:
                    1f:b8:39:2c:92:91:f4:8c:60:c9:f6:f5:64:c6:84:
                    f0:f4:5b:d7:a6:6b:d1:f7:01:1d:16:9b:10:16:73:
                    49:73:52:e8:99:e5:7c:da:d5:63:2e:92:60:dd:a7:
                    1a:69:65:85:42:c8:15:c9:55:29:6f:9d:ae:4f:09:
                    2f:2f:a4:cb:fb:5c:9f:04:d8:23:b9:19:cf:55:33:
                    b6:29:1b:42:57:91:7a:b6:34:72:9f:c9:71:17:78:
                    cc:f4:3c:6d:a1:d8:13:67:95:6d:da:85:9d:5f:2a:
                    27:82:57:cd:c0:bb:29:b5:38:17:d5:64:85:be:fa:
                    00:32:a7:f1:11:2a:b6:59:77:71:5a:20:ce:dc:6b:
                    23:8d:05:c0:d1:57:61:b9:aa:0d:ec:6f:4c:73:84:
                    15:29
publicExponent: 65537 (0x10001)
privateExponent:
    00:12:34:56:78:9a:bc:de:fe:11:12:31:12:12:12:
prime1:
    00:12:34:56:78:9a:bc:de:fe:11:12:31:12:12:12:
prime2:
    00:12:34:56:78:9a:bc:de:fe:11:12:31:12:12:12:
exponent1:
    00:12:34:56:78:9a:bc:de:fe:11:12:31:12:12:12:
exponent2:
    00:12:34:56:78:9a:bc:de:fe:11:12:31:12:12:12:
coefficient:
    00:12:34:56:78:9a:bc:de:fe:11:12:31:12:12:12:
writing RSA key
-----BEGIN RSA PRIVATE KEY-----
BlaBlaBla
-----END RSA PRIVATE KEY-----
`

lib.mainCert=  `
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 290124 (0x46d4c)
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C=US, O=GeoTrust Inc., CN=RapidSSL SHA256 CA - G3
        Validity
            Not Before: May 16 05:28:23 2015 GMT
            Not After : May 18 21:37:09 2017 GMT
        Subject: OU=GT46628488, OU=See www.rapidssl.com/resources/cps (c)15, OU=Domain Control Validated - RapidSSL(R), CN=www.monstermedia.hu
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:b3:42:35:26:05:e3:f3:98:b7:a6:95:9a:5d:8a:
                    f0:5c:7f:b1:0e:78:25:c1:83:ec:9f:3a:e8:39:22:
                    df:43:5e:bd:46:6d:72:89:9c:97:d5:be:2f:e4:14:
                    54:6d:19:34:79:19:9b:41:66:b0:0b:ab:8d:73:46:
                    ec:ca:1e:87:26:66:cb:ca:ed:af:bd:bf:fb:3c:ad:
                    c2:aa:df:d1:7e:39:d8:2b:dd:90:15:45:a8:b5:65:
                    c1:ed:b3:75:50:3e:46:1f:d4:e4:81:1e:5f:9d:0d:
                    1f:b8:39:2c:92:91:f4:8c:60:c9:f6:f5:64:c6:84:
                    f0:f4:5b:d7:a6:6b:d1:f7:01:1d:16:9b:10:16:73:
                    49:73:52:e8:99:e5:7c:da:d5:63:2e:92:60:dd:a7:
                    1a:69:65:85:42:c8:15:c9:55:29:6f:9d:ae:4f:09:
                    2f:2f:a4:cb:fb:5c:9f:04:d8:23:b9:19:cf:55:33:
                    b6:29:1b:42:57:91:7a:b6:34:72:9f:c9:71:17:78:
                    cc:f4:3c:6d:a1:d8:13:67:95:6d:da:85:9d:5f:2a:
                    27:82:57:cd:c0:bb:29:b5:38:17:d5:64:85:be:fa:
                    00:32:a7:f1:11:2a:b6:59:77:71:5a:20:ce:dc:6b:
                    23:8d:05:c0:d1:57:61:b9:aa:0d:ec:6f:4c:73:84:
                    15:29
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Authority Key Identifier:
                keyid:C3:9C:F3:FC:D3:46:08:34:BB:CE:46:7F:A0:7C:5B:F3:E2:08:CB:59

            Authority Information Access:
                OCSP - URI:http://gv.symcd.com
                CA Issuers - URI:http://gv.symcb.com/gv.crt

            X509v3 Key Usage: critical
                Digital Signature, Key Encipherment
            X509v3 Extended Key Usage:
                TLS Web Server Authentication, TLS Web Client Authentication
            X509v3 Subject Alternative Name:
                DNS:www.monstermedia.hu, DNS:monstermedia.hu
            X509v3 CRL Distribution Points:

                Full Name:
                  URI:http://gv.symcb.com/gv.crl

            X509v3 Basic Constraints: critical
                CA:FALSE
            X509v3 Certificate Policies:
                Policy: 2.23.140.1.2.1
                  CPS: https://www.rapidssl.com/legal

    Signature Algorithm: sha256WithRSAEncryption
         0c:ff:22:d5:0d:66:c3:e7:2f:db:cf:8e:3c:cc:03:e5:8d:b4:
         6a:cb:66:f1:cd:9e:a4:b0:21:48:d4:9b:f3:35:70:b1:9b:a1:
         44:1d:98:cd:fb:14:3d:b4:6f:1a:10:0d:37:8b:3a:d8:c2:ae:
         6f:9d:ab:09:a8:e7:ed:0a:0c:e5:24:f4:02:7d:28:b2:54:8d:
         4b:e8:e7:1f:e9:e2:75:00:d5:16:f3:0e:28:50:cb:8d:36:86:
         8d:d3:42:1c:d0:4a:44:a4:3b:50:cc:d9:cc:0d:60:64:2f:86:
         44:2d:b6:87:bf:8e:48:78:90:90:17:5d:44:0f:aa:5a:59:ca:
         f5:76:0f:db:63:07:4e:48:ab:eb:9d:d7:e7:9d:97:7d:bc:04:
         85:b4:4e:d2:e0:1f:bd:b3:12:5a:7b:be:be:31:e6:44:ec:b0:
         ab:1b:41:f6:5f:3b:c0:87:9b:2b:16:01:d6:23:02:e6:08:18:
         a1:63:84:42:bb:84:c8:47:6e:36:ab:a7:d4:42:03:cd:82:5f:
         de:62:3e:c0:49:33:3d:07:f5:bb:f4:8c:55:23:4e:1b:26:99:
         f8:2e:75:c9:c7:58:ea:48:be:ed:dd:ed:fd:91:21:c1:9e:c3:
         bd:21:b0:fb:12:55:5a:ea:4e:e7:ae:a9:f9:bd:e3:6a:a2:92:
         f2:4b:22:0a
-----BEGIN CERTIFICATE-----
cert0
-----END CERTIFICATE-----
`


// https://github.com/hannob/tlshelpers/blob/master/examples/symantec.key
// https://blog.hboeck.de/archives/888-How-I-tricked-Symantec-with-a-Fake-Private-Key.html
lib.fakeRsaPrivateKey = `
Private-Key: (2048 bit)
modulus:
    00:ea:5b:f3:a9:e5:33:95:9f:fa:51:bf:b1:38:0e:
    7e:9f:1b:15:9e:cb:65:a2:db:78:8e:a8:23:91:37:
    98:5d:83:e3:2e:31:35:8f:0e:6d:50:9c:28:55:cd:
    4d:c5:2f:c8:10:a5:19:9e:40:70:17:62:aa:0f:d8:
    b5:31:1e:bb:6f:71:e2:6d:b7:e9:ba:10:b0:9c:96:
    6c:39:fe:08:39:80:80:2c:eb:47:a8:66:5a:62:36:
    4d:b5:8a:0e:06:96:2b:90:a8:fa:cf:b2:9d:14:0d:
    72:cd:e9:ce:7e:e3:ed:a8:5d:b5:84:27:29:98:f3:
    86:05:1e:0e:ef:7f:88:14:ff:5a:26:c7:67:d4:c7:
    aa:98:59:38:5a:4c:47:f5:30:30:b4:2f:5c:5a:bf:
    68:c3:1e:b2:e5:15:86:e5:30:53:44:b3:7c:ac:5f:
    cf:bd:7b:1b:a6:bf:5b:b4:8c:31:48:c9:be:dc:ca:
    bb:8a:ba:47:7e:20:65:56:98:ad:89:67:36:da:49:
    ed:7e:d6:d3:b0:8e:83:2a:30:fe:bc:a9:59:83:f7:
    61:31:9e:6b:03:03:42:da:d0:0d:2f:61:da:45:a2:
    f9:de:4f:fe:7b:42:25:98:01:3b:fa:6f:16:38:d1:
    a4:e3:37:30:83:d2:c7:e6:e2:64:90:fd:39:35:7d:
    4f:a1
publicExponent: 65537 (0x10001)
privateExponent:
    6d:c7:95:b9:14:0f:36:0a:66:87:ba:d0:36:b1:fd:
    a1:14:80:86:3d:59:e6:1e:53:5c:9a:db:94:c9:e4:
    15:f4:de:80:4a:61:0d:68:66:92:f8:21:09:52:4c:
    ae:4b:35:d6:24:b9:15:7f:a1:44:51:ec:e7:ef:7a:
    1b:ec:f1:3f:ab:42:ae:f3:39:74:d5:5b:5d:de:9a:
    6a:ae:f5:fd:80:49:dc:e9:35:f8:aa:c6:64:06:27:
    7d:9b:55:f5:fd:98:09:3a:23:c4:37:4f:33:fb:f3:
    93:a6:e2:ec:2b:32:d2:58:7c:d5:40:8b:0c:19:6a:
    23:03:14:7f:3c:36:b0:5b:75:51:a3:7e:8c:c7:aa:
    57:44:ee:3c:14:9f:a0:59:0f:e2:8d:8e:7b:ce:fa:
    c7:77:a2:48:ff:15:f3:b5:90:35:c6:06:a0:99:46:
    ee:88:39:8d:6e:21:e7:81:44:e0:ed:29:76:cd:60:
    9b:b8:9e:c1:a2:10:dd:ff:e6:3b:64:92:67:a0:9a:
    e1:6e:c5:ce:92:38:cf:9a:43:19:81:4f:93:65:e8:
    58:82:2c:10:9b:61:7d:79:5c:b1:91:c6:96:6a:af:
    24:e2:b1:b8:6e:c9:81:e1:ef:ef:4c:a2:b4:22:2d:
    c8:ca:6f:a9:f8:38:15:87:51:75:83:ba:f2:39:6a:
    39
prime1:
    00:f9:83:d9:40:81:d5:d7:c5:d0:26:ab:f6:b9:e0:
    18:a9:48:a7:a2:69:76:9b:83:f2:3e:ca:e3:3c:53:
    6e:8f:68:46:13:e8:f8:ae:e3:61:f7:e8:b7:92:d8:
    df:7a:4e:8a:de:78:5e:0e:98:08:5f:a0:c2:7d:ee:
    49:ad:44:1f:1e:f2:0f:49:3b:38:5f:9b:82:30:b9:
    33:c5:3d:10:59:87:98:59:89:98:8e:e0:ce:b2:15:
    90:01:65:fa:02:1e:42:20:2b:4f:73:e3:f4:d7:2f:
    6f:ac:22:c4:7b:89:20:94:d0:d3:99:fe:83:5c:9e:
    62:88:ad:5b:29:07:3b:94:23
prime2:
    00:da:88:86:89:ac:6c:a3:f5:69:5b:51:62:6d:f9:
    11:19:fc:84:9d:60:ec:af:4e:84:7b:aa:69:74:42:
    b2:9e:07:7e:dc:96:96:1e:07:dc:32:29:44:c1:e3:
    52:b6:d5:28:4f:6f:e5:a7:fb:c6:2f:08:ea:33:3f:
    97:36:6e:ee:a5:f6:fe:d5:93:3a:c3:53:20:70:7c:
    2c:5c:f8:2b:7b:d6:63:e4:db:4c:ab:ec:f2:f9:8d:
    ed:23:23:6f:0f:b9:5e:f8:dc:22:f9:b2:6f:18:56:
    89:50:eb:54:60:cb:8b:d2:29:09:3b:21:54:92:99:
    95:4d:c7:65:29:5b:17:3c:9f
exponent1:
    5c:43:bf:10:35:72:87:c9:64:01:08:d1:c1:45:f6:
    98:92:7f:3a:75:59:20:84:98:0d:54:24:d6:e7:db:
    18:38:6c:c1:6c:02:99:59:07:9f:0e:74:e3:5e:42:
    b5:0d:5a:18:8d:d5:e9:fb:9a:0d:12:7b:18:ea:06:
    60:85:b2:24:55:4a:05:c0:5a:46:30:50:e2:07:79:
    f7:53:6f:3a:4c:03:b0:08:4b:5b:7a:11:b4:94:78:
    9d:ab:c1:d9:0a:ab:55:0d:ee:e1:61:c7:e5:d2:6a:
    ae:f2:64:49:72:19:94:2d:ec:31:23:86:1b:b7:8d:
    a4:10:c6:86:d3:5f:da:95
exponent2:
    00:d7:31:2f:82:6c:97:02:91:4c:31:0f:20:24:f8:
    e9:3d:34:ab:06:2a:86:87:4e:83:29:1f:b2:fe:38:
    1b:99:7f:fa:0d:8f:ff:bd:0c:b2:69:76:b7:ed:51:
    43:87:24:b9:b0:17:99:07:64:e7:50:33:b0:23:cd:
    0f:c3:1b:53:c7:7c:44:8e:dd:8f:34:fe:2b:68:78:
    ee:5a:92:4a:87:84:a3:13:9e:5b:8e:c5:22:b2:59:
    6e:d5:76:34:0c:40:d6:e3:aa:2a:8a:c5:89:9e:66:
    c0:8b:3d:60:f6:7c:19:70:56:d6:6f:32:b0:0c:38:
    4e:8c:ab:b7:d3:14:8e:fa:33
coefficient:
    47:98:eb:bc:f4:a9:0e:56:38:10:51:53:92:93:4b:
    73:38:2b:7e:eb:56:59:f8:e0:45:a1:d8:a4:f4:3c:
    b7:8d:1b:10:62:d0:04:d4:d5:c8:c4:0d:32:aa:9c:
    63:01:d0:53:54:f3:d4:11:11:7e:5f:3d:cf:42:2e:
    5a:8d:b6:89:26:ca:66:cd:24:9e:47:c4:1f:99:1b:
    aa:20:68:8b:10:b1:33:0f:10:51:2a:85:f4:d7:53:
    de:15:75:70:c8:37:eb:b9:6c:b8:cd:9c:ff:f4:3d:
    ea:51:5a:70:a3:94:7c:21:ad:06:3a:e5:ef:c1:8b:
    8d:ce:ef:4b:52:52:c7:95
RSA key error: n does not equal p q
writing RSA key
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA6lvzqeUzlZ/6Ub+xOA5+nxsVnstlott4jqgjkTeYXYPjLjE1
jw5tUJwoVc1NxS/IEKUZnkBwF2KqD9i1MR67b3HibbfpuhCwnJZsOf4IOYCALOtH
qGZaYjZNtYoOBpYrkKj6z7KdFA1yzenOfuPtqF21hCcpmPOGBR4O73+IFP9aJsdn
1MeqmFk4WkxH9TAwtC9cWr9owx6y5RWG5TBTRLN8rF/PvXsbpr9btIwxSMm+3Mq7
irpHfiBlVpitiWc22kntftbTsI6DKjD+vKlZg/dhMZ5rAwNC2tANL2HaRaL53k/+
e0IlmAE7+m8WONGk4zcwg9LH5uJkkP05NX1PoQIDAQABAoIBAG3HlbkUDzYKZoe6
0Dax/aEUgIY9WeYeU1ya25TJ5BX03oBKYQ1oZpL4IQlSTK5LNdYkuRV/oURR7Ofv
ehvs8T+rQq7zOXTVW13emmqu9f2ASdzpNfiqxmQGJ32bVfX9mAk6I8Q3TzP785Om
4uwrMtJYfNVAiwwZaiMDFH88NrBbdVGjfozHqldE7jwUn6BZD+KNjnvO+sd3okj/
FfO1kDXGBqCZRu6IOY1uIeeBRODtKXbNYJu4nsGiEN3/5jtkkmegmuFuxc6SOM+a
QxmBT5Nl6FiCLBCbYX15XLGRxpZqryTisbhuyYHh7+9MorQiLcjKb6n4OBWHUXWD
uvI5ajkCgYEA+YPZQIHV18XQJqv2ueAYqUinoml2m4PyPsrjPFNuj2hGE+j4ruNh
9+i3ktjfek6K3nheDpgIX6DCfe5JrUQfHvIPSTs4X5uCMLkzxT0QWYeYWYmYjuDO
shWQAWX6Ah5CICtPc+P01y9vrCLEe4kglNDTmf6DXJ5iiK1bKQc7lCMCgYEA2oiG
iaxso/VpW1FibfkRGfyEnWDsr06Ee6ppdEKyngd+3JaWHgfcMilEweNSttUoT2/l
p/vGLwjqMz+XNm7upfb+1ZM6w1MgcHwsXPgre9Zj5NtMq+zy+Y3tIyNvD7le+Nwi
+bJvGFaJUOtUYMuL0ikJOyFUkpmVTcdlKVsXPJ8CgYBcQ78QNXKHyWQBCNHBRfaY
kn86dVkghJgNVCTW59sYOGzBbAKZWQefDnTjXkK1DVoYjdXp+5oNEnsY6gZghbIk
VUoFwFpGMFDiB3n3U286TAOwCEtbehG0lHidq8HZCqtVDe7hYcfl0mqu8mRJchmU
LewxI4Ybt42kEMaG01/alQKBgQDXMS+CbJcCkUwxDyAk+Ok9NKsGKoaHToMpH7L+
OBuZf/oNj/+9DLJpdrftUUOHJLmwF5kHZOdQM7AjzQ/DG1PHfESO3Y80/itoeO5a
kkqHhKMTnluOxSKyWW7VdjQMQNbjqiqKxYmeZsCLPWD2fBlwVtZvMrAMOE6Mq7fT
FI76MwKBgEeY67z0qQ5WOBBRU5KTS3M4K37rVln44EWh2KT0PLeNGxBi0ATU1cjE
DTKqnGMB0FNU89QREX5fPc9CLlqNtokmymbNJJ5HxB+ZG6ogaIsQsTMPEFEqhfTX
U94VdXDIN+u5bLjNnP/0PepRWnCjlHwhrQY65e/Bi43O70tSUseV
-----END RSA PRIVATE KEY-----
`


lib.setupRelayerMapiPoolForCertInsert = function (assert, app, c_id, server, webhosting_id, mainDomain, putCertCallback) {
        var re = {putCert:0, postDocroot: 0, oGetRelayerMapiPool: app.GetRelayerMapiPool}
        app.GetRelayerMapiPool = function(){
          return {
             putAsync: function(url, data) {
               // console.log("put", url, data)
               assert.equal(url, "/s/"+server+"/cert/certificates/by-webhosting/"+webhosting_id)
               re.putCert++
               if(putCertCallback)
                  return putCertCallback(url, data);

               return Promise.resolve({result:{c_id:c_id, c_begin: '2015-01-01T05:28:23.000Z', c_end: '2016-01-01T21:37:09.000Z', c_subject_common_name: mainDomain}})
             },
             postAsync: function(url, data) {
               // console.log("post", url, data)
               re.postDocroot++
               assert.equal(url, "/s/"+server+"/docrootapi/docroots/"+webhosting_id+"/"+mainDomain+"/certificate")
               if(c_id)
                  assert.deepEqual(data, { certificate_id: c_id })
               else
                  assert.ok(data.certificate_id);

               return Promise.resolve({result:"ok"})
             }
          }
        }
        return re
    }
