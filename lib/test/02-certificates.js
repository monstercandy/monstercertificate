require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../cert-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const path = require("path")
    const fs = require("fs")
    const common = require("00-common.js")

    const webhosting = 16387
    const user = 14851

    var m = common.mocking(assert, app, user)
    m.setupMonsterInfoWebhosting(webhosting)
    m.setupMonsterInfoAccount()

    var getInfoCalls = 0

    var certIds = {}
    var c_id

    var expectedCertificate = { c_user_id: '14851',
  c_webhosting: 16387,
  c_issuer_organization: 'GeoTrust Inc.',
  c_issuer_common_name: 'RapidSSL SHA256 CA - G3',
  c_subject_common_name: 'www.monstermedia.hu',
  c_subject_organization_unit: 'GT46628488 / See www.rapidssl.com/resources/cps (c)15 / Domain Control Validated - RapidSSL(R)',
  c_begin: '2015-05-16T05:28:23.000Z',
  c_end: '2017-05-18T21:37:09.000Z',
  c_purchase: "",
  c_comment: "foobar",
  c_settled: 0,
  c_private_key: 1,
  c_certificate: 1,
  c_intermediate_chain: 1,
  c_certificate_signing_request: 0,
  c_alias: '',
  c_vendor: '',
  c_product: '',
  c_deleted: 0,
  closeToExpire: true,
  isExpired: true
   }

    const jsonInput = common.putCertJsonInput;

 var oCommander = app.commander
 app.commander =  {
      spawn: function(task, params) {
        // console.log("!!!", task, params)

        var re = ""
        if(task.chain.stdin == jsonInput.privateKey)
            re = common.mainPrivKey
        else if(task.chain.stdin.indexOf("intermediate1") > -1)
            re = common.intermediateCert1
        else if(task.chain.stdin.indexOf("intermediate2") > -1)
            re = common.intermediateCert2
        else if((task.chain.stdin == jsonInput.certificate)||(task.chain.stdin == "-----BEGIN CERTIFICATE-----\ncert0\n-----END CERTIFICATE-----\n"))
            re = common.mainCert

        return Promise.resolve({
           executionPromise: Promise.resolve({output: re})
        })
      }
     }



    app.MonsterInfoWebhosting = {
       GetInfo: function(id){
           assert.equal(id, webhosting)

           getInfoCalls++

           return Promise.resolve({
             wh_id: id,
             wh_user_id: user,
             wh_storage: "/web/w3",
             extras: {
                 web_path: "/web/w3/"+id+"-sdfsdfsdf"
             },
             template:{
               t_x509_certificate_external_allowed:1,
             },
           })
       }
    }


    describe("certificates", function(){

        shouldBeEmpty()

        it("put a new certificate", function(done){

                 mapi.put( "/certificates/by-webhosting/"+webhosting, jsonInput,
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.property(result, "c_id")
                    assert.property(result, "c_subject_common_name")
                    assert.property(result, "c_begin")
                    assert.property(result, "c_end")

                    done()

                 })


        })



        it("the certificate just added should be in the list", function(done){

                 mapi.get( "/certificates/by-webhosting/"+webhosting, function(err, result, httpResponse){

                    assert.propertyVal(result, "canBeAdded", true)
                    assert.property(result, "certificates")
                    assert.ok(Array.isArray(result.certificates))
                    assert.equal(result.certificates.length, 1)
                    c_id = result.certificates[0].c_id
                    Array("c_id","created_at").forEach(x=>{
                      assert.ok(result.certificates[0][x])
                      delete result.certificates[0][x]
                    })
                    // console.log(result.certificates[0])
                    var expected = simpleCloneObject(expectedCertificate)
                    expected.c_comment = ""
                    assert.deepEqual(result.certificates[0], expected)
                    done()

                 })

        })



        it("changing some params (raw)", function(done){
                 mapi.post( "/certificates/all/item/"+c_id+"/raw", {c_comment:"foobar"},
                 function(err, result, httpResponse){
                    assert.equal(result, "ok")

                    done()

                 })
        })

        shouldReturnTheExpectedCertificate()

        wipeIt()

        shouldBeEmpty()

    })


    describe("partial uploads", function(){

        addSomeCert()


        it("the certificate just added should be in the list", function(done){

                 mapi.get( "/certificates/by-webhosting/"+webhosting, function(err, result, httpResponse){

                    assert.propertyVal(result, "canBeAdded", true)
                    assert.property(result, "certificates")
                    assert.ok(Array.isArray(result.certificates))
                    assert.equal(result.certificates.length, 1)
                    c_id = result.certificates[0].c_id
                    assert.propertyVal(result.certificates[0], "c_certificate", 1)
                    assert.propertyVal(result.certificates[0], "c_certificate_signing_request", 0)
                    assert.propertyVal(result.certificates[0], "c_intermediate_chain", 0)
                    assert.propertyVal(result.certificates[0], "c_private_key", 0)
                    done()

                 })

        })

        it("uploading the private key later", function(done){

                 mapi.put( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/private-key", {privateKey:jsonInput.privateKey},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.equal(result, "ok")

                    done()

                 })


        })

        it("second tiem should be rejected", function(done){

                 mapi.put( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/private-key", {privateKey:jsonInput.privateKey},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.propertyVal(err, "message", "GOT_IT_ALREADY")

                    done()

                 })


        })



        it("uploading intermediate later", function(done){

                 mapi.put( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/intermediates", {intermediates:jsonInput.intermediate},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.equal(result, "ok")

                    done()

                 })

        })

        it("and overriding it again", function(done){

                 mapi.put( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/intermediates", {intermediates:jsonInput.intermediate},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.equal(result, "ok")

                    done()

                 })
        })

        shouldReturnTheExpectedCertificate()


        wipeIt()

        shouldBeEmpty()

        addSomeCert()

        it("marking it as settled", function(done){

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/settled", {settled: true},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.equal(result, "ok")

                    done()

                 })


        })

        it("uploading the private key should be rejected", function(done){

                 mapi.put( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/private-key", {privateKey:jsonInput.privateKey},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.propertyVal(err, "message", "ALREADY_SETTLED")

                    done()

                 })


        })

        it("uploading intermediate should be rejected", function(done){

                 mapi.put( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/intermediates", {intermediates:jsonInput.intermediate},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.propertyVal(err, "message", "ALREADY_SETTLED")

                    done()

                 })

        })

        wipeIt()


   })

   describe("enable/disable", function(){

        addSomeCert()


       it("disabling it", function(done){

                 var stats = setupRelayerForDocroot()

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/disable", {},
                 function(err, result, httpResponse){

                    assert.equal(result, "ok")

                    return fs.readdirAsync(app.config.get("dir_cert_store_active"))
                      .then(foos=>{
                        assert.deepEqual(foos, [".placeholder"])

                        return fs.readdirAsync(app.config.get("dir_cert_store_archive"))
                      })
                      .then(foos=>{
                        // note: fullchain should be written even if there is no intermediate!
                        assert.equal(foos.length, 4) // placeholder + crt + fullchain + nfo
                        assert.equal(stats.disableWasCalled, 1)
                        app.GetRelayerMapiPool = stats.oGetRelayerMapiPool
                        done()
                      })

                 })

        })


        it("we should still be able to find it by id", function(done){

                 mapi.get( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id, function(err, result, httpResponse){

                    assert.ok(result.c_id)
                    assert.propertyVal(result, "c_deleted", 1)
                    done()

                 })

        })


        Array({}, {c_deleted:false}).forEach(x=>{
            it("listing certificates by webhosting through the search route, they should be empty: "+JSON.stringify(x), function(done){

                     mapi.post( "/certificates/by-webhosting/"+webhosting+"/search", x, function(err, result, httpResponse){

                        assert.deepEqual(result, {canBeAdded: true, certificates:[]})
                        done()

                     })

            })

        })


        it("listing certificates by webhosting through the search route, should be found when asking for deleted ones", function(done){

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/search", {c_deleted: true}, function(err, result, httpResponse){

                    assert.equal(result.certificates.length, 1)
                    done()

                 })

        })

       it("enabling it", function(done){

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/enable", {},
                 function(err, result, httpResponse){
                    assert.equal(result, "ok")

                    return fs.readdirAsync(app.config.get("dir_cert_store_active"))
                      .then(foos=>{
                        assert.ok(foos.length >= 3) // .placeholder +crt +nfo

                        return fs.readdirAsync(app.config.get("dir_cert_store_archive"))
                      })
                      .then(foos=>{
                        assert.deepEqual(foos, [".placeholder"])
                        done()
                      })


                 })

        })


        Array({}, {c_deleted:false}).forEach(x=>{
            it("listing certificates by webhosting through the search route, should be found again: "+JSON.stringify(x), function(done){

                     mapi.post( "/certificates/by-webhosting/"+webhosting+"/search", x, function(err, result, httpResponse){

                        assert.equal(result.certificates.length, 1)
                        done()

                     })

            })

        })


        it("listing certificates by webhosting through the search route, should not be found when asking for deleted ones", function(done){

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/search", {c_deleted: true}, function(err, result, httpResponse){

                    assert.deepEqual(result, {canBeAdded: true, certificates:[]})
                    done()

                 })

        })

        wipeIt()

        shouldBeEmpty()

   })

   describe("attaching/detaching", function(){

       addSomeCert()


       it("detach certificate from webhostring", function(done){

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/detach", {}, function(err, result, httpResponse){

                    assert.equal(result, "ok")
                    done()
                 })

        })

       shouldBeFoundInUserRouteOnly()


       it("attach certificate to webhostring", function(done){

                 mapi.post( "/certificates/by-user/"+user+"/item/"+c_id+"/attach", {c_webhosting: webhosting}, function(err, result, httpResponse){

                    assert.equal(result, "ok")
                    done()
                 })

        })


            it("listing certificates by webhosting, should be found again", function(done){

                     mapi.get( "/certificates/by-webhosting/"+webhosting+"/", function(err, result, httpResponse){

                        assert.equal(result.certificates.length, 1)
                        done()

                     })

            })

            it("listing certificates by user, should not be found", function(done){

                     mapi.get( "/certificates/by-user/"+user+"/", function(err, result, httpResponse){

                        assert.equal(result.certificates.length, 0)
                        done()

                     })

            })


       it("remove webhosting should detach all certificates from webhostring", function(done){

                 mapi.delete( "/certificates/by-webhosting/"+webhosting, {}, function(err, result, httpResponse){

                    assert.equal(result, "ok")
                    done()
                 })

        })

       shouldBeFoundInUserRouteOnly()

        wipeIt()

        shouldBeEmpty()

   })



   describe("aliasing", function(){

        it("adding a first certificate", function(done){

                 mapi.put( "/certificates/by-webhosting/"+webhosting, {certificate: '-----BEGIN CERTIFICATE-----\ncert0\n-----END CERTIFICATE-----\n'},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.property(result, "c_id")
                    assert.ok(result.c_id)
                    certIds.oldCertId = result.c_id

                    done()

                 })

        })


        addSomeCert()

        it("set alias", function(done){

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/alias", {alias:"foobar"}, function(err, result, httpResponse){

                    assert.equal(result, "ok")

                    fs.readdir(app.config.get("dir_cert_store_active"), function(err, foos){
                        assert.ok(foos.indexOf("foobar.crt") > -1)
                        var pointsTo = fs.readlinkSync(path.join(app.config.get("dir_cert_store_active"),"foobar.crt"))
                        assert.ok(pointsTo.indexOf(c_id+".crt") > -1)
                        done()
                    })

                 })

        })


        it("querying cert should show the alias", function(done){

                 mapi.get( "/certificates/by-user/"+user+"/item/"+c_id, function(err, result, httpResponse){

                    assert.propertyVal(result, "c_alias", "foobar")
                    done()

                 })

        })


        it("removing the cert should now be not possible", function(done){

                 mapi.delete( "/certificates/by-user/"+user+"/item/"+c_id, {}, function(err, result, httpResponse){

                    assert.propertyVal(err, "message", "REMOVE_ALIAS_FIRST")
                    done()
                 })

        })

        it("changing alias should change the symlinks", function(done){

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/item/"+c_id+"/alias", {alias:"somethingelse"}, function(err, result, httpResponse){

                    assert.equal(result, "ok")
                    fs.readdir(app.config.get("dir_cert_store_active"), function(err, foos){
                        assert.ok(foos.indexOf("foobar.crt") < 0)
                        assert.ok(foos.indexOf("somethingelse.crt") > -1)
                        done()
                    })
                 })

        })


        it("call to set alias on another certicate should be successfull", function(done){

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/item/"+certIds.oldCertId+"/alias", {alias:"somethingelse"}, function(err, result, httpResponse){

                    assert.equal(result, "ok")

                    fs.readdir(app.config.get("dir_cert_store_active"), function(err, foos){
                        assert.ok("file should still be there", foos.indexOf("somethingelse.crt") > -1)
                        var pointsTo = fs.readlinkSync(path.join(app.config.get("dir_cert_store_active"),"somethingelse.crt"))
                        assert.ok(pointsTo.indexOf(certIds.oldCertId+".crt") > -1)
                        done()
                    })

                 })

        })


        it("querying cert should not show the alias anymore", function(done){

                 mapi.get( "/certificates/by-user/"+user+"/item/"+c_id, function(err, result, httpResponse){

                    assert.propertyVal(result, "c_alias", "")
                    done()

                 })

        })

        it("but should show it at the other", function(done){

                 mapi.get( "/certificates/by-user/"+user+"/item/"+certIds.oldCertId, function(err, result, httpResponse){

                    assert.propertyVal(result, "c_alias", "somethingelse")
                    done()

                 })

        })

        it("removing alias should remove symlinks", function(done){

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/item/"+certIds.oldCertId+"/alias", {alias:""}, function(err, result, httpResponse){

                    assert.equal(result, "ok")
                    fs.readdir(app.config.get("dir_cert_store_active"), function(err, foos){
                        assert.ok(foos.indexOf("foobar.crt") < 0)
                        assert.ok(foos.indexOf("somethingelse.crt") < 0)
                        done()
                    })
                 })

        })


        wipeIt()
        wipeIt("oldCertId")

        shouldBeEmpty()

   })


   function shouldBeFoundInUserRouteOnly(){

            it("listing certificates by webhosting, should not be found", function(done){

                     mapi.get( "/certificates/by-webhosting/"+webhosting+"/", function(err, result, httpResponse){

                        assert.equal(result.certificates.length, 0)
                        done()

                     })

            })

            it("listing certificates by user, should be found", function(done){

                     mapi.get( "/certificates/by-user/"+user+"/", function(err, result, httpResponse){

                        assert.equal(result.certificates.length, 1)
                        done()

                     })

            })

   }

   function addSomeCert(cb){
        it("put a new certificate (certificate only)", function(done){

                 mapi.put( "/certificates/by-webhosting/"+webhosting, {c_comment:"foobar",certificate:jsonInput.certificate},
                 function(err, result, httpResponse){
                    // console.log("re", result, err)
                    assert.property(result, "c_id")
                    assert.ok(result.c_id)
                    c_id = result.c_id

                    if(cb) cb(result)

                    done()

                 })


        })

   }


    function shouldBeEmpty(){
        it("listing certificates by webhosting, they should be empty", function(done){

                 mapi.get( "/certificates/by-webhosting/"+webhosting, function(err, result, httpResponse){

                    assert.deepEqual(result, {canBeAdded: true, certificates:[]})
                    done()

                 })

        })

        it("listing certificates by user, they should be empty", function(done){

                 mapi.get( "/certificates/by-user/"+user, function(err, result, httpResponse){

                    assert.deepEqual(result, {canBeAdded: true, certificates:[]})
                    done()

                 })

        })

        it("listing certificates by webhosting through the search route, they should be empty", function(done){

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/search", {c_deleted: false}, function(err, result, httpResponse){

                    assert.deepEqual(result, {canBeAdded: true, certificates:[]})
                    done()

                 })

        })

        it("listing certificates by webhosting through the search route with broken params", function(done){

                 mapi.post( "/certificates/by-webhosting/"+webhosting+"/search", ["path.format;"], function(err, result, httpResponse){

                    assert.deepEqual(result, {canBeAdded: true, certificates:[]})
                    done()

                 })

        })


    }

    function shouldReturnTheExpectedCertificate(){

        it("we should be able to find it by id as well; it should return the raw data as well", function(done){

                 mapi.get( "/certificates/by-user/"+user+"/item/"+c_id, function(err, result, httpResponse){

                    assert.ok(result.c_id)
                    delete result.c_id
                    assert.ok(result.created_at)
                    delete result.created_at

                    assert.deepEqual(result.data, { certificate: '-----BEGIN CERTIFICATE-----\ncert0\n-----END CERTIFICATE-----\n',
  intermediates:
   [ '-----BEGIN CERTIFICATE-----\ncert1\n-----END CERTIFICATE-----\n',
     '-----BEGIN CERTIFICATE-----\ncert2\n-----END CERTIFICATE-----\n' ],
 })

                    delete result.data

                    assert.deepEqual(result, expectedCertificate)
                    done()

                 })

        })

        it("and at the /full route, we should even get the private key", function(done){

                 mapi.get( "/certificates/by-user/"+user+"/item/"+c_id+"/full", function(err, result, httpResponse){

                    assert.ok(result.c_id)
                    delete result.c_id
                    assert.ok(result.created_at)
                    delete result.created_at

                    assert.deepEqual(result.data, { certificate: '-----BEGIN CERTIFICATE-----\ncert0\n-----END CERTIFICATE-----\n',
                        "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nBlaBlaBla\n-----END RSA PRIVATE KEY-----\n",
  intermediates:
   [ '-----BEGIN CERTIFICATE-----\ncert1\n-----END CERTIFICATE-----\n',
     '-----BEGIN CERTIFICATE-----\ncert2\n-----END CERTIFICATE-----\n' ],
 })

                    delete result.data

                    assert.property(result,"c_reseller")
                    delete result.c_reseller
                    assert.property(result,"c_reseller_certificate_id")
                    delete result.c_reseller_certificate_id

                    assert.deepEqual(result, expectedCertificate)
                    done()

                 })

        })
    }

    function wipeIt(certIdName){


       it("removing it should remove the files", function(done){


                 var certId = (certIdName ? certIds[certIdName] : "") || c_id

                 var stats = setupRelayerForDocroot(certId)

                 mapi.delete( "/certificates/by-user/"+user+"/item/"+certId, {}, function(err, result, httpResponse){

                    assert.equal(result, "ok")
                    fs.readdir(app.config.get("dir_cert_store_active"), function(err, foos){
                        if(err) return done(err)
                        for(var f of foos) {
                            // console.log("checking",f,"against",certId,foos)
                            if(f.indexOf(certId) > -1)
                                throw new Error("file should not be present: "+f)
                        }
                        assert.equal(stats.disableWasCalled, 1)
                        app.GetRelayerMapiPool = stats.oGetRelayerMapiPool
                        done()
                    })

                 })

        })

    }


    function setupRelayerForDocroot(ac_id){
        var re = {disableWasCalled: 0, oGetRelayerMapiPool: app.GetRelayerMapiPool}
        disableWasCalled = 0
        app.GetRelayerMapiPool = function(){
          return {
             deleteAsync: function(url, data) {
               // console.log("delete", url, data)
               assert.equal(url, "/s/s111/docrootapi/certificates/"+(ac_id||c_id))
               re.disableWasCalled++
               return Promise.resolve({result:"ok"})
             },
          }
        }
        return re
    }


}, 10000)


