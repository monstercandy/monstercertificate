require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../cert-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var aclib = require("../lib-accents.js")

  describe("available resellers/vendors etc", function(){

      it("resellers", function(done){
             mapi.get( "/config/resellers", function(err, result, httpResponse){
                    assert.deepEqual(result, {resellers:["letsencrypt","namecheap"]})
                    done()

                 })
      })


      it("vendors of letsencrypt", function(done){
             mapi.get( "/config/resellers/letsencrypt/vendors", function(err, result, httpResponse){
                    assert.deepEqual(result, {vendors:["Let's Encrypt"]})
                    done()

                 })
      })


      it("products of letsencrypt", function(done){
             mapi.post( "/config/resellers/letsencrypt/vendors/products", {vendor: "Let's Encrypt"}, function(err, result, httpResponse){
                    assert.deepEqual(result, {products:["Let's Encrypt"]})
                    done()

                 })
      })


      it("vendors", function(done){
             mapi.get( "/config/resellers/namecheap/vendors", function(err, result, httpResponse){
                    assert.deepEqual(result, {vendors:["Comodo"]})
                    done()

                 })
      })

      it("products of namecheap", function(done){
             mapi.get( "/config/resellers/namecheap/vendors/Comodo/products", function(err, result, httpResponse){
               // console.log(result)
                    assert.ok(result.products.indexOf("PositiveSSL") > -1)
                    done()

                 })
      })

  })


  describe("misc controls", function(){

    it("xml2js building", function(){
        var xml2js = require('xml2js');

        var obj = {name: "Super", Surname: "Man", age: 23};

        var builder = new xml2js.Builder();
        var xml = builder.buildObject(obj);

        var r = xml.replace(/\n/g,"").replace(/\s{2,}/g, " ")
        // console.log("xml", r)
        assert.equal('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><root> <name>Super</name> <Surname>Man</Surname> <age>23</age></root>', r)
    })

    it("xml2js parsing", function(){
      const common = require("00-common.js")
      var resp = common.namecheapGetBalanceResponse

        var p = app.getXmlParser()
        return p.parseStringAsync(resp)
          .then(data=>{
            // var util = require("util");console.log(util.inspect(data, {showHidden: false, depth: null}))
            assert.deepEqual(data, { ApiResponse:
   { '$': { Status: 'OK', xmlns: 'http://api.namecheap.com/xml.response' },
     Errors: [ '' ],
     Warnings: [ '' ],
     RequestedCommand: [ 'namecheap.users.getbalances' ],
     CommandResponse:
      [ { '$': { Type: 'namecheap.users.getBalances' },
          UserGetBalancesResult:
           [ { '$':
                { Currency: 'USD',
                  AvailableBalance: '29.86',
                  AccountBalance: '29.86',
                  EarnedAmount: '0.00',
                  WithdrawableAmount: '0.00',
                  FundsRequiredForAutoRenew: '0.00' } } ] } ],
     Server: [ 'PHX01APIEXT03' ],
     GMTTimeDifference: [ '--4:00' ],
     ExecutionTime: [ '0.058' ] } })


          })


    })

    it("replacements should be fine", function(){
        var a = aclib("árvíztűrő tükörfúrógép ÁRVÍZTŰRŐ TÜKÖRFÚRÓGÉP")
        assert.equal(a, "arvizturo tukorfurogep ARVIZTURO TUKORFUROGEP")
    })

    it("replacements should be fine for hashes as well, functions to be discarded", function(){
        var a = aclib.hash({a:"árvíztűrő tükörfúrógép ÁRVÍZTŰRŐ TÜKÖRFÚRÓGÉP", b: function(){return 42;}})
        assert.deepEqual(a, {a:"arvizturo tukorfurogep ARVIZTURO TUKORFUROGEP"})
    })


        it("listing supported countries", function(done){

                 mapi.get( "/config/countries", function(err, result, httpResponse){
                    assert.propertyVal(result, "HU", "Hungary")
                    done()

                 })

        })
  })


}, 10000)


