var lib = module.exports = function(app, knex){
	var re = {}

	const vendorCode = "vendor1"

    const dotq = require("MonsterDotq");
	const fs = dotq.fs()
	const path = require("path")

    var ValidatorsLib = require("MonsterValidators")
    var vali = ValidatorsLib.ValidateJs()

    var spawnParameters = [app.config.get("cmd_pathes"), app.config.get("common_params"), ]

    const MError = app.MError

    re.GetVendors = function(){
    	return ["Let's Encrypt"]
    }
    re.GetProductsOfVendor = function(vendor){
    	return ["Let's Encrypt"]
    }

	re.isValidCertificateId = function(certId){
		return !certId
	}

	re.Buy = function(purchaseData){

		// we don't need to do anything here as these certs dont need to be purchased
		return Promise.resolve()
	}

	re.doActivateGeneric = function(command, purchase, d, in_data, insertActivationCb){
		throw new MError("LETSENCRYPT_IS_LOCAL_ONLY")
	}

	re.Poll = function(purchase) {
		throw new MError("NOT_SUPPORTED")
	}

	re.Revoke = function(purchase){

        var cmd_letsencrypt_revoke = app.config.get("cmd_letsencrypt_revoke")
        var params = simpleCloneObject(spawnParameters)
        var livedir = path.join(params[0].letsencrypt_config_dir_path, "live", purchase.pc_subject_common_name)
        var path_privkey = path.join(livedir, "privkey.pem")
        var path_cert = path.join(livedir, "cert.pem")
        params.push({private_key_path:path_privkey,certificate_path:path_cert})

        return app.commander.spawn({chain:cmd_letsencrypt_revoke,executeImmediately: true, removeImmediately: true}, params)
		  .then(h=>{
		  	return h.executionPromise
		  	  .catch(ex=>{
		  	  	 if(ex.output.indexOf('Certificate already revoked')) {
		  	  	 	console.log("Certificate was already revoked, proceeding for robustness")
		  	  	 	return Promise.resolve()
		  	  	 }
		  	  	 throw ex
		  	  })
		  })
		  .then(()=>{
		  	 var fn = path.join(params[0].letsencrypt_config_dir_path, "renewal", purchase.pc_subject_common_name+".conf")
		  	 const patternsToDel = [fn]

             var dirs = []
             Array("live","archive").forEach(dirName=>{
             	var dir = path.join(params[0].letsencrypt_config_dir_path, dirName, purchase.pc_subject_common_name)
             	dirs.push(dir)
             	patternsToDel.push(path.join(dir, "*.pem"))
             	patternsToDel.push(path.join(dir, "README"))
             })


             const del = require('MonsterDel');
             // console.log("xxx",patternsToDel)
             return del(patternsToDel,{"force": true})
               .then(()=>{
               	   var ps = []
               	   dirs.forEach(dir=>{
               	   	  ps.push(
               	   	  	 fs.rmdirAsync(dir)
               	   	  	   .catch((ex=>{

					  	   	  if(ex.code != "ENOENT") {
			                    console.error("Error removing letsencrypt directory", dir, ex)
					  	   	  	throw ex
					  	   	  }
					  	   	  return Promise.resolve()

               	   	  	   }))
               	   	  )
               	   })
               	   return Promise.all(ps)
               })

		  })
	  	  .catch(ex=>{
	  	  	 console.error("letsencrypt returned failure during revocation", ex)
	  	  	 throw new MError("LETSENCRYPT_FAILURE")
	  	  })

	}


	re.ShouldRenewBeBought = function(purchase, now, pcend){
		return false; // since it will be doen automatically
	}

	re.doActivateLocal = function(command, purchase, d, in_data, insertActivationCb, emitter){
		// d is validated

	  	 if((d.attach)&&(d.attach.server != app.config.get("mc_server_name")))
	  	 	throw new MError("LETSENCRYPT_IS_LOCAL_ONLY")

 		 var activation
 		 var purchases = app.getPurchases()
	  	 var mainDomain = d.domains[0]

         var p
         if(d.attach) {
           p = app.MonsterInfoWebhosting.GetInfo(d.attach.webhosting_id)
           .then(webhostingInfo=>{
             if(!webhostingInfo.template.t_x509_certificate_letsencrypt_allowed) throw new MError("LETSENCRYPT_NOT_ALLOWED_FOR_THIS_WEBHOSTING")
             if(webhostingInfo.wh_user_id != purchase.pc_user_id) throw new MError("INVALID_USER")
             return Promise.resolve()
           })
         }
         else
         	p = Promise.resolve()

         return p
           .then(()=>{
     	  	 return insertActivationCb({extraParameters: {mainDomain:mainDomain}})

           })
	 	   .then(aActivation=>{

		  	activation = aActivation

            var cmd_letsencrypt_create = simpleCloneObject(app.config.get("cmd_letsencrypt_create"))
            if(app.config.get("certbot_webroot_param")) {
            	cmd_letsencrypt_create.args.push("--webroot");
            }
            d.domains.forEach(dom=>{
            	cmd_letsencrypt_create.args.push("-d")
            	cmd_letsencrypt_create.args.push(dom)
            })

	        return app.commander.spawn({chain:cmd_letsencrypt_create,emitter:emitter,executeImmediately: true, removeImmediately: true}, spawnParameters)

		  })
		  .then(h=>{
		  	return h.executionPromise
		  	  .catch(ex=>{
		  	  	 console.error("letsencrypt returned failure during activation", ex)
		  	  	 throw new MError("LETSENCRYPT_FAILURE", null, ex)
		  	  })
		  })
		  .then(taskOutput=>{
		  	 // slurp files and store certificate

		  	 return slurpLetsEncryptFiles(mainDomain)
		  })
		  .then(issuedCertificate=>{
		  	 // insert certificates

		  	 return activation.UploadResult(issuedCertificate)
		  })
		  .catch(ex=>{
    	  	 console.log("error during letsencrypt activation", in_data, ex)

		  	 if(activation) {
		  	 	// mark activation as unsuccessful
		  	 	activation.SetStatus("failed")
		  	 }


		  	 throw ex
		  })
	}

	re.InsertAndActivate = function(in_data, ts, req) {
		var purchases = app.getPurchases()
       return Promise.resolve()
         .then(()=>{
             if(typeof in_data != "object") throw new MError("VALIDATION_ERROR")
             if(typeof in_data.attach != "object") throw new MError("VALIDATION_ERROR")
             if(!Array.isArray(in_data.domains)) throw new MError("VALIDATION_ERROR")

             var mainDomain = in_data.domains[0]
             // console.log("testing", mainDomain)
             var x= knex("purchases").select().whereRaw("pc_reseller=? AND pc_subject_common_name=? AND NOT (pc_status IN ('removed','revoked'))", ['letsencrypt', mainDomain])
             // console.log(x.toSQL())
             return x
          })
          .then(rows=>{
             // console.log("????", rows)
             if(rows.length) throw new MError("LETSENCRYPT_ALREADY_ACTIVATED")

             in_data.pc_reseller = "letsencrypt"
             in_data.pc_product = in_data.pc_vendor = "Let's Encrypt"
             in_data.pc_status = "pending"
          	 in_data.local = true // for the activation
          	 in_data.skip_email = true

             return purchases.Insert(in_data, ts)
         })
         .then(x=>{
            return purchases.GetPurchaseById(x.pc_id)
         })
         .then(purchase=>{

            return purchase.Activate(in_data, ts, req, req ? true : false)

         })
	}

	re.RenewWhereExpirationIsClose = function(emitter){
		var days = app.config.get("letsencrypt_auto_reissue_days_before_expiration")

		var max_retries = app.config.get("letsencrypt_auto_reissue_max_retries")
		var sleep_between = app.config.get("letsencrypt_auto_reissue_sleep_between")

		var purchases = app.getPurchases()

		// ts should look like: 2015-05-16T05:28:23Z
		return purchases.ListByRaw({sqlFilterStr:"pc_reseller='letsencrypt' AND pc_status='issued' AND pc_end < strftime('%Y-%m-%dT%H:%M:%SZ', 'now','"+days+" days')", sqlFilterValues:[]})
		  .then(aPurchases=>{

		  	 if(emitter)emitter.send_stdout_ln("There are "+aPurchases.length+" certificates to be renewed")

		  	 if(!aPurchases.length) return Promise.resolve()

		  	 var purchasesToBeRenewed = aPurchases

		  	 console.log("renewing", purchasesToBeRenewed.length, "letsencrypt certificate")

		  	 return dotq.linearMap({array: purchasesToBeRenewed, action: function(original){

			  	 	if(emitter)emitter.send_stdout_ln("Renewing "+original.pc_subject_common_name)

			  	    var renewed
			  	    var pc;

			  	 	return original.Renew()
			  	      .then(apc=>{
			  	      	 pc = apc;
			  	      	 return purchases.GetPurchaseById(pc.pc_id)
			  	      })
			  	      .then(aRenewed=>{
			  	      	 renewed = aRenewed
			  	      	 return original.GetActivation()
			  	      })
			  	      .then(activation=>{
			  	      	 return activateRetryLogic(activation, renewed, emitter, 3);
			  	      })
			  	      .then(()=>{
			  	      	 console.log("In the middle of a letsencrypt auto renewal process; original purchase is:", original);
			  	      	 return original.GetCertificate()
			  	      	   .then(originalCertificate=>{
			  	      	   	  console.log("Corresponding certificate was retrieved:", originalCertificate);
			  	      	   	  if(!originalCertificate.c_alias) return;

			  	      	   	  console.log("Retrieving the renewed purchase by id", pc.pc_id);
                              return purchases.GetPurchaseById(pc.pc_id)
                                .then(renewed=>{
                                	console.log("Retrieving the the certificate of the renewed purchase", renewed);
			  	      	   	        return renewed.GetCertificate()                                	
                                })
			  	      	   	    .then(renewedCertificate=>{
			  	      	   	    	console.log("Setting new alias for the renewedCertificate", renewedCertificate);
			  	      	   	    	return renewedCertificate.SetAlias(originalCertificate.c_alias);
			  	      	   	    })
			  	      	   })
			  	      })
			  	 	  .catch(ex=>{
			  	 	  	 app.InsertException(null, {
			  	 	  	 	message: "LETSENCRYPT_AUTO_RENEW_FAILED", 
			  	 	  	 }, ex);
			  	 	  	 console.error("Unable to auto renew lets encrypt certificate", original, ex)
			  	 	  })


			  	 	function activateRetryLogic(activation, renewed, emitter, maxRetries){
			  	         console.log("Activating letsencrypt certificate", maxRetries, activation)
			  	      	 return renewed.Activate(activation.a_payload, null, emitter)			  	 	
			  	 		  .catch(ex=>{
			  	 		  	 console.error("Error while activating lets encrypt cert", original, ex, maxRetries)
			  	 		  	 if(maxRetries > 0) {
			  	 		  	    return dotq.sleep(10000)
			  	 		  	      .then(function(){
			  	 		  	 	      return activateRetryLogic(activation, renewed, emitter, maxRetries -1);
			  	 		  	      })
			  	 		  	 }

			  	 		  	 throw ex;
			  	 		  })

			  	 	}

		  	 }})


		  })
	}

	re.GetActions_pending = function(){
		return Array(vendorCode+"-activate")
	}
	re.GetActions_issued = function(){
		return Array(vendorCode+"-reissue", vendorCode+"-revoke")
	}


    re.Renew = function(purchase, candidate, in_data) {

    	 console.log("renewing letsencrypt", purchase)
    	 candidate.skip_email = true
 		 return Promise.resolve()
    }

    install_cron();

	return re

	function slurpLetsEncryptFiles(domain){
		var re = {}
		var d = app.config.get("common_params").domain_validation_letsencrypt_live_directory

		// console.log("slurping!!!!")
		var ps = []
		Array("cert","privkey","chain").forEach(x=>{
			ps.push(fs.readFileAsync(path.join(d, domain, x+".pem"), "utf8").then(c=>{re[x]=c;}))
		})
		return Promise.all(ps)
		  .then(()=>{

		  	 // console.log("slurping!!!! finished", re)

		  	 return Promise.resolve({certificate: re.cert, intermediate: re.chain, privateKey: re.privkey})
		  })
	}

  function install_cron(){
    if(app.cron_letsencrypt_reissue) return
    app.cron_letsencrypt_reissue = true

    var csp = app.config.get("cron_letsencrypt_renew")
    if(!csp) return

    const cron = require('Croner');

    cron.schedule(csp, function(){
        return re.RenewWhereExpirationIsClose()
    });

  }


}
