FROM monstercommon

ADD lib /opt/MonsterCertificate/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterCertificate/lib/bin/cert-install.sh && /opt/MonsterCertificate/lib/bin/cert-test.sh

ENTRYPOINT ["/opt/MonsterCertificate/lib/bin/cert-start.sh"]
